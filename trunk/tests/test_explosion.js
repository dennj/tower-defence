var Map = require("js/map").Map;
var Match = require("js/match").Match;
var Explosion = require("js/explosion").Explosion;

qModule('js/explosion', {
	setup: function() {
		// Set match and map
		this.match = new Match();
		this.map = new Map(this.match, 800, 400);
		this.match.setMap(this.map);
		this.duration = 1000;
	}
});

//test getDuration
test("Checking if getDuration works correctly", function() {
	
	var testExplosion = new Explosion(this.match, [1,1], this.duration);
	
	strictEqual(testExplosion.getDuration(), this.duration, "Function returns its duration");	
});

//test getMatch
test("Checking if getMatch works correctly", function() {
	var testExplosion = new Explosion(this.match, [1,1], this.duration);
	
	
	strictEqual(testExplosion.getMatch(), this.match, "Function returns its match");	
});

//test update
test("Checking if update works correctly", function() {
	var testExplosion = new Explosion(this.match, [1,1], this.duration);
	
	var testUpdate=500;
	testExplosion.update(testUpdate);
	strictEqual(testExplosion.getDuration(), this.duration-testUpdate , "Function update works correctly");	
});

//test if explosion terminates correctly
test("Checking if explosion dies after its duration", function() {
	var testExplosion = new Explosion(this.match, [1,1], this.duration);
	
	testExplosion.update(500);
	ok(! (testExplosion.isDead()),"Explosion is alive after 500 ms (duration = 1000ms)");
	testExplosion.update(800);
	ok(testExplosion.isDead(),"Explosion terminates correctly");	
});

