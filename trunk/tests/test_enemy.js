var Enemy = require('js/enemy').Enemy;
var Match = require('js/match').Match;
var Map = require('js/map').Map;
var gamejs = require('gamejs');

qModule('js/enemy', {
	setup: function() {
		this.match = new Match();
		this.map = new Map(this.match, 800, 400);
		this.match.setMap(this.map);
		// Instantiate some "clean" Enemy
		this.testE1 = new Enemy(this.match, [0,0],0);
		this.testE2 = new Enemy(this.match, [0,0],0);
		this.testE3 = new Enemy(this.match, [0,0],0);
		this.testE4 = new Enemy(this.match, [0,0],0);
		this.testE5 = new Enemy(this.match, [0,0],0);
	}
});

test("Enemy(match, position, level)", function(){
	var center = [10,50];
	var testEnemy = new Enemy(this.match, center, 0);
	ok(testEnemy instanceof Enemy,
		"Test with arguments (Match, [10,50], default): " +
		"object correctly created"
	);
	deepEqual(testEnemy.getMatch(), this.match,
		"Test with arguments (Match, [10,50], default): " +
		"match correctly setted"
	);
	deepEqual(testEnemy.getCenter(), center,
		"Test with arguments (Match, [10,50], default): " +
		"center correctly setted"
	);
	deepEqual(testEnemy.getLevel(), 0,
		"Test with arguments (Match, [10,50], default): " +
		"default level correctly setted"
	);
	// Test level param
	testEnemy = new Enemy(this.match, center, 1);
	deepEqual(testEnemy.getLevel(), 0,
		"Test with arguments (Match, [10,50], 1): " +
		"level correctly setted"
	);
});

test("Enemy.hit(hitrate)", function() {
	// With 0
	var healthE1 = this.testE1.health;
	this.testE1.hit(0);
	equal(healthE1, this.testE1.health, "Test with argument: (0)");
	// With a positive number
	var healthE2 = 100;
	var hitE2 = 10;
	this.testE2.health = healthE2;
	this.testE2.armor = 0;
	this.testE2.hit(hitE2);
	equal(healthE2 - hitE2, this.testE2.health,
		"Test with argument: (Number > 0)"
	);
	// With a negative number
	var healthE3 = this.testE3.health;
	this.testE3.armor = 0;
	var hitE3 = -10;
	this.testE3.hit(hitE3);
	equal(healthE3 - hitE3, this.testE3.health,
		"Test with argument: (Number < 0)"
	);
	// With Enemy.health
	var healthE4 = this.testE4.health;
	this.testE4.armor = 0;
	this.testE4.hit(healthE4);
	equal(this.testE4.health, 0, "Test with argument: (Enemy.health)");
	// With a number > Enemy.health
	var healthE5 = this.testE5.health;
	this.testE5.armor = 0;
	this.testE5.hit(healthE5 + 1);
	equal(this.testE5.health, 0, "Test with argument: (Enemy.health + 1)");
});


test("Enemy.setDetected(detected)", function() {
	// true argument
	this.testE1.setDetected(true);
	ok(this.testE1.isDetected(), "Test with argument: (true)");
	// truthy argument
	this.testE2.setDetected("this is a truthy argument");
	ok(this.testE2.isDetected(), "Test with argument: (truthy value)");
	// false argument
	this.testE3.setDetected(false);
	ok(! this.testE3.isDetected(), "Test with argument: (false)");
	// falsy argument
	this.testE4.setDetected(undefined); // undefined is falsy
	ok(! this.testE4.isDetected(), "Test with argument: (falsy value)");
});


test("Enemy.isDetected()", function() {
	// Test with a invisible enemy
	this.testE1.setDetected(false);  // To make sure it's really invisible
	ok(! this.testE1.isDetected(), "Test on a invisible enemy");
	// Test with a visible enemy
	this.testE2.setDetected(true);
	ok(this.testE2.isDetected(), "Test on a visible enemy");
});