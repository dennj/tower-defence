var gamejs = require("gamejs");
var ScenesDirector = require("js/scenes/scenesDirector").ScenesDirector;
var StartScene = require("js/scenes/startScene").StartScene;
var sounds = require("js/soundManager");

var surface = new gamejs.Surface(MAP_WIDTH, MAP_HEIGHT);
var testScenesDirector;
var testScene;

qModule('js/scenes/scenesDirector',{
	setup: function(){
		testScenesDirector = new ScenesDirector(surface);
		testScene = new StartScene(testScenesDirector);
	}
});

test("ScenesDirector(surface)", function() {
        ok(testScenesDirector instanceof ScenesDirector,
           "Test #1 object created correctly");
        throws(function() {var userScenesDirector = new ScenesDirector(undefined);},
                          TypeError, "Test #2 exception correctly throwed");
});
test("scenesDirector.getSurface()", function() {
        equal(testScenesDirector.getSurface(), surface,
              "Test with arguments: (surface)" +
	      ": surface is correct");
});

test("scenesDirector.getSoundManager()", function() {
        equal(testScenesDirector.getSoundManager(), testScenesDirector.soundManager,
              "Test with arguments: (soundManager)" +
	      ": soundManager is correct");
});

test("scenesDirector.start(startScene)", function() {
        testScenesDirector.start(testScene);
        ok(testScenesDirector.onAir, "Test #1 this director is on air");
        var currentScene = testScenesDirector.popScene();
        equal(currentScene, testScene, "Test #2 started the right scene");
        throws(
        	function(){
        		testScenesDirector.start(null);
        	}, TypeError, "Test #3 throw correctly the exception with an invalid input"
        );
});

test("ScenesDirector.changeScene(newScene)", function(){
	//creates test StartScenes
	var testScene1 = new StartScene(testScenesDirector);
	//insert in the scenesStack a scene
	testScenesDirector.pushScene(testScene1);
	testScenesDirector.changeScene(testScene);
	strictEqual(testScenesDirector.popScene(), testScene, "Test #1 newScene inserted correctly in the stack");
	strictEqual(testScenesDirector.scenesStack.length, 0, "Test #2 check if all scenes in the stack was removed");
	throws(
			function(){
				testScenesDirector.changeScene(null);
			}, TypeError, "Test #3 throw correctly the exception with an invalid input"
		);
});

test("ScenesDirector.pushScene(scene)", function(){
	testScenesDirector.pushScene(testScene);
	strictEqual(testScenesDirector.popScene(), testScene, "Test #1 scene inserted correctly in the stack");
	throws(
			function(){
				testScenesDirector.pushScene(null);
			}, TypeError, "Test #3 throw correctly the exception with an invalid input"
		);
});

test("ScenesDirector.popScene(scene)", function(){
	testScenesDirector.changeScene(testScene);
	strictEqual(testScenesDirector.popScene(), testScene, "Test #1 scene popped correctly");
});

test("ScenesDirector.getPreviousScene()", function(){
	var testScene2 = new StartScene(testScenesDirector);
	testScenesDirector.changeScene(testScene);
	testScenesDirector.pushScene(testScene2);
	strictEqual(testScenesDirector.getPreviousScene(), testScene, "Test #1 correct behaviour with standard situation");
});

test("ScenesDirector.getActiveScene()", function(){
	testScenesDirector.changeScene(testScene);
	strictEqual(testScenesDirector.getActiveScene(), testScene, "Test #1 with standard situation");
});