var Radar = require('js/radar').Radar;
var Match = require('js/match').Match;
var Map = require('js/map').Map;
var gamejs = require('gamejs');

qModule('js/radar',{
	setup: function(){
		this.testMatch = new Match();
		this.testMatch.setMap(new Map(this.testMatch, 100, 100));
		this.testMatch.start();
		this.testRadar = new Radar(this.testMatch, [50, 50]);
	}
});

test("Radar(Match, coords, startAngle, centralAngle)", function(){
	// Radar creation
	ok(this.testRadar instanceof Radar,
		"Test with arguments: (Match, [Number,Number], undefined, undefined)"
	);
	// Test that default values are setted
	equal(this.testRadar.getStartAngle(), 0,
		"Test with arguments: (Match, [Number,Number], undefined, undefined)" +
		": default startAngle is correct"
	);
	equal(this.testRadar.getCentralAngle(), Math.PI * 2,
		"Test with arguments: (Match, [Number,Number], undefined, undefined)" +
		": default centralAngle is correct"
	);
	// Test with user provided startAngle and centralAngle
	var userRadar = new Radar(this.testMatch, [0,0], Math.PI, Math.PI);
	equal(userRadar.getStartAngle(), Math.PI,
		"Test with arguments: (Match, [Number,Number], Number, Number)" +
		": startAngle is correct"
	);
	equal(userRadar.getCentralAngle(), Math.PI,
		"Test with arguments: (Match, [Number,Number], Number, Number)" +
		": centralAngle is correct"
	);
});

test("Radar.getRadius()", function(){
	equal(this.testRadar.getRadius(), 100,
			"Test with level 0 radar");
});

test("Radar.getAngularVelocity()", function(){
	equal(this.testRadar.getAngularVelocity(), Math.PI/2,
			"Test with level 0 radar");
});

test("Radar.getMatch()", function(){
	// Default match
	strictEqual(this.testRadar.getMatch(), this.testMatch,
		"Test with argument: (Match)"
	);
});

test("Radar.setStartAngle(angle)", function(){
	// angle = 0
	this.testRadar.setStartAngle(0);
	equal(this.testRadar.getStartAngle(), 0,
		"Test with argument: (Number = 0)"
	);
	// 0 < angle < Math.PI * 2
	this.testRadar.setStartAngle(Math.PI);
	equal(this.testRadar.getStartAngle(), Math.PI,
		"Test with argument: (0 < Number < Math.PI * 2)"
	);
	// angle = Math.PI*2
	this.testRadar.setStartAngle(Math.PI * 2);
	equal(this.testRadar.getStartAngle(), 0,
		"Test with argument: (Number = Math.PI * 2)"
	);
	// angle > Math.PI*2
	this.testRadar.setStartAngle(Math.PI * 3);
	equal(this.testRadar.getStartAngle(), Math.PI,
		"Test with argument: (Number > Math.PI * 2)"
	);
	// - Math.PI*2 < angle < 0
	this.testRadar.setStartAngle(-Math.PI);
	equal(this.testRadar.getStartAngle(), -Math.PI,
		"Test with argument: (- Math.PI * 2 < Number < 0)"
	);
	// angle = - Math.PI*2
	this.testRadar.setStartAngle(- Math.PI * 2);
	equal(this.testRadar.getStartAngle(), 0,
		"Test with argument: (Number = - Math.PI * 2)"
	);
	// angle < - Math.PI*2
	this.testRadar.setStartAngle(- Math.PI * 3);
	equal(this.testRadar.getStartAngle(), - Math.PI,
		"Test with argument: (Number < - Math.PI * 2)"
	);
	// not number
	throws(
		function(){ this.testRadar.setStartAngle(null); },
		TypeError,
		"Test with argument: (not angle)"
	);
});

test("Radar.getStartAngle()", function() {
	// 0 is the default value for new Radars
	equal(this.testRadar.getStartAngle(), 0, "Test on a normal radar");
});

test("Radar.setCentralAngle(angle)", function(){
	// angle = 0
	this.testRadar.setCentralAngle(0);
	equal(this.testRadar.getCentralAngle(), 0,
		"Test with argument: (Number = 0)"
	);
	// 0 < angle < Math.PI * 2
	this.testRadar.setCentralAngle(Math.PI);
	equal(this.testRadar.getCentralAngle(), Math.PI,
		"Test with argument: (0 < Number < Math.PI * 2)"
	);
	// angle = Math.PI * 2
	this.testRadar.setCentralAngle(Math.PI*2);
	equal(this.testRadar.getCentralAngle(), Math.PI*2,
		"Test with argument: (Math.PI * 2)"
	);
	//out of range angle
	throws(
		function(){ this.testRadar.setCentralAngle(10); },
		RangeError,
		"Test with argument: (out of range angle)"
	);
	//not angle
	throws(
		function() { this.testRadar.setCentralAngle(undefined); },
		TypeError,
		"Test with argument: (not angle)"
	);
});

test("Radar.getCentralAngle()", function() {
	// Math.PI * 2 is the default value for new radars
	equal(this.testRadar.getCentralAngle(), Math.PI * 2,
		"Test on a default radar"
	);
});

test("Radar.upgrade()", function() {
	// New radars start with level 0
	this.testRadar.upgrade();
	equal(this.testRadar.getLevel(), 1, "Test on a default radar");
});

test("Radar.getLevel()", function() {
	var currentLevel = 0; // New radars start with level 0
	equal(this.testRadar.getLevel(), 0, "Test on a default radar");
});