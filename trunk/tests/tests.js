var utils = require("js/utils");
var gamejs = require("gamejs");
var missions = require("js/missions");

// Preload of resources
utils.preload();

gamejs.ready(function(){
	require("tests/test_enemy");
	require("tests/test_map");
	require("tests/test_obstacle");
	require("tests/test_radar");
	require("tests/test_explosion");
	require("tests/test_match");
	require("tests/test_projectile");
	require("tests/test_turret");
	require("tests/test_utils");
	require("tests/test_gameEntity");
	require("tests/test_scenesDirector");
});