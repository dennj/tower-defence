var GameEntity = require('js/gameEntity').GameEntity;
var Match = require('js/match').Match;
var Map = require('js/map').Map;
var gamejs = require('gamejs');

qModule('js/gameEntity', {
	setup: function() {
		// Set match and map
		this.match = new Match();
		this.map = new Map(this.match, 800, 400);
		this.match.setMap(this.map);
		this.match.start();
		// Bootstrap the GameEntity
		this.center = [100, 100];
		this.upgradeList = [
			// Level 0
			{ image: IMAGE_ROOT + 'enemies/squaredEnemy.png',
			myProperty: 100,
			price: 100,
			resaleValue: 100
			},
			// Level 1
			{ image: IMAGE_ROOT + 'turrets/gunturret.png',
			myProperty: 1000,
			price: 200
			}
		];
		this.gE = new GameEntity(this.match, this.center, this.upgradeList);
	}
});

test("GameEntity(match, center, upgradeList, fillsMap, startingLvl)",
	function() {
	// Test constructor with defaults values (where applicable)
	ok(this.gE instanceof GameEntity,
		"Test with arguments " +
		"(Match, [Number, Number], upgradeList, default, default): " +
		"object correctly created."
	);
	deepEqual(this.gE.getMatch(), this.match,
		"Test with arguments " +
		"(Match, [Number, Number], upgradeList, default, default): " +
		"match correctly setted."
	);
	deepEqual(this.gE.getCenter(), this.center,
		"Test with arguments " +
		"(Match, [Number, Number], upgradeList, default, default): " +
		"center correctly setted."
	);
	deepEqual(this.gE.getUpgradeList(), this.upgradeList,
		"Test with arguments " +
		"(Match, [Number, Number], upgradeList, default, default): " +
		"upgradeList correctly setted."
	);
	deepEqual(this.gE.fillsMap(), false,
		"Test with arguments " +
		"(Match, [Number, Number], upgradeList, default, default): " +
		"default fillsMap correctly setted."
	);
	deepEqual(this.gE.getLevel(), 0,
		"Test with arguments " +
		"(Match, [Number, Number], upgradeList, default, default): " +
		"default startingLvl correctly setted."
	);
	// Test constructor with non defaults values (where applicable)
	var userGE = new GameEntity(this.match, this.center, this.upgradeList,
		true, 1
	);
	ok(userGE instanceof GameEntity,
		"Test with arguments " +
		"(Match, [Number, Number], upgradeList, Boolean, Number): " +
		"object correctly created."
	);
	deepEqual(userGE.getMatch(), this.match,
		"Test with arguments " +
		"(Match, [Number, Number], upgradeList, Boolean, Number): " +
		"match correctly setted."
	);
	deepEqual(userGE.getCenter(), this.center,
		"Test with arguments " +
		"(Match, [Number, Number], upgradeList, Boolean, Number): " +
		"center correctly setted."
	);
	deepEqual(userGE.getUpgradeList(), this.upgradeList,
		"Test with arguments " +
		"(Match, [Number, Number], upgradeList, Boolean, Number): " +
		"upgradeList correctly setted."
	);
	deepEqual(userGE.fillsMap(), true,
		"Test with arguments " +
		"(Match, [Number, Number], upgradeList, Boolean, Number): " +
		"fillsMap correctly setted."
	);
	deepEqual(userGE.getLevel(), 1,
		"Test with arguments " +
		"(Match, [Number, Number], upgradeList, Boolean, Number): " +
		"startingLvl correctly setted."
	);
	// Check that RangeError is throwed when using an invalid startingLvl
	throws(
		function() {
			new GameEntity(this.match, this.center, this.upgradeList,
				true, -1
			);
		},
		RangeError,
		"Test with arguments " +
		"(Match, [Number, Number], upgradeList, Boolean, Number < 0): " +
		"exception correctly throwed with a negative startingLvl."
	);
	throws(
		function() {
			var maxLevel = this.upgradeList.length - 1;
			new GameEntity(this.match, this.center, this.upgradeList,
				true, maxLevel +1
			);
		},
		RangeError,
		"Test with arguments " +
		"(Match, [Number, Number], upgradeList, Boolean, Number > maxLvl): " +
		"exception correctly throwed with a startingLvl > maximum level."
	);
});

test("GameEntity.getMatch()", function() {
	deepEqual(this.gE.getMatch(), this.match, "Works correctly!");
});

test("GameEntity.getMatchMap()", function() {
	strictEqual(this.gE.getMatchMap(), this.match.getMap(), "Works correctly!");
});

test("GameEntity.getShape()", function() {
	//test with circular shape
	deepEqual(this.gE.getShape(), SHAPE_CIRCULAR, "Works correctly!");
	//test with rectangular shape
	var userGE = new GameEntity(this.match, this.center, this.upgradeList,
		true, 0, SHAPE_RECTANGULAR, false);
	deepEqual(userGE.getShape(), SHAPE_RECTANGULAR, "Works correctly!");
});

test("GameEntity.getCenter()", function() {
	deepEqual(this.gE.getCenter(), this.center, "Works correctly!");
});

test("GameEntity.getObstructiveRadius()", function(){
	//test with circular shape
	deepEqual(this.gE.getObstructiveRadius(), 15, "Works correctly!");
	//test with rectangular shape
	var userGE = new GameEntity(this.match, this.center, this.upgradeList,
			true, 0, SHAPE_RECTANGULAR, false);
	throws(
			function(){ userGE.getObstructiveRadius(); },
			Error,
			"Test with SHAPE_RECTANGULAR");
});

test("GameEntity.getRect()", function() {
	// This one is somewhat useless
	deepEqual(this.gE.getRect(), this.gE.rect, "Works correctly!");
});

test("GameEntity.fillsMap()", function() {
	// Default is false
	deepEqual(this.gE.fillsMap(), false, "Works correctly!");
});

test("GameEntity.getLevel()", function() {
	// Default staring level is 0
	deepEqual(this.gE.getLevel(), 0, "Works correctly!");
});

test("GameEntity.isAtMaximumLevel()", function() {
	// Test on a GameEntity not at its maximum level
	ok( ! this.gE.isAtMaximumLevel(),
		"Works correctly on a GameEntity not at maximum level."
	);
	// Test on a GameEntity at maximum level
	this.gE.upgrade();
	ok( this.gE.isAtMaximumLevel(),
		"Works correctly on a GameEntity at maximum level."
	);
});

test("GameEntity.canUpgrade()", function() {
	var match = this.gE.getMatch();
	// Enough money and available level
	ok(this.gE.canUpgrade(),
		"Works with enough credits - GameEntity not at maximum level"
	);
	// Not enough money and GameEntity already at Maximum level
	match.changeCredit(- match.getCredit());  // 0 credits
	ok( ! this.gE.canUpgrade(),
		"Works without enough credits - GameEntity at maximum level"
	);
	// GameEntity not at maxium level but not enough money
	var gE = new GameEntity(this.match, this.center, this.upgradeList);
	ok( ! gE.canUpgrade(),
		"Works without enough credits - GameEntity not at maximum level"
	);
	// Enough money but GameEntity already at maximum level
	match.changeCredit(100000);
	this.gE.upgrade();
	ok( ! this.gE.canUpgrade(),
		"Works with enough credits - GameEntity at maximum level"
	);
});

test("GameEntity.getUpgradeList()", function() {
	deepEqual(this.gE.getUpgradeList(), this.upgradeList,
		"Works correctly!"
	);
});

test("GameEntity.getLevelSettings()", function() {
	// Test without arguments (get the current level settings)
	deepEqual(this.gE.getLevelSettings(), this.upgradeList[0],
		"Test with argument (undefined): " +
		"current level settings correctly returned."
	);
	// Test without a numeric argument
	deepEqual(this.gE.getLevelSettings(1), this.upgradeList[1],
		"Test with argument (Number): " +
		"settings for another level correctly returned."
	);
	// Tests with invalid level numbers
	throws (
		function() { this.gE.getLevelSettings(-1); },
		RangeError,
		"Test with argument (Number < 0): " +
		"exception correctly throwed."
	);
	throws (
		function() {
			var maxLevel = this.gE.getUpgradeList().length - 1;
			this.gE.getLevelSettings(maxLevel + 1);
		},
		RangeError,
		"Test with argument (Number > Maximum Level Allowed): " +
		"exception correctly throwed."
	);
	throws (
		function() { this.gE.getLevelSettings("I'm not a number"); },
		RangeError,
		"Test with argument (String): " +
		"exception correctly throwed."
	);
});

test("GameEntity.getUpgradePrice()", function() {
	// Upgrade cost defined
	var gEUpgradeCost = this.upgradeList[1].price;
	deepEqual(this.gE.getUpgradePrice(), gEUpgradeCost,
		"Works correctly on a GameEntity with upgrade price set."
	);
	// undefined price
	this.upgradeList.push(
		{image: IMAGE_ROOT + 'enemy.png'}
	);
	this.gE.upgrade();
	deepEqual(this.gE.getUpgradePrice(), 0,
		"Works correctly on a GameEntity with undefined price."
	);
	// Exception correctly throwed at maximum level
throws(
		function() { this.gE.gEUpgradeCost(); },
		Error,
		"Exception correctly trowed by a GameEntity already at its " +
		"maximum level."
	);
});

test("GameEntity.getBuyPrice(upgrdeList, level)", function(){
	//test with level 0 price
	var level=0;
	deepEqual(GameEntity.getBuyPrice(this.upgradeList, level),
			this.upgradeList[level].price,
			"Test with level 0 price.");
	//test with level 1 price
	level=1;
	var total=0;
	for(var i=0; i<=level; i++)
		total+=this.upgradeList[i].price;
	deepEqual(GameEntity.getBuyPrice(this.upgradeList, level),
			total, "Test with level 1 price.");
	//test with invalid level
	level=2;
	throws(
			function(){ GameEntity.getBuyPrice(this.upgradeList, level); },
			Error,
			"Test with a invalid level");
});

test("GameEntity.upgrade()", function() {
	// Test on a GameEntity having the same image for two levels
	var sameImageUL = [
		{image: IMAGE_ROOT + 'enemies/squaredEnemy.png'},
		{image: IMAGE_ROOT + 'enemies/squaredEnemy.png'}
	];
	var sameImageGE = new GameEntity(this.match, this.center, sameImageUL);
	sameImageGE.upgrade();
	deepEqual(sameImageGE.getLevel(), 1,
		"Test on a GameEntity with equal images across levels: " +
		"level number correctly increased."
	);
	throws(
		function() { sameImageGE.upgrade(); },
		Error,
		"Test on a GameEntity with equal images across levels: " +
		"exception correctly trowed while upgrading a GameEntity already " +
		"at its maximum level."
	);
	// Tests on this.gE (created with fillsMap = false, starts from level 0 and
	// the image of the second level is different from the one in the first
	// one)
	this.gE.upgrade();
	deepEqual(this.gE.getLevel(), 1,
		"Test on a GameEntity with different images across levels: " +
		"level number correctly increased."
	);
	// Check that the new image of the new level get loaded
	var newLevelImage = gamejs.image.load(this.upgradeList[1].image);
	deepEqual(this.gE.image, newLevelImage,
		"Test on a GameEntity with different images across levels: " +
		"new image correctly loaded."
	);
	// Check that that also the rect gets updated
	deepEqual(this.gE.getRect().center, this.center,
		"Test on a GameEntity with different images across levels: " +
		"new rect correctly setted."
	);
	var rectSize = [this.gE.getRect().width, this.gE.getRect().height ];
	deepEqual(rectSize, newLevelImage.getSize(),
		"Test on a GameEntity with different images across levels: " +
		"new rect correctly setted."
	);
	// Check that the exception is correctly trowed when trying to upgrade
	// again (this.gE has already reached its maximum level)
	throws(
		function() { this.gE.upgrade(); },
		Error,
		"Test on a GameEntity with different images across levels: " +
		"exception correctly trowed while upgrading a GameEntity already " +
		"at its maximum level."
	);
	// Test that the exception is throwed when there insn't enough credits
	// for the upgrade
	this.match.changeCredit( - this.match.getCredit() );  // 0 credits
	gE = new GameEntity(this.match, this.center, this.upgradeList,
		true /* Starting level is the default 0 */
	);
	throws (
		function() { gE.upgrade(); },
		Error,
		"Test on a GameEntity created with fillsMap=true and on Map: " +
		"exception correctly throwed if there isn't enough credits for it."
	);
});

test("GameEntity.isSaleable()", function(){
	//test with saleable GE
	deepEqual(this.gE.isSaleable(), true, "Test with saleable GE.");
	//test with not saleable GE
	var userGE = new GameEntity(this.match, this.center, this.upgradeList,
			true, 0, SHAPE_RECTANGULAR, false);
	deepEqual(userGE.isSaleable(), false, "Test with not saleable GE.");
});

test("GameEntity.getResaleValue()", function() {
	// Test on a GameEntity with resaleValue correctly defined
	var currentResaleValue = this.upgradeList[0].resaleValue;
	deepEqual(this.gE.getResaleValue(), currentResaleValue,
		"Test on a GameEntity with resaleValue defined."
	);
	// Test on a GameEntity with undefined resaleValue
	this.gE.upgrade();
	deepEqual(this.gE.getResaleValue(), 0,
		"Test on a GameEntity with undefined resaleValue."
	);
});

test("GameEntity.sell()", function(){
	//test with not saleable GE
	var userGE = new GameEntity(this.match, this.center, this.upgradeList,
			true, 0, SHAPE_RECTANGULAR, false);
	throws(
			function(){ userGE.sell(); },
			Error,
			"Test with not saleable GE.");
});

test("GameEntity.updateImage()", function() {
	// updateImage is called by the GameEntity constructor
	var imageUl = gamejs.image.load(this.upgradeList[0].image);
	deepEqual(this.gE.image, imageUl,
		"Works fine!"
	);
});