var Obstacle = require('js/obstacle').Obstacle;
var Radar = require('js/radar').Radar;
var GunTurret = require("js/turrets/gunTurret").GunTurret;
var LittleEnemy = require("js/enemies/littleEnemy").LittleEnemy;
var GiantEnemy = require("js/enemies/giantEnemy").GiantEnemy;


var MISSION_5 = exports.MISSION_5 = {
	startingCredits: 500,
	// Obstacles, Turrets or Radar automatically created at the start of
	// the mission.
	entities: [
		// Obstacles
		{constructor: Obstacle, center: [300, 80], level: 0 },
		{constructor: Obstacle, center: [500, 250], level: 2 },
		// Radars
		{constructor: Radar, coords: [200, 250], level: 0},
		{constructor: Radar, coords: [500, 100], level: 0},
		// Turrets
		{constructor: GunTurret, coords: [100, 100], level: 0},
		{constructor: GunTurret, coords: [300, 180], level: 0}
	],
	// array of levels(= waves)
	levels: []
};

// The first level
MISSION_5.levels[0] = [
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 0},
	{constructor: LittleEnemy, level: 0, position: [700, 250], time: 1000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 1500},
	{constructor: LittleEnemy, level: 0, position: [700, 260], time: 2000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 2500},
	{constructor: LittleEnemy, level: 0, position: [700, 270], time: 3000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 3500},
	{constructor: LittleEnemy, level: 0, position: [700, 280], time: 4000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 4500},
	{constructor: LittleEnemy, level: 0, position: [700, 290], time: 5000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 5500},
	{constructor: LittleEnemy, level: 0, position: [700, 300], time: 6000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 6500},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 7000},
	{constructor: LittleEnemy, level: 0, position: [700, 300], time: 7500},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 8000}
];

// The second level
MISSION_5.levels[1] = [
	{constructor: LittleEnemy, level: 0, position: [700, 200], time: 0},
	{constructor: LittleEnemy, level: 0, position: [700, 205], time: 1000},
	{constructor: LittleEnemy, level: 0, position: [700, 210], time: 2000},
	{constructor: LittleEnemy, level: 0, position: [700, 220], time: 3000},
	{constructor: LittleEnemy, level: 0, position: [700, 230], time: 3500},
	{constructor: LittleEnemy, level: 1, position: [700, 230], time: 4000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 4500},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 5000},
	{constructor: LittleEnemy, level: 1, position: [700, 250], time: 5500},
	{constructor: LittleEnemy, level: 0, position: [700, 250], time: 6000},
	{constructor: LittleEnemy, level: 0, position: [700, 260], time: 6500},
	{constructor: LittleEnemy, level: 1, position: [700, 260], time: 7000},
	{constructor: GiantEnemy, level: 0, position: [700, 250], time: 7500},
	{constructor: LittleEnemy, level: 0, position: [700, 270], time: 8000},
	{constructor: LittleEnemy, level: 1, position: [700, 280], time: 9000},
	{constructor: GiantEnemy, level: 0, position: [700, 250], time: 9500},
	{constructor: LittleEnemy, level: 0, position: [700, 290], time: 10000},
	{constructor: LittleEnemy, level: 1, position: [700, 290], time: 11000}
];

// The third level
MISSION_5.levels[2] = [
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 0},
	{constructor: GiantEnemy, level: 1, position: [700, 250], time: 3000},
	{constructor: GiantEnemy, level: 0, position: [700, 250], time: 5000},
	{constructor: GiantEnemy, level: 1, position: [700, 260], time: 8000},
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 10000}
];

// The fourth level
MISSION_5.levels[3] = [
	{constructor: GiantEnemy, level: 1, position: [700, 240], time: 0},
	{constructor: GiantEnemy, level: 1, position: [700, 240], time: 2000},
	{constructor: GiantEnemy, level: 1, position: [700, 300], time: 3500},
	{constructor: GiantEnemy, level: 1, position: [700, 240], time: 7000},
	{constructor: GiantEnemy, level: 1, position: [700, 300], time: 10000}
];