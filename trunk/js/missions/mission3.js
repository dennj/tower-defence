var Obstacle = require('js/obstacle').Obstacle;
var Radar = require('js/radar').Radar;
var GunTurret = require("js/turrets/gunTurret").GunTurret;
var LittleEnemy = require("js/enemies/littleEnemy").LittleEnemy;
var GiantEnemy = require("js/enemies/giantEnemy").GiantEnemy;
var ExplodingEnemy = require("js/enemies/explodingEnemy").ExplodingEnemy;


var MISSION_3 = exports.MISSION_3 = {
	startingCredits: 600,
	// Obstacles, Turrets or Radar automatically created at the start of
	// the mission.
	entities: [
		// Obstacles
		{constructor: Obstacle, center: [200, 80], level: 0 },
		{constructor: Obstacle, center: [350, 150], level: 1 },
		{constructor: Obstacle, center: [500, 300], level: 2 },
		// Radar
		{constructor: Radar, coords: [200, 250], level: 0},
		// Turret
		{constructor: GunTurret, coords: [100, 100], level: 1}
	],
	// array of levels(= waves)
	levels: []
};

// The first level
MISSION_3.levels[0] = [
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 0},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 1000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 1500},
	{constructor: ExplodingEnemy, level: 0, position: [700, 240], time: 2000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 2500},
	{constructor: ExplodingEnemy, level: 0, position: [700, 240], time: 3500},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 5000}
];

// The second level
MISSION_3.levels[1] = [
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 0},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 1000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 2000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 3000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 4000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 5000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 6000}
];

// The third level
MISSION_3.levels[2] = [
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 0},
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 4000},
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 9000},
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 12000}
];