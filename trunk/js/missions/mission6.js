var Obstacle = require("js/obstacle").Obstacle;
var LittleEnemy = require("js/enemies/littleEnemy").LittleEnemy;
var GiantEnemy = require("js/enemies/giantEnemy").GiantEnemy;
var SlowingTurret = require("js/turrets/slowingTurret").SlowingTurret;


var MISSION_6 = exports.MISSION_6 = {
	startingCredits: 600,
	// Obstacles, Turrets or Radar automatically created at the start of
	// the mission.
	entities: [
		// Obstacles
		{constructor: Obstacle, center: [350, 350], level: 2},
		{constructor: Obstacle, center: [420, 20], level: 2},
		{constructor: Obstacle, center: [550, 160], level: 2},
		//Turret
		{constructor: SlowingTurret, coords: [100, 100], level: 0}
	],
	// array of levels(= waves)
	levels: []
};

//The first level
MISSION_6.levels[0] = [
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 0},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 1000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 1500},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 2000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 2500},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 3000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 3500},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 4000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 4500},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 5000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 5500},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 6000}
];

//The second level
MISSION_6.levels[1] = [
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 0},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 1000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 2000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 3000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 4000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 5000},
	{constructor: LittleEnemy, level: 0, position: [700, 100], time: 6000},
	{constructor: LittleEnemy, level: 0, position: [700, 100], time: 7000},
	{constructor: LittleEnemy, level: 0, position: [700, 100], time: 8000},
	{constructor: LittleEnemy, level: 0, position: [700, 100], time: 9000},
	{constructor: LittleEnemy, level: 0, position: [700, 100], time: 10000}
];

//The third level
MISSION_6.levels[2] = [
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 0},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 1000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 2000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 3000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 4000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 5000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 6000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 7000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 8000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 9000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 10000}
];

//The fourth level
MISSION_6.levels[3] = [
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 0},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 1000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 2000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 3000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 4000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 5000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 6000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 7000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 8000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 9000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 10000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 11000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 17000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 13000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 14000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 15000}  
];

//The fifth level
MISSION_6.levels[4] = [
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 0},
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 1000},
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 2000},
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 3000},
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 4000},
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 5000},
	{constructor: GiantEnemy, level: 0, position: [700, 100], time: 6000},
	{constructor: GiantEnemy, level: 0, position: [700, 100], time: 7000},
	{constructor: GiantEnemy, level: 0, position: [700, 100], time: 8000},
	{constructor: GiantEnemy, level: 0, position: [700, 100], time: 9000},
	{constructor: GiantEnemy, level: 0, position: [700, 100], time: 10000}
];