var Obstacle = require('js/obstacle').Obstacle;
var Radar = require('js/radar').Radar;
var GunTurret = require("js/turrets/gunTurret").GunTurret;
var LittleEnemy = require("js/enemies/littleEnemy").LittleEnemy;
var GiantEnemy = require("js/enemies/giantEnemy").GiantEnemy;
var ExplodingEnemy = require("js/enemies/explodingEnemy").ExplodingEnemy;


var MISSION_4 = exports.MISSION_4 = {
	startingCredits: 600,
	// Obstacles, Turrets or Radar automatically created at the start of
	// the mission.
	entities: [
		// Obstacles
		{constructor: Obstacle, center: [300, 80], level: 0 },
		{constructor: Obstacle, center: [500, 250], level: 2 },
		// Radars
		{constructor: Radar, coords: [200, 250], level: 0},
		{constructor: Radar, coords: [500, 100], level: 0},
		// Turret
		{constructor: GunTurret, coords: [100, 100], level: 1}
	],
	// array of levels(= waves)
	levels: []
};

// The first level
MISSION_4.levels[0] = [
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 0},
	{constructor: LittleEnemy, level: 0, position: [700, 250], time: 1000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 1500},
	{constructor: LittleEnemy, level: 0, position: [700, 260], time: 2000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 2500},
	{constructor: LittleEnemy, level: 0, position: [700, 270], time: 3000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 3500},
	{constructor: LittleEnemy, level: 0, position: [700, 280], time: 4000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 4500}
];

// The second level
MISSION_4.levels[1] = [
	{constructor: LittleEnemy, level: 0, position: [700, 200], time: 0},
	{constructor: LittleEnemy, level: 0, position: [700, 205], time: 1000},
	{constructor: LittleEnemy, level: 0, position: [700, 210], time: 2000},
	{constructor: LittleEnemy, level: 0, position: [700, 220], time: 3000},
	{constructor: ExplodingEnemy, level: 0, position: [700, 240], time: 3500},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 4500},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 5000},
	{constructor: LittleEnemy, level: 0, position: [700, 250], time: 5500},
	{constructor: LittleEnemy, level: 0, position: [700, 250], time: 6000},
	{constructor: LittleEnemy, level: 0, position: [700, 260], time: 6500},
	{constructor: LittleEnemy, level: 1, position: [700, 260], time: 7000}
];

// The third level
MISSION_4.levels[2] = [
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 0},
	{constructor: GiantEnemy, level: 0, position: [700, 250], time: 3000},
	{constructor: GiantEnemy, level: 0, position: [700, 250], time: 5000}
];

// The fourth level
MISSION_4.levels[3] = [
	{constructor: GiantEnemy, level: 1, position: [700, 240], time: 0},
	{constructor: GiantEnemy, level: 1, position: [700, 240], time: 5000},
	{constructor: GiantEnemy, level: 1, position: [700, 300], time: 9000},
	{constructor: GiantEnemy, level: 1, position: [700, 240], time: 12000}
];