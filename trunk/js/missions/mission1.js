var Obstacle = require('js/obstacle').Obstacle;
var Radar = require('js/radar').Radar;
var LittleEnemy = require("js/enemies/littleEnemy").LittleEnemy;
var GiantEnemy = require("js/enemies/giantEnemy").GiantEnemy;
var ExplodingEnemy = require("js/enemies/explodingEnemy").ExplodingEnemy;


var MISSION_1 = exports.MISSION_1 = {
	startingCredits: 500,
	// Obstacles, Turrets or Radar automatically created at the start of
	// the mission.
	entities: [
		// Obstacles
		{constructor: Obstacle, center: [240, 80], level: 2 },
		{constructor: Obstacle, center: [350, 280], level: 1 },
		{constructor: Obstacle, center: [400, 350], level: 2 },
		// Radars
		{constructor: Radar, coords: [240, 270], level: 0 }
	],
	// array of levels(= waves)
	levels: []
};

// The first level
MISSION_1.levels[0] = [
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 0},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 1000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 2000},
	{constructor: ExplodingEnemy, level: 0, position: [700, 240], time: 0}
];

// The second level
MISSION_1.levels[1] = [
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 0},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 1000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 2000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 3000}
];

// The third level
MISSION_1.levels[2] = [
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 0},
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 5000}
];