var Obstacle = require('js/obstacle').Obstacle;
var Radar = require('js/radar').Radar;
var SlowingTurret = require("js/turrets/slowingTurret").SlowingTurret;
var LittleEnemy = require("js/enemies/littleEnemy").LittleEnemy;
var GiantEnemy = require("js/enemies/giantEnemy").GiantEnemy;


var MISSION_7 = exports.MISSION_7 = {
	startingCredits: 600,
	// Obstacles, Turrets or Radar automatically created at the start of
	// the mission.
	entities: [
		// Obstacles
		{constructor: Obstacle, center: [600, 400], level: 2},
		{constructor: Obstacle, center: [600, 100], level: 2},
		{constructor: Obstacle, center: [400, 200], level: 2},
		{constructor: Obstacle, center: [200, 260], level: 2}
	],
	// array of levels(= waves)
	levels: []
};

//The first level
MISSION_7.levels[0] = [
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 0},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 1000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 1500},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 2000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 2500},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 3000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 3500},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 4000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 4500},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 5000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 5500},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 6000}
];

//The second level
MISSION_7.levels[1] = [
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 0},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 1000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 2000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 3000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 4000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 5000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 6000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 7000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 8000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 9000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 10000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 11000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 17000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 13000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 14000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 15000}
];

//The third level
MISSION_7.levels[2] = [
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 0},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 1000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 2000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 3000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 4000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 5000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 6000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 7000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 8000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 9000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 10000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 11000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 17000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 13000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 14000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 15000}
];

//The fourth level
MISSION_7.levels[3] = [
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 0},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 1000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 2000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 3000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 4000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 5000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 6000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 7000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 8000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 9000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 10000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 11000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 17000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 13000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 14000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 15000},
	{constructor: LittleEnemy, level: 1, position: [700, 360], time: 16000},
	{constructor: LittleEnemy, level: 1, position: [700, 360], time: 17000},
	{constructor: LittleEnemy, level: 1, position: [700, 360], time: 18000},
	{constructor: LittleEnemy, level: 1, position: [700, 360], time: 19000},
	{constructor: LittleEnemy, level: 1, position: [700, 360], time: 20000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 21000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 22000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 23000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 24000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 25000}  
];

//The fifth level
MISSION_7.levels[4] = [
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 0},
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 1000},
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 2000},
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 3000},
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 4000},
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 5000},
	{constructor: GiantEnemy, level: 0, position: [700, 100], time: 6000},
	{constructor: GiantEnemy, level: 0, position: [700, 100], time: 7000},
	{constructor: GiantEnemy, level: 0, position: [700, 100], time: 8000},
	{constructor: GiantEnemy, level: 0, position: [700, 100], time: 9000},
	{constructor: GiantEnemy, level: 0, position: [700, 100], time: 10000},
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 11000},
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 17000},
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 13000},
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 14000},
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 15000},
	{constructor: GiantEnemy, level: 0, position: [700, 360], time: 16000},
	{constructor: GiantEnemy, level: 0, position: [700, 360], time: 17000},
	{constructor: GiantEnemy, level: 0, position: [700, 360], time: 18000},
	{constructor: GiantEnemy, level: 0, position: [700, 360], time: 19000},
	{constructor: GiantEnemy, level: 0, position: [700, 360], time: 20000},
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 21000},
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 22000},
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 23000},
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 24000},
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 25000}  
];