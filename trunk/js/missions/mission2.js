var Obstacle = require('js/obstacle').Obstacle;
var Radar = require('js/radar').Radar;
var GunTurret = require('js/turrets/gunTurret').GunTurret;
var LittleEnemy = require("js/enemies/littleEnemy").LittleEnemy;
var GiantEnemy = require("js/enemies/giantEnemy").GiantEnemy;
var ExplodingEnemy = require("js/enemies/explodingEnemy").ExplodingEnemy;


var MISSION_2 = exports.MISSION_2 = {
	startingCredits: 550,
	// Obstacles, Turrets or Radar automatically created at the start of
	// the mission.
	entities: [
		{constructor: Obstacle, center: [240, 80], level: 2 },
		{constructor: Obstacle, center: [240, 240], level: 2 },
		{constructor: Obstacle, center: [520, 350], level: 2 },
		{constructor: Obstacle, center: [520, 190], level: 2 },
		// Radar
		{constructor: Radar, coords: [120, 370], level: 0 },
		// Turret
		{constructor: GunTurret, coords: [450, 40], level:0 }
	],
	// array of levels(= waves)
	levels: []
};

// The first level
MISSION_2.levels[0] = [
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 0},
	{constructor: ExplodingEnemy, level: 0, position: [700, 240], time: 0},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 1000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 2000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 3000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 4000}
];

// The second level
MISSION_2.levels[1] = [
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 0},
	{constructor: LittleEnemy, level: 0, position: [700, 220], time: 0},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 1000},
	{constructor: LittleEnemy, level: 0, position: [700, 220], time: 1000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 2000},
	{constructor: LittleEnemy, level: 0, position: [700, 220], time: 2000},
];

// The third level
MISSION_2.levels[2] = [
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 0},
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 4000},
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 8000}
];