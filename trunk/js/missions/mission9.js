var Obstacle = require('js/obstacle').Obstacle;
var Radar = require('js/radar').Radar;
var LittleEnemy = require("js/enemies/littleEnemy").LittleEnemy;
var GiantEnemy = require("js/enemies/giantEnemy").GiantEnemy;


var MISSION_9 = exports.MISSION_9 = {
	startingCredits: 800,
	// Obstacles, Turrets or Radar automatically created at the start of
	// the mission.
	entities: [
		// Obstacles
		{constructor: Obstacle, center: [120, 50], level: 1},
		{constructor: Obstacle, center: [120, 100], level: 1},
		{constructor: Obstacle, center: [120, 150], level: 1},
		{constructor: Obstacle, center: [120, 200], level: 1},
		{constructor: Obstacle, center: [350, 350], level: 1},
		{constructor: Obstacle, center: [350, 400], level: 1},
		{constructor: Obstacle, center: [350, 300], level: 1},
		{constructor: Obstacle, center: [350, 250], level: 1},
		{constructor: Obstacle, center: [580, 50], level: 1},
		{constructor: Obstacle, center: [580, 100], level: 1},
		{constructor: Obstacle, center: [580, 150], level: 1},
		{constructor: Obstacle, center: [580, 200], level: 1},
		//Radar
		{constructor: Radar, coords: [200, 350], level: 0}
	],
	// array of levels(= waves)
	levels: []
};

//The first level
MISSION_9.levels[0] = [
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 0},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 1000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 1500},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 2000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 2500},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 3000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 3500},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 4000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 4500},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 5000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 5500},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 6000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 6500},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 7000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 8500},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 9000},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 9500},
	{constructor: LittleEnemy, level: 0, position: [700, 240], time: 10000}
];

//The second level
MISSION_9.levels[1] = [
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 0},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 1000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 2000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 3000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 4000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 5000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 6000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 7000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 8000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 9000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 10000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 11000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 17000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 13000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 14000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 15000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 15500},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 16000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 16500},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 17000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 17500},
];

//The third level
MISSION_9.levels[2] = [
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 0},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 1000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 2000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 3000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 4000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 5000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 6000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 7000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 8000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 9000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 10000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 11000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 12000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 13000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 14000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 15000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 15000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 15000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 15000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 15000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 15000}
];

//The fourth level
MISSION_9.levels[3] = [
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 0},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 1000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 2000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 3000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 4000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 5000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 6000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 7000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 8000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 9000},
	{constructor: LittleEnemy, level: 1, position: [700, 100], time: 10000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 11000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 17000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 13000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 14000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 15000},
	{constructor: LittleEnemy, level: 1, position: [700, 360], time: 16000},
	{constructor: LittleEnemy, level: 1, position: [700, 360], time: 17000},
	{constructor: LittleEnemy, level: 1, position: [700, 360], time: 18000},
	{constructor: LittleEnemy, level: 1, position: [700, 360], time: 19000},
	{constructor: LittleEnemy, level: 1, position: [700, 360], time: 20000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 21000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 22000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 23000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 24000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 25000},
	{constructor: LittleEnemy, level: 1, position: [700, 360], time: 25000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 25000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 25000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 25000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 25000},
	{constructor: LittleEnemy, level: 1, position: [700, 240], time: 25000} 
];

//The fifth level
MISSION_9.levels[4] = [
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 0},
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 1000},
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 2000},
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 3000},
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 4000},
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 5000},
	{constructor: GiantEnemy, level: 0, position: [700, 100], time: 6000},
	{constructor: GiantEnemy, level: 0, position: [700, 100], time: 7000},
	{constructor: GiantEnemy, level: 0, position: [700, 100], time: 8000},
	{constructor: GiantEnemy, level: 0, position: [700, 100], time: 9000},
	{constructor: GiantEnemy, level: 0, position: [700, 100], time: 10000},
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 11000},
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 17000},
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 13000},
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 14000},
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 15000},
	{constructor: GiantEnemy, level: 0, position: [700, 360], time: 16000},
	{constructor: GiantEnemy, level: 0, position: [700, 360], time: 17000},
	{constructor: GiantEnemy, level: 0, position: [700, 360], time: 18000},
	{constructor: GiantEnemy, level: 0, position: [700, 360], time: 19000},
	{constructor: GiantEnemy, level: 0, position: [700, 360], time: 20000},
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 21000},
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 22000},
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 23000},
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 24000},
	{constructor: GiantEnemy, level: 0, position: [700, 240], time: 25000}  
];

//The sixth level
MISSION_9.levels[5] = [
	{constructor: GiantEnemy, level: 1, position: [700, 240], time: 0},
	{constructor: GiantEnemy, level: 1, position: [700, 240], time: 1000},
	{constructor: GiantEnemy, level: 1, position: [700, 240], time: 2000},
	{constructor: GiantEnemy, level: 1, position: [700, 240], time: 3000},
	{constructor: GiantEnemy, level: 1, position: [700, 240], time: 4000},
	{constructor: GiantEnemy, level: 1, position: [700, 240], time: 5000},
	{constructor: GiantEnemy, level: 1, position: [700, 100], time: 6000},
	{constructor: GiantEnemy, level: 1, position: [700, 100], time: 7000},
	{constructor: GiantEnemy, level: 1, position: [700, 100], time: 8000},
	{constructor: GiantEnemy, level: 1, position: [700, 100], time: 9000},
	{constructor: GiantEnemy, level: 1, position: [700, 100], time: 10000},
	{constructor: GiantEnemy, level: 1, position: [700, 240], time: 11000},
	{constructor: GiantEnemy, level: 1, position: [700, 240], time: 17000},
	{constructor: GiantEnemy, level: 1, position: [700, 240], time: 13000},
	{constructor: GiantEnemy, level: 1, position: [700, 240], time: 14000},
	{constructor: GiantEnemy, level: 1, position: [700, 240], time: 15000},
	{constructor: GiantEnemy, level: 1, position: [700, 360], time: 16000},
	{constructor: GiantEnemy, level: 1, position: [700, 360], time: 17000},
	{constructor: GiantEnemy, level: 1, position: [700, 360], time: 18000},
	{constructor: GiantEnemy, level: 1, position: [700, 360], time: 19000},
	{constructor: GiantEnemy, level: 1, position: [700, 360], time: 20000},
	{constructor: GiantEnemy, level: 1, position: [700, 240], time: 21000},
	{constructor: GiantEnemy, level: 1, position: [700, 240], time: 22000},
	{constructor: GiantEnemy, level: 1, position: [700, 240], time: 23000},
	{constructor: GiantEnemy, level: 1, position: [700, 240], time: 24000},
	{constructor: GiantEnemy, level: 1, position: [700, 240], time: 25000},
	{constructor: GiantEnemy, level: 1, position: [700, 360], time: 25000},
	{constructor: GiantEnemy, level: 1, position: [700, 240], time: 25000},
	{constructor: GiantEnemy, level: 1, position: [700, 240], time: 25000},
	{constructor: GiantEnemy, level: 1, position: [700, 240], time: 25000},
	{constructor: GiantEnemy, level: 1, position: [700, 240], time: 25000},
	{constructor: GiantEnemy, level: 1, position: [700, 240], time: 25000} 
];