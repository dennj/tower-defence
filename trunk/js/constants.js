var DISPLAY_WIDTH = 800;
var DISPLAY_HEIGHT = 480;

var WIDTH_OFFSET = 70;//space for in-game menu
var HEIGHT_OFFSET = 0;//space for info

var MAP_WIDTH = DISPLAY_WIDTH - WIDTH_OFFSET;
var MAP_HEIGHT = DISPLAY_HEIGHT - HEIGHT_OFFSET;

//defining the planet's area, it's a vertical line
var PLANET_HEIGHT = Math.round(MAP_HEIGHT / 2);//this is the height of the planet's area
var PLANET_CENTER = Math.round(MAP_HEIGHT / 2);//the planet's area is centered at half-height


var X = 0;
var Y = 1;
var WIDTH = 0;
var HEIGHT = 1;

var FPS = 60;
var DAY_TIME = 60000;//time to build, in ms
var LOAD_TIME = 2000;//change level time
var INITIAL_HEALTH = 50;

var SHAPE_RECTANGULAR = "rectangular";
var SHAPE_CIRCULAR = "circular";

// continue button
var CONT_BTN_X = 540;// topleft X
var CONT_BTN_Y = 410;// topleft Y
var CONT_BTN_W = 350;// width
var CONT_BTN_H = 75;// height

//in explodingEnemy the probability (of 100%) of generating a little enemy (used 100-q)
var PROBABILITY_NOT_NEW_ENEMY = 30;

//ion cannon beam size in pixels
var CANNON_BEAM_SIZE = 50;

//nuke bomb range
var NUKE_RADIUS = 200;