var gamejs = require("gamejs");
var Sprite = require("gamejs/sprite").Sprite;
var draw = require("gamejs/draw");
var utils = require("js/utils");
var NightAction = require("js/nightAction").NightAction;
var Flare = require("js/nightActions/flare").Flare;
var IonCannon = require("js/nightActions/ionCannon").IonCannon;
var NukeBomb = require("js/nightActions/nukeBomb").NukeBomb;

/**
 * NightStoreScene, the scene where player can buy actions to execute
 * them during the night.
 */

/**
 * Creates a NightStoreScene object, here the player can buy NightAction objects.
 * @param {ScenesDirector} director The ScenesDirector object that directs the game.
 * @param {Match} match The Match object representing the game.
 *
 * @constructor
 */
var NightStoreScene = exports.NightStoreScene = function(director, match){
	this.director = director;
	this.match = match;
	this.credit = match.getCredit();
	this.level = match.getMission();

	//background image
	this.background = gamejs.image.load(IMAGE_ROOT + "startBackground.png");

	// arrow image(go back)
	this.backArrow = new Sprite();
	this.backArrow.image = gamejs.image.load(IMAGE_ROOT + "backArrow.png");
	this.backArrow.rect = new gamejs.Rect([0, 0], this.backArrow.image.getSize());

	this.bigFont = new gamejs.font.Font("30px Verdana");
	this.smallFont = new gamejs.font.Font("15px Verdana");
	// font with a background color for the name
	this.evidentFont = new gamejs.font.Font("15px Verdana", "#C0C0C0");

	// 40 px is the space reserved to the back arrow and the credit
	// 2 shelf
	this.SHELF_HEIGHT = Math.round((DISPLAY_HEIGHT - 40) / 2);

	// 5 slot for shelf
	// squared slot -> need only one dimension
	this.SLOT_DIM = Math.round(DISPLAY_WIDTH / 5);

	//to place some buttons and the price
	this.DETAILS_HEIGHT = this.SHELF_HEIGHT - this.SLOT_DIM;

	/***** SLOT INITIALIZATION *****/

	this.slot = [];
	for(i = 0; i < 10; i++) {
		this.slot[i] = {
			filled : false,
			buyable : false,
			showInfo : false,
			buySprite : new Sprite(),
			infoSprite : new Sprite()
		};

		this.slot[i].infoSprite.image = gamejs.image.load(IMAGE_ROOT + "storeInfo.png");
		this.slot[i].infoSprite.image.setAlpha(0.5);
		var dims = this.slot[i].infoSprite.image.getSize();
		var cX = this.SLOT_DIM * (i % 5) + 20;
		var cY = 0;
		if(i < 5) {
			cY = 40 + this.SLOT_DIM + (this.SHELF_HEIGHT - this.SLOT_DIM) / 2;
		}
		else {
			cY = 40 + this.SHELF_HEIGHT + this.SLOT_DIM + (this.SHELF_HEIGHT - this.SLOT_DIM) / 2;
		}
		var center = [cX, cY];
		var topleft = utils.calcTopleft(center, dims);
		this.slot[i].infoSprite.rect = new gamejs.Rect(topleft, dims);

		this.slot[i].buySprite.image = gamejs.image.load(IMAGE_ROOT + "storeBuy.png");
		this.slot[i].buySprite.image.setAlpha(0.5);
		dims = this.slot[i].buySprite.image.getSize();
		cX = this.SLOT_DIM * (i % 5) + 140;
		center = [cX, cY];
		topleft = utils.calcTopleft(center, dims);
		this.slot[i].buySprite.rect = new gamejs.Rect(topleft, dims);

	}

	/***** BUYABLE ACTIONS *****/

	this.actions = [];
	if(this.level > 0) {
		// Flare, detects all the enemies in the map
		var flareObj = new Flare(this.match);
		var flare = {
			name : "Flare",
			image : flareObj.image,
			price : flareObj.getBuyPrice(),
			time : flareObj.getReloadTime(),
			object : flareObj
		};
		this.actions.push(flare);
		
		// IonCannon, destroy all enemies in a row
		var ionCannonObj = new IonCannon(this.match);
		var ionCannon = {
			name : "Ion Cannon",
			image : ionCannonObj.image,
			price : ionCannonObj.getBuyPrice(),
			time : ionCannonObj.getReloadTime(),
			object : ionCannonObj
		};
		this.actions.push(ionCannon);
		
		// NukeBomb, destroy all enemies in a large area
		var nukeBombObj = new NukeBomb(this.match);
		var nukeBomb = {
			name : "Nuke Bomb",
			image : nukeBombObj.image,
			price : nukeBombObj.getBuyPrice(),
			time : nukeBombObj.getReloadTime(),
			object : nukeBombObj
		};
		this.actions.push(nukeBomb);
	}

	// set the slot buttons
	for(i = 0; i < this.slot.length; i++) {
		if( ! this.actions[i]) {
			break;
		}
		this.slot[i].filled = true;
		this.slot[i].buyable = this.match.hasCredit(this.actions[i].price);
		if(this.slot[i].buyable) {
			this.slot[i].buySprite.image.setAlpha(0);
		}
		this.slot[i].showInfo = this.actions[i].constructor.getInfo ? true : false;
		if(this.slot[i].showInfo) {
			this.slot[i].infoSprite.image.setAlpha(0);
		}
	}
};

/**
 * Updates the scene and its contents, managing the various events.
 */
NightStoreScene.prototype.update = function() {
	var events = new gamejs.event.get();
	var prevScene = this.director.getPreviousScene();

	for (i = 0; i < events.length; i++) {
		var currEvent = events[i];

		// process only mouse click
		if (currEvent.type == gamejs.event.MOUSE_UP) {

			if (this.backArrow.rect.collidePoint(currEvent.pos)) {
				// click on the back arrow
				this.director.popScene();

				// no more to do here
				return;
			}

			// checks if player click on a button of a slot
			for(i = 0; i < this.actions.length; i++) {
				// check for click on the buy button
				if(this.slot[i].buyable && this.slot[i].buySprite.rect.collidePoint(currEvent.pos)) {
					// pass the selected item to the previous scene
					prevScene.setNightAction(this.actions[i].object);
					// go to the previous scene
					this.director.popScene();

					// no more to do here
					return;
				}

				// check for click on the info button
				if(this.slot[i].showInfo && this.slot[i].infoSprite.rect.collidePoint(currEvent.pos)) {
					// create a InfoScene and make it the active scene
					var infoScene = new infoSceneMod.InfoScene();
					this.director().pushScene(infoScene);

					// no more to do here
					return;
				}
			}
		}
	}
};


/**
 * Draws the scenes into the director surface.
 */
NightStoreScene.prototype.draw = function() {
	var surface = this.director.getSurface();
	surface.blit(this.background);

	// to go back
	this.backArrow.draw(surface);

	// credits
	var creditSurface = this.bigFont.render("Credit : " + this.credit, "#FFFFFF");
	utils.drawAtMiddle(creditSurface, surface, [DISPLAY_WIDTH / 2, 20]);

	/***** DRAW THE SLOTS *****/

	// start from the left border
	var topleftX = 0;
	// live a 40 px space for the back arrow
	var topleftY = 40;
	var topleft = [topleftX, topleftY];
	var slotDims = [this.SLOT_DIM, this.SLOT_DIM];

	var imageX = Math.round(this.SLOT_DIM / 2);
	var imageY = 40 + Math.round(this.SLOT_DIM / 2);
	var imageCenter = [imageX, imageY];

	var detailsX = Math.round(this.SLOT_DIM / 2);
	var detailsY = 40 + this.SLOT_DIM + 10;
	var detailsCenter = [detailsX, detailsY];

	var nameX = Math.round(this.SLOT_DIM / 2);
	var nameY = 40 + 10;
	var nameCenter = [nameX, nameY];

	for (i = 0; i < this.slot.length; i++) {
		// draw the slot border
		draw.rect(surface, "#C0C0C0", new gamejs.Rect(topleft, slotDims), 2);
		// draw the buttons
		this.slot[i].infoSprite.draw(surface);
		this.slot[i].buySprite.draw(surface);

		if (this.actions[i]) {
			// draw item
			var imgDims = this.actions[i].image.getSize();
			var imgTopleft = utils.calcTopleft(imageCenter, imgDims);
			surface.blit(this.actions[i].image, imgTopleft);

			// draw price
			var secTime = (this.actions[i].time / 1000).toFixed(1);
			var price = this.actions[i].price + "$| " + secTime + "s";
			var detailsSurface = this.smallFont.render("Price : " + price, "#FFFFFF");
			utils.drawAtMiddle(detailsSurface, surface, detailsCenter);

			// draw name
			var name = this.actions[i].name;
			var nameSurface = this.evidentFont.render(name, "#0000FF");
			utils.drawAtMiddle(nameSurface, surface, nameCenter);
		}

		// for the next cycle
		topleft[X] += this.SLOT_DIM;
		imageCenter[X] += this.SLOT_DIM;
		detailsCenter[X] += this.SLOT_DIM;
		nameCenter[X] += this.SLOT_DIM;
		
		if (i == 4) {
			// go to the second shelf
			topleft[X] = 0;
			topleft[Y] += this.SHELF_HEIGHT;

			imageCenter[X] = Math.round(this.SLOT_DIM / 2);
			imageCenter[Y] += this.SHELF_HEIGHT;

			detailsCenter[X] = Math.round(this.SLOT_DIM / 2);
			detailsCenter[Y] += this.SHELF_HEIGHT;

			nameCenter[X] = Math.round(this.SLOT_DIM / 2);
			nameCenter[Y] += this.SHELF_HEIGHT;
		}
	}
};