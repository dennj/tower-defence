var gamejs = require("gamejs");
var Sprite = require("gamejs/sprite").Sprite;
var Font = require("gamejs/font").Font;
var utils = require("js/utils");
var PreLevelScene = require("js/scenes/preLevelScene").PreLevelScene;

/**
 * PauseScene, is the scene where you can stop playing
 */

/**
 * Creates a PauseScene object
 *
 * @param {ScenesDirector} director The SceneDirector that directs the game
 * @param {Match} match The match object that representing the game
 *
 * @constructor
 */
var PauseScene = exports.PauseScene = function(director, match) {
	this.director = director;
	this.match = match;

	// background image
	this.background = gamejs.image.load(IMAGE_ROOT + "pauseScene.png");

	// continue rect
	this.continueRect = new gamejs.Rect([222, 209], [328, 51]);

	// restart level rect
	this.restartLevelRect = new gamejs.Rect([134, 303], [547, 51]);

	// restart mission rect
	this.restartMissionRect = new gamejs.Rect([86, 402], [636, 51]);
};

/**
 * Updates the scene managing the events.
 *
 * @param {number} msDuration The time past from the last call, in ms
 */
PauseScene.prototype.update = function(msDuration) {
	var events = gamejs.event.get();

	for (i = 0; i < events.length; i++) {
		var currentEvent = events[i];
		// process only the mouse click
		if (currentEvent.type == gamejs.event.MOUSE_UP) {
			var point = currentEvent.pos;
			if (this.continueRect.collidePoint(point)) {
				// click on continue
				// removes the pause scene
				this.director.popScene();
				break;
			}

			if (this.restartLevelRect.collidePoint(point)) {
				// click on restart level
				// reset the match to this level
				this.match.restartLevel();
				// restart the scene
				this.director.changeScene(
					new PreLevelScene(this.director, this.match)
				);
				break;
			}

			if (this.restartMissionRect.collidePoint(point)) {
				// click on restart mission
				// reset the match to the first level of this mission
				this.match.restartMission();
				// restart the scene
				this.director.changeScene(
					new PreLevelScene(this.director, this.match)
				);
				break;
			}
		}
		else if (currentEvent.type == gamejs.event.MOUSE_MOTION){
			var point = currentEvent.pos;
			if (this.continueRect.collidePoint(point))
				document.getElementsByTagName("body")[0].style.cursor ='pointer';
			else if (this.restartLevelRect.collidePoint(point))
				document.getElementsByTagName("body")[0].style.cursor ='pointer';
			else if (this.restartMissionRect.collidePoint(point))
				document.getElementsByTagName("body")[0].style.cursor ='pointer';
			else
				document.getElementsByTagName("body")[0].style.cursor ='default';
		}
	}
};

/**
 * Draws the scene and its contents in the director Surface.
 */
PauseScene.prototype.draw = function() {
	var surface = this.director.getSurface();
	surface.blit(this.background);
};