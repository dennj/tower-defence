var gamejs = require("gamejs");
var Sprite = require("gamejs/sprite").Sprite;
var Font = require("gamejs/font").Font;
var daySceneModule = require("js/scenes/dayScene");
var utils = require("js/utils");

/**
 * PreLevelScene, is the scene where you wait next level.
 */

/**
 * Creates a PreLevelScene object.
 *
 * @param {ScenesDirector} director The SceneDirector that directs the game.
 * @param {Match} match The Match object that representing the current game.
 *
 * @constructor
 */
var PreLevelScene = exports.PreLevelScene = function(director, match) {
	this.director = director;
	this.match = match;
	this.timer = LOAD_TIME;
	// background image
	this.background = gamejs.image.load(IMAGE_ROOT + "startBackground.png");
};

/**
 * Updates the scene menaging an event
 *
 * @param {number] msDuration The time past from the last call, in ms
 */
PreLevelScene.prototype.update = function(msDuration) {
	this.timer -= msDuration;
	if(this.timer <= 0) {
		this.director.changeScene(new daySceneModule.DayScene(this.director, this.match));
		return;
	}
};

/**
 * Draws the scene and its contents in the director Surface.
 */
PreLevelScene.prototype.draw = function() {
	var surface = this.director.getSurface();
	surface.blit(this.background);
	// mission number
	utils.drawAtMiddle(
		new Font("72px Verdana").render("MISSION " + this.match.getMission(), "#FF0000"),
		surface,
		[Math.round(DISPLAY_WIDTH / 2), Math.round(DISPLAY_HEIGHT / 2) - 50]
	);
	// level number
	utils.drawAtMiddle(
		new Font("72px Verdana").render("LEVEL " + this.match.getLevel(), "#FF0000"),
		surface,
		[Math.round(DISPLAY_WIDTH / 2), Math.round(DISPLAY_HEIGHT / 2) + 50]
	);
};