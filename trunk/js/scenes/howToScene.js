var gamejs = require("gamejs");
var Sprite = require("gamejs/sprite").Sprite;

/**
 * HowToScene, is the scene where you learn how to play the game.
 */

/**
 * Creates a HowToScene object
 *
 * @param {ScenesDirector} director The SceneDirector that directs the game
 *
 * @constructor
 */
var HowToScene = exports.HowToScene = function(director) {
	this.director = director;
	
	// background image
	this.background1 = gamejs.image.load(IMAGE_ROOT + "tutorial1.png");
	this.background2 = gamejs.image.load(IMAGE_ROOT + "tutorial2.png");
	this.background3 = gamejs.image.load(IMAGE_ROOT + "tutorial3.png");
	
	//the current tutorial page (1,2,3)
	this.countTutorial = 1;
	
	// sets the rect over the continue button
	this.continueButton = new gamejs.Rect(
		[CONT_BTN_X, CONT_BTN_Y],// topleft
		[CONT_BTN_W, CONT_BTN_H]// dimensions
	);
};


/**
 * Updates the scene managing an event
 *
 * @param {number} msDuration The time past from the last call, in ms
 */
HowToScene.prototype.update = function(msDuration) {
	var events = new gamejs.event.get();
	for(i = 0; i < events.length; i++) {
		var currentEvent = events[i];
		if(currentEvent.type == gamejs.event.MOUSE_UP && this.continueButton.collidePoint(currentEvent.pos)) {
			if(this.countTutorial >= 3){
				// the tutorial is ended, go back
				this.director.popScene();
			}
			this.countTutorial ++;
		}
		else if(currentEvent.type == gamejs.event.MOUSE_MOTION)
			if (this.continueButton.collidePoint(currentEvent.pos))
				document.getElementsByTagName("body")[0].style.cursor ='pointer';
			else
				document.getElementsByTagName("body")[0].style.cursor ='default';
	}
};

/**
 * Draws the scene and its contents in the director Surface.
 */
HowToScene.prototype.draw = function() {
	var surface = this.director.getSurface();
	
	var image;
	//we'll change the background
	switch(this.countTutorial){
		case 1: image = this.background1;
				break;
		case 2: image = this.background2;
				break;
		case 3: image = this.background3;
				break;
	}
	
	surface.blit(image);
};