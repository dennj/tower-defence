var gamejs = require("gamejs");
var Draw = require("gamejs/draw");
var missions = require("js/missions");
var enemyModule = require("js/enemy");
var NightScene = require("js/scenes/nightScene").NightScene;
var PreLevelScene = require("js/scenes/preLevelScene").PreLevelScene;

/**
 * LoadingNightScene, the scene where create all the enemies
 * in a wave, so the game will not lag.
 */

/**
 * Creates a LoadingNightScene object to load all the enemies.
 *
 * @param {ScenesDirector} director The ScenesDirector object that directs the game.
 * @param {Match} match The Match object where the enemies will work.
 *
 * @constructor
 */
var LoadingNightScene = exports.LoadingNightScene = function(director, match) {
	this.director = director;
	this.match = match;
	this.isDrawn = false;
	this.createdWave = [];
	var lvl = match.getLevel() - 1;
	var miss = match.getMission() - 1;
	this.requiredWave = missions.MISSIONS[miss].levels[lvl];
	this.waveIndex = 0;
	this.waveLength = this.requiredWave.length;

	//background image
	this.background = gamejs.image.load(IMAGE_ROOT + "loadingNightBackground.png");
};

/**
 * Updates the scene: creates an enemy at every call, till the wave is completed.
 * Once completed the wave, it sorts the wave by the enemies time of creation
 * and passes it to a NightScene object, that become the active scene.
 *
 * @param {number} msDuration The time past from the last call, in ms.
 */
LoadingNightScene.prototype.update = function(msDuration) {
	// even if we don't need check the events, we must call
	// this to delete the useless events
	gamejs.event.get();

	if(this.waveIndex >= this.waveLength) {
		// all the enemies are created
		// sort for required time of creation
		this.createdWave.sort(function(obj1, obj2) {
			return obj1.time - obj2.time;
		});

		// no more need this scene, if we go back from night
		// we want to go to the day scene
		this.director.popScene();
		// go to night
		this.director.pushScene(new NightScene(this.director, this.match, this.createdWave));

		return;
	}

	// starts only if the background is already drawn
	if(this.isDrawn) {
		// create one enemy
		var requiredEnemy = this.requiredWave[this.waveIndex++];
		
		try {
			// creates the enemy (and finds the path, if any)
			var createdEnemy = new requiredEnemy.constructor(
				this.match, requiredEnemy.position, requiredEnemy.level
			);
			
			// adds the created enemy to the wave
			this.createdWave.push({enemy : createdEnemy, time : requiredEnemy.time});
		}
		catch (e) {
			if(e.message == "Can't find a path.") {
				// there's no way to reach the planet
				// show an alert to the player
				alert("Enemies can't find a path. " +
					"\nWhy didn't you leave them a navigable path? " +
					"Are you scared?\nYou must restart this level.");
				
				// restart this level
				this.match.restartLevel();
				// restart the scene
				this.director.changeScene(
					new PreLevelScene(this.director, this.match)
				);
			}
			else {
				throw e;
			}
		}
	}
};

/**
 * Draws the scene in the director Surface: a background and a progress bar.
 */
LoadingNightScene.prototype.draw = function() {
	var surface = this.director.getSurface();

	// draws the background
	surface.blit(this.background);

	// draws the progress bar
	var startPoint = [0, (DISPLAY_HEIGHT * 2) / 3];
	var progress = Math.round((this.waveIndex / this.waveLength) * DISPLAY_WIDTH);
	var endPoint = [progress, (DISPLAY_HEIGHT * 2) / 3];
	Draw.line(surface, "#00FF00", startPoint, endPoint, 5);

	this.isDrawn = true;
};