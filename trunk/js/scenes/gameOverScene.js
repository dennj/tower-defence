var gamejs = require("gamejs");
var Sprite = require("gamejs/sprite").Sprite;
var startSceneModule = require("js/scenes/startScene");

/**
 * GameOverScene, is the scene where you are redirected after losing all the health points.
 */

/**
 * Creates a GameOverScene object.
 *
 * @param {ScenesDirector} director The SceneDirector that directs the game
 *
 * @constructor
 */
var GameOverScene = exports.GameOverScene = function(director) {
	this.director = director;
	
	// background image
	this.background = gamejs.image.load(IMAGE_ROOT + "gameOver.png");
	
	// sets the rect over the continue button
	this.continueButton = new gamejs.Rect(
		[CONT_BTN_X, CONT_BTN_Y],// topleft
		[CONT_BTN_W, CONT_BTN_H]// dimensions
	);
};

/**
 * Updates the scene managing an event
 */
GameOverScene.prototype.update = function() {
	var events = gamejs.event.get();
		
	for (i = 0; i < events.length; i++) {
		// process the mouse click
		var currentEvent = events[i];
		var point = currentEvent.pos;
		
		if (currentEvent.type == gamejs.event.MOUSE_UP) {
			if (this.continueButton.collidePoint(point)) {
				// click on continue
				this.director.start(new startSceneModule.StartScene(this.director));
				return;
			}
		}
		// changes the cursor if it is on a button
		else if (currentEvent.type == gamejs.event.MOUSE_MOTION) {
			if(this.continueButton.collidePoint(point)) {
				document.getElementsByTagName("body")[0].style.cursor ='pointer';
			}
			else{
				document.getElementsByTagName("body")[0].style.cursor ='default';
			}
		}
	}
};

/**
 * Draws the scene and its contents in the director Surface.
 */
GameOverScene.prototype.draw = function() {
	var surface = this.director.getSurface();
	surface.blit(this.background);
};