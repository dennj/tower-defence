var gamejs = require("gamejs");
var PreLevelScene = require("js/scenes/preLevelScene").PreLevelScene;

/**
 * LevelClearedScene, it is the scene where you are redirected after
 * completing a mission or a level.
 */

/**
 * Creates a LevelClearedScene object.
 *
 * @param {ScenesDirector} director The ScenesDirector that directs the game.
 * @param {Match} match The Match object representing the current game.
 *
 * @constructor
 */
var LevelClearedScene = exports.LevelClearedScene = function(director , match) {
	this.director = director;
	this.match = match;
	
	// sets the background image
	if(match.isLastLevel()){
		// display the mission cleared image if a mission is been completed...
		this.background = gamejs.image.load(IMAGE_ROOT + "missionCompleted.png");
	}
	else{
		// ...or just the level cleared image
		this.background = gamejs.image.load(IMAGE_ROOT + "levelCleared.png");
	}
	
	// sets the rect over the continue button
	this.continueButton = new gamejs.Rect(
		[CONT_BTN_X, CONT_BTN_Y],// topleft
		[CONT_BTN_W, CONT_BTN_H]// dimensions
	);
};

/**
 * Updates the scene managing an event
 */
LevelClearedScene.prototype.update = function() {
	var events = gamejs.event.get();
	
	for (i = 0; i < events.length; i++) {
		// process the mouse click
		var currentEvent = events[i];
		var point = currentEvent.pos;
		
		if (currentEvent.type == gamejs.event.MOUSE_UP) {
			if (this.continueButton.collidePoint(point)) {
				// click on continue
				this.director.changeScene(new PreLevelScene(this.director, this.match));
				return;
			}
		}
		// changes the cursor if it is on a button
		else if (currentEvent.type == gamejs.event.MOUSE_MOTION) {
			if(this.continueButton.collidePoint(point)) {
				document.getElementsByTagName("body")[0].style.cursor ='pointer';
			}
			else{
				document.getElementsByTagName("body")[0].style.cursor ='default';
			}
		}
	}
};

/**
 * Draws the scene and its contents in the director Surface.
 */
LevelClearedScene.prototype.draw = function() {
	var surface = this.director.getSurface();
	surface.blit(this.background);
};