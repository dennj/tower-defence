var gamejs = require("gamejs");
var daySceneModule = require("js/scenes/dayScene");
var Match = require("js/match").Match;
var Map = require("js/map").Map;
var PreLevelScene = require("js/scenes/preLevelScene").PreLevelScene;
var HowToSceneModule = require("js/scenes/howToScene").HowToScene;

/**
 * StartScene, the scene to show at start. It contains a simple menu.
 */

/**
 * Creates a StartScene object, it will draw and manage a start menu.
 * @param {ScenesDirector} director The ScenesDirector object that directs the game.
 *
 * @constructor
 */
var StartScene = exports.StartScene = function(director) {
	this.director = director;

	// background image
	this.background = gamejs.image.load(IMAGE_ROOT + "main.png");

	// new game rect
	this.newGame = new gamejs.Rect([50, 196], [350, 75]);

	// how to play rect
	this.howToPlay = new gamejs.Rect([50, 300], [350, 75]);

	// credits rect
	this.credits = new gamejs.Rect([50, 400], [350, 75]);
};

/**
 * Updates the scene changing the active scene according to the mouse click.
 *
 * @param {number} msDuration The time past from the last call, in ms.
 */
StartScene.prototype.update = function(msDuration) {
	var events = gamejs.event.get();

	for (i = 0; i < events.length; i++) {
		var currentEvent = events[i];
		// process the mouse click
		if (currentEvent.type == gamejs.event.MOUSE_UP) {
			var point = currentEvent.pos;
			if (this.newGame.collidePoint(point)) {
				// click on new game
				var match = new Match(this.director.getSoundManager());
				match.start();
				this.director.changeScene(new PreLevelScene(this.director, match));
				break;
			}

			if (this.howToPlay.collidePoint(point)) {
				// click on how to play
				this.director.pushScene(new HowToSceneModule(this.director));
				break;
			}

			if (this.credits.collidePoint(point)) {
				// click on credits
				// this.director.pushScene(new CreditsScene(this.director));
				break;
			}
		
		// process the mouse motion (to change the cursor)
		}else if (currentEvent.type == gamejs.event.MOUSE_MOTION) {
			var point = currentEvent.pos;
			if(this.newGame.collidePoint(point))
				document.getElementsByTagName("body")[0].style.cursor ='pointer';
			else if(this.howToPlay.collidePoint(point))
				document.getElementsByTagName("body")[0].style.cursor ='pointer';
			else if(this.credits.collidePoint(point))
				document.getElementsByTagName("body")[0].style.cursor ='pointer';
			else
				document.getElementsByTagName("body")[0].style.cursor ='default';
		}
	}
};

/**
 * Draws the scene in the director Surface:
 * a background and a simple menu.
 */
StartScene.prototype.draw = function() {
	var surface = this.director.getSurface();
	surface.blit(this.background);
};