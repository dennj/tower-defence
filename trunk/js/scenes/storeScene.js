var gamejs = require("gamejs");
var Sprite = require("gamejs/sprite").Sprite;
var draw = require("gamejs/draw");
var Radar = require("js/radar").Radar;
var Obstacle = require("js/obstacle").Obstacle;
var GunTurret = require("js/turrets/gunTurret").GunTurret;
var LaserTurret = require("js/turrets/laserTurret").LaserTurret;
var SlowingTurret = require("js/turrets/slowingTurret").SlowingTurret;
var RocketTurret = require("js/turrets/rocketTurret").RocketTurret;
var Mine = require("js/turrets/mine").Mine;
// game entity module
var geMod = require("js/gameEntity");
var utils = require("js/utils");

/**
 * StoreScene, the scene where player can buy obstacles, turrets and radars
 */

/**
 * Creates a StoreScene object, here the player can buy obstacles, turrets and radars.
 * @param {ScenesDirector} director The ScenesDirector object that directs the game.
 * @param {Match} match The Match object representing the game.
 *
 * @constructor
 */
var StoreScene = exports.StoreScene = function(director, match){
	this.director = director;
	this.match = match;
	this.credit = match.getCredit();
	this.level = match.getMission();

	//background image
	this.background = gamejs.image.load(IMAGE_ROOT + "startBackground.png");

	// arrow image(go back)
	this.backArrow = new Sprite();
	this.backArrow.image = gamejs.image.load(IMAGE_ROOT + "backArrow.png");
	this.backArrow.rect = new gamejs.Rect([0, 0], this.backArrow.image.getSize());

	this.bigFont = new gamejs.font.Font("30px Verdana");
	this.smallFont = new gamejs.font.Font("15px Verdana");
	// font with a background color for the name
	this.evidentFont = new gamejs.font.Font("15px Verdana", "#C0C0C0");

	// 40 px is the space reserved to the back arrow and the credit
	// 2 shelf
	this.SHELF_HEIGHT = Math.round((DISPLAY_HEIGHT - 40) / 2);

	// 5 slot for shelf
	// squared slot -> need only one dimension
	this.SLOT_DIM = Math.round(DISPLAY_WIDTH / 5);

	//to place some buttons and the price
	this.DETAILS_HEIGHT = this.SHELF_HEIGHT - this.SLOT_DIM;

	/***** SLOT INITIALIZATION *****/

	this.slot = [];
	for(i = 0; i < 10; i++) {
		this.slot[i] = {
			filled : false,
			buyable : false,
			showInfo : false,
			buySprite : new Sprite(),
			infoSprite : new Sprite()
		};

		this.slot[i].infoSprite.image = gamejs.image.load(IMAGE_ROOT + "storeInfo.png");
		this.slot[i].infoSprite.image.setAlpha(0.5);
		var dims = this.slot[i].infoSprite.image.getSize();
		var cX = this.SLOT_DIM * (i % 5) + 20;
		var cY = 0;
		if(i < 5) {
			cY = 40 + this.SLOT_DIM + (this.SHELF_HEIGHT - this.SLOT_DIM) / 2;
		}
		else {
			cY = 40 + this.SHELF_HEIGHT + this.SLOT_DIM + (this.SHELF_HEIGHT - this.SLOT_DIM) / 2;
		}
		var center = [cX, cY];
		var topleft = utils.calcTopleft(center, dims);
		this.slot[i].infoSprite.rect = new gamejs.Rect(topleft, dims);

		this.slot[i].buySprite.image = gamejs.image.load(IMAGE_ROOT + "storeBuy.png");
		this.slot[i].buySprite.image.setAlpha(0.5);
		dims = this.slot[i].buySprite.image.getSize();
		cX = this.SLOT_DIM * (i % 5) + 140;
		center = [cX, cY];
		topleft = utils.calcTopleft(center, dims);
		this.slot[i].buySprite.rect = new gamejs.Rect(topleft, dims);

	}

	/***** BUYABLE ITEMS *****/

	this.items = [];
	if(this.level > 0) {
		// can buy base turret and radar

		// Radar, the base one
		var radar1 = {
			name : "Radar",
			image : gamejs.image.load(IMAGE_ROOT + "radar.png"),
			price : geMod.GameEntity.getBuyPrice(Radar.UPGRADE_LIST),
			constructor : Radar
		};
		this.items.push(radar1);

		// Gun Turret, the base one
		var turret1 = {
			name : "Gun Turret",
			image : gamejs.image.load(IMAGE_ROOT + "turrets/gunturret.png"),
			price : geMod.GameEntity.getBuyPrice(GunTurret.UPGRADE_LIST),
			constructor : GunTurret
		};
		this.items.push(turret1);

		if(this.level >= 3) {
			// can buy all turrets and radar

			// Laser Turret
			var turret2 = {
				name : "Laser Turret",
				image : gamejs.image.load(IMAGE_ROOT + "turrets/laserturret.png"),
				price : geMod.GameEntity.getBuyPrice(LaserTurret.UPGRADE_LIST),
				constructor : LaserTurret
			};
			this.items.push(turret2);

			// Slowing Turret
			var turret2 = {
				name : "Slowing Turret",
				image : gamejs.image.load(IMAGE_ROOT + "turrets/slowingturret.png"),
				price : geMod.GameEntity.getBuyPrice(SlowingTurret.UPGRADE_LIST),
				constructor : SlowingTurret
			};
			this.items.push(turret2);

			// Rocket Turret
			var turret2 = {
				name : "Rocket Turret",
				image : gamejs.image.load(IMAGE_ROOT + "turrets/rocketturret.png"),
				price : geMod.GameEntity.getBuyPrice(RocketTurret.UPGRADE_LIST),
				constructor : RocketTurret
			};
			this.items.push(turret2);
			
			//Mine
			var mine1 = {
					name : "Mine",
					image : gamejs.image.load(IMAGE_ROOT + "turrets/mine.png"),
					price : geMod.GameEntity.getBuyPrice(Mine.UPGRADE_LIST),
					constructor : Mine
				};
			this.items.push(mine1);

			if(this.level >= 5) {
				// can buy all (obstacles too)
				
				//sets the images for each level
				var smallObsImage = Obstacle.LEVELS_LIST[0].image;
				var medObsImage = Obstacle.LEVELS_LIST[1].image;
				var bigObsImage = Obstacle.LEVELS_LIST[2].image;
				
				//sets the upgradeList for each level
				var smallUpgradeList = [Obstacle.LEVELS_LIST[0]];
				var medUpgradeList = [Obstacle.LEVELS_LIST[1]];
				var bigUpgradeList = [Obstacle.LEVELS_LIST[2]];

				// Small Obstacle
				var obstacle1 = {
					name : "Small Obstacle",
					image : gamejs.image.load(smallObsImage),
					price : geMod.GameEntity.getBuyPrice(smallUpgradeList),
					constructor : Obstacle,
					level : 0
				};
				this.items.push(obstacle1);

				// Medium Obstacle
				var obstacle2 = {
					name : "Medium Obstacle",
					image : gamejs.image.load(medObsImage),
					price : geMod.GameEntity.getBuyPrice(medUpgradeList),
					constructor : Obstacle,
					level : 1
				};
				this.items.push(obstacle2);

				// Big Obstacle
				var obstacle3 = {
					name : "Big Obstacle",
					image : gamejs.image.load(bigObsImage),
					price : geMod.GameEntity.getBuyPrice(bigUpgradeList),
					constructor : Obstacle,
					level : 2
				};
				this.items.push(obstacle3);
			}
		}
	}

	// set the slot buttons
	for(i = 0; i < this.slot.length; i++) {
		if( ! this.items[i]) {
			break;
		}
		this.slot[i].filled = true;
		this.slot[i].buyable = this.match.hasCredit(this.items[i].price);
		if(this.slot[i].buyable) {
			this.slot[i].buySprite.image.setAlpha(0);
		}
		this.slot[i].showInfo = this.items[i].constructor.getInfo ? true : false;
		if(this.slot[i].showInfo) {
			this.slot[i].infoSprite.image.setAlpha(0);
		}
	}
};

/**
 * Updates the scene and its contents, managing the various events.
 */
StoreScene.prototype.update = function() {
	var events = new gamejs.event.get();
	var prevScene = this.director.getPreviousScene();

	for (i = 0; i < events.length; i++) {
		var currEvent = events[i];

		// process only mouse click
		if (currEvent.type == gamejs.event.MOUSE_UP) {

			if (this.backArrow.rect.collidePoint(currEvent.pos)) {
				// click on the back arrow
				this.director.popScene();

				// no more to do here
				return;
			}

			// checks if player click on a button of a slot
			for(i = 0; i < this.items.length; i++) {
				// check for click on the buy button
				if(this.slot[i].buyable && this.slot[i].buySprite.rect.collidePoint(currEvent.pos)) {
					// pass the selected item to the previous scene
					prevScene.setToPlaceItem(this.items[i]);
					// go to the previous scene
					this.director.popScene();

					// no more to do here
					return;
				}

				// check for click on the info button
				if(this.slot[i].showInfo && this.slot[i].infoSprite.rect.collidePoint(currEvent.pos)) {
					// create a InfoScene and make it the active scene
					var infoScene = new infoSceneMod.InfoScene();
					this.director().pushScene(infoScene);

					// no more to do here
					return;
				}
			}
		}
	}
};


/**
 * Draws the scenes into the director surface.
 */
StoreScene.prototype.draw = function() {
	var surface = this.director.getSurface();
	surface.blit(this.background);

	// to go back
	this.backArrow.draw(surface);

	// credits
	var creditSurface = this.bigFont.render("Credit : " + this.credit, "#FFFFFF");
	utils.drawAtMiddle(creditSurface, surface, [DISPLAY_WIDTH / 2, 20]);

	/***** DRAW THE SLOTS *****/

	// start from the left border
	var topleftX = 0;
	// live a 40 px space for the back arrow
	var topleftY = 40;
	var topleft = [topleftX, topleftY];
	var slotDims = [this.SLOT_DIM, this.SLOT_DIM];

	var imageX = Math.round(this.SLOT_DIM / 2);
	var imageY = 40 + Math.round(this.SLOT_DIM / 2);
	var imageCenter = [imageX, imageY];

	var detailsX = Math.round(this.SLOT_DIM / 2);
	var detailsY = 40 + this.SLOT_DIM + 10;
	var detailsCenter = [detailsX, detailsY];

	var nameX = Math.round(this.SLOT_DIM / 2);
	var nameY = 40 + 10;
	var nameCenter = [nameX, nameY];

	for (i = 0; i < this.slot.length; i++) {
		// draw the slot border
		draw.rect(surface, "#C0C0C0", new gamejs.Rect(topleft, slotDims), 2);
		// draw the buttons
		this.slot[i].infoSprite.draw(surface);
		this.slot[i].buySprite.draw(surface);

		if (this.items[i]) {
			// draw item
			var imgDims = this.items[i].image.getSize();
			var imgTopleft = utils.calcTopleft(imageCenter, imgDims);
			surface.blit(this.items[i].image, imgTopleft);

			// draw price
			var price = this.items[i].price;
			var detailsSurface = this.smallFont.render("Price : " + price, "#FFFFFF");
			utils.drawAtMiddle(detailsSurface, surface, detailsCenter);

			// draw name
			var name = this.items[i].name;
			var nameSurface = this.evidentFont.render(name, "#0000FF");
			utils.drawAtMiddle(nameSurface, surface, nameCenter);
		}

		// for the next cycle
		topleft[X] += this.SLOT_DIM;
		imageCenter[X] += this.SLOT_DIM;
		detailsCenter[X] += this.SLOT_DIM;
		nameCenter[X] += this.SLOT_DIM;
		
		if (i == 4) {
			// go to the second shelf
			topleft[X] = 0;
			topleft[Y] += this.SHELF_HEIGHT;

			imageCenter[X] = Math.round(this.SLOT_DIM / 2);
			imageCenter[Y] += this.SHELF_HEIGHT;

			detailsCenter[X] = Math.round(this.SLOT_DIM / 2);
			detailsCenter[Y] += this.SHELF_HEIGHT;

			nameCenter[X] = Math.round(this.SLOT_DIM / 2);
			nameCenter[Y] += this.SHELF_HEIGHT;
		}
	}
};