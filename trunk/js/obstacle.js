var GameEntity = require('js/gameEntity').GameEntity;
var gamejs = require('gamejs');
var utils = require("js/utils");

/*
 * OBSTACLE: an object representing an obstacle
 */

/**
 * Creates a new obstacle
 *
 * @param {array} center The coordinates as vector [x, y]
 * @param {number} radius The radius of the obstacle
 * @param {Number} level The level of the obstacle [default 0]
 *
 * @extends Sprite
 * @constructor
 */
var Obstacle = exports.Obstacle = function(match, center, level) {
	if ( ! utils.validIndex(level, this.constructor.LEVELS_LIST)) {
		throw new RangeError('Obstacle: invalid starting level!');
	}
	
	this.creationLevel = level;
	
	//the list of levels reachable with upgrade
	//obstacle can't upgrade -> 1 level
	var upgradeList = this.constructor.LEVELS_LIST.slice(level, level + 1);
	
	Obstacle.superConstructor.call(this, match, center, upgradeList,
		true, 0);
};

/**
 * Extending GameEntity with Obstacle
 */
GameEntity.extend(Obstacle);

/**
 * The list of available levels.
 * NOTE that this isn't a list of available upgrade, so when we have to
 * pass the upgradeList to the constructor of GameEntity we pass to it just
 * one level; this make the obstacle not upgradable, as it's right.
 */
Obstacle.LEVELS_LIST = [
	// Level 0
	{ image: IMAGE_ROOT + "obstacle0.png", price: 150, resaleValue: 75},
	// Level 1
	{ image: IMAGE_ROOT + "obstacle1.png", price: 300, resaleValue: 150},
	// Level 2
	{ image: IMAGE_ROOT + "obstacle2.png", price: 500, resaleValue: 300}
];

/**
 * Returns the radius of the Obstacle
 *
 * @returns {number} The radius of the Obstacle
 */
Obstacle.prototype.getRadius = function() {
	return this.radius;
};

/**
 * Returns the level in which this obstacle was created.
 *
 * @return {number} The level in which this obstacle was created.
 */
Obstacle.prototype.getCreationLevel= function() {
	return this.creationLevel;
};