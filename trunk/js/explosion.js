var gamejs = require("gamejs");
var match = require("js/match");

/**
 * A explosion.
 *
 * @param {Match} match The Match object where this object will work.
 * @param {[numeric, numeric]} the position of the explosion.
 * @param {numeric} the explosion duration.
 */
var Explosion = exports.Explosion = function(match, rect, duration) {
	Explosion.superConstructor.apply(this, arguments);

	this.match = match;
	this.rect = rect;
	this.start_duration = this.duration = duration; // duration of the explosion on the screen

	this.image = gamejs.image.load(IMAGE_ROOT + "explosion.png");
};

/**
 * Explosion extend Sprite.
 */
gamejs.utils.objects.extend(Explosion, gamejs.sprite.Sprite);

/**
 * Return the match where this explosion work.
 *
 * @return {Match} The match where this explosion work.
 */
Explosion.prototype.getMatch = function() {
	return this.match;
};

/**
 * Return the current explosion duration.
 *
 * @return {numeric} The duration of the explosion.
 */
Explosion.prototype.getDuration = function() {
	return this.duration;
};

/**
 *
 *
 * We don't need to update the position of the explosion in this moment.
 *
 *
 */
Explosion.prototype.update = function(msDuration) {
	this.duration -= msDuration;
	this.image.setAlpha(1 - this.duration / this.start_duration);
	if (this.duration < 0)
		this.kill();
};

