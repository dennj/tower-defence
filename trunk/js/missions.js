var MISSIONS = exports.MISSIONS = [
	require('js/missions/mission1').MISSION_1,
	require('js/missions/mission2').MISSION_2,
	require('js/missions/mission3').MISSION_3,
	require('js/missions/mission4').MISSION_4,
	require('js/missions/mission5').MISSION_5,
	require('js/missions/mission6').MISSION_6,
	require('js/missions/mission7').MISSION_7,
	require('js/missions/mission8').MISSION_8,
	require('js/missions/mission9').MISSION_9,
	require('js/missions/mission10').MISSION_10
];