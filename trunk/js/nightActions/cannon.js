var gamejs = require("gamejs");
var utils = require("js/utils");
var NightAction = require("js/nightAction").NightAction;

/**
 * Cannon : this NightAction, once executed, destroys every enemy on a line in the map.
 */

/**
 * Creates a Cannon object.
 * 
 * @param {Match} match The Match object where this Cannon will be executed.
 * 
 * @constructor
 */
var Cannon = exports.Cannon = function (match) {
	Cannon.superConstructor.call(this, match);
	this.image = gamejs.image.load(IMAGE_ROOT + "nightActions/cannon.png");
	this.executionTime = 1000;// 1 sec
	this.reloadTime = 120000;// 120 sec = 2 min
	this.buyPrice = 500;
	this.executionPoint = null;
	
	// not modify this variable in when extending
	this.timer = this.executionTime;
	
	this.beamStart = null;
};

/**
 * Cannon extends NightAction.
 */
gamejs.utils.objects.extend(Cannon, NightAction);

/**
 * Executes the Cannon in the last set position, setting
 * executed = true and executionPoint = position.
 */
Cannon.prototype.execute = function () {
	this.beamStart = [WIDTH_OFFSET, this.position[1] - CANNON_BEAM_SIZE/2];
	var range = new gamejs.Rect(this.beamStart[0], this.beamStart[1], DISPLAY_WIDTH, CANNON_BEAM_SIZE);
	var allEnemies = this.match.getMap().getEnemies().sprites();
	var nEnemies = allEnemies.length;
	for(var i=0; i < nEnemies; i++){
		if(range.collideRect(allEnemies[0].rect)){
			allEnemies[0].hit(allEnemies[0].health + allEnemies[0].armor); //kill the enemy
		}
	}
	
	this.executionPoint = this.selectedPosition;
	this.executed = true;
};

/**
 * Draw the Cannon graphic effects
 */
Cannon.prototype.draw = function (surface) {
	if( ! this.isOver()) {
		if( ! this.executed) {
			// the position is referred to the map, but we have to draw
			// in the main surface, mind the offset
			var drawingPos = [
				this.position[X] + WIDTH_OFFSET,
				this.position[Y] + HEIGHT_OFFSET
			];
			utils.drawAtMiddle(this.image, surface, drawingPos);
		}else{
			//rectangle at the core of the beam
			var range = new gamejs.Rect(this.beamStart[0], this.beamStart[1], DISPLAY_WIDTH, CANNON_BEAM_SIZE);
			gamejs.draw.rect(
				surface, '#FFFFFF', range);
			
			//blue-to-white gradient (2px is the standard line height)
			var start = this.beamStart;
			var end = [DISPLAY_WIDTH, this.beamStart[1]];
			gamejs.draw.line(surface,'#0000C2', [start[0],start[1]], [end[0],end[1]], 2);
			gamejs.draw.line(surface,'#0000E0', [start[0],start[1]+ 2], [end[0],end[1]+ 2], 2);
			gamejs.draw.line(surface,'#0020C2', [start[0],start[1]+ 4], [end[0],end[1]+ 4], 2);
			gamejs.draw.line(surface,'#0000FF', [start[0],start[1]+ 6], [end[0],end[1]+ 6], 2);
			gamejs.draw.line(surface,'#001FE2', [start[0],start[1]+ 8], [end[0],end[1]+ 8], 2);
			gamejs.draw.line(surface,'#0041C2', [start[0],start[1]+10], [end[0],end[1]+10], 2);
			gamejs.draw.line(surface,'#0062E1', [start[0],start[1]+12], [end[0],end[1]+12], 2);
			gamejs.draw.line(surface,'#0080FF', [start[0],start[1]+14], [end[0],end[1]+14], 2);
			gamejs.draw.line(surface,'#2181FF', [start[0],start[1]+16], [end[0],end[1]+16], 2);
			gamejs.draw.line(surface,'#5291EF', [start[0],start[1]+18], [end[0],end[1]+18], 2);
			gamejs.draw.line(surface,'#82C0FF', [start[0],start[1]+20], [end[0],end[1]+20], 2);
			gamejs.draw.line(surface,'#A1E2FF', [start[0],start[1]+22], [end[0],end[1]+22], 2);
			gamejs.draw.line(surface,'#C0E1FF', [start[0],start[1]+24], [end[0],end[1]+24], 2);
			gamejs.draw.line(surface,'#C2EFFF', [start[0],start[1]+26], [end[0],end[1]+26], 2);
			gamejs.draw.line(surface,'#E0F1FF', [start[0],start[1]+28], [end[0],end[1]+28], 2);
			gamejs.draw.line(surface,'#E0FFFF', [start[0],start[1]+30], [end[0],end[1]+30], 2);
			//second part
			var cbs = CANNON_BEAM_SIZE;
			gamejs.draw.line(surface,'#0000C2', [start[0],start[1] +cbs], [end[0],end[1] +cbs], 2);
			gamejs.draw.line(surface,'#0000E0', [start[0],start[1]- 2 +cbs], [end[0],end[1]- 2 +cbs], 2);
			gamejs.draw.line(surface,'#0020C2', [start[0],start[1]- 4 +cbs], [end[0],end[1]- 4 +cbs], 2);
			gamejs.draw.line(surface,'#0000FF', [start[0],start[1]- 6 +cbs], [end[0],end[1]- 6 +cbs], 2);
			gamejs.draw.line(surface,'#001FE2', [start[0],start[1]- 8 +cbs], [end[0],end[1]- 8 +cbs], 2);
			gamejs.draw.line(surface,'#0041C2', [start[0],start[1]-10 +cbs], [end[0],end[1]-10 +cbs], 2);
			gamejs.draw.line(surface,'#0062E1', [start[0],start[1]-12 +cbs], [end[0],end[1]-12 +cbs], 2);
			gamejs.draw.line(surface,'#0080FF', [start[0],start[1]-14 +cbs], [end[0],end[1]-14 +cbs], 2);
			gamejs.draw.line(surface,'#2181FF', [start[0],start[1]-16 +cbs], [end[0],end[1]-16 +cbs], 2);
			gamejs.draw.line(surface,'#5291EF', [start[0],start[1]-18 +cbs], [end[0],end[1]-18 +cbs], 2);
			gamejs.draw.line(surface,'#82C0FF', [start[0],start[1]-20 +cbs], [end[0],end[1]-20 +cbs], 2);
			gamejs.draw.line(surface,'#A1E2FF', [start[0],start[1]-22 +cbs], [end[0],end[1]-22 +cbs], 2);
			gamejs.draw.line(surface,'#C0E1FF', [start[0],start[1]-24 +cbs], [end[0],end[1]-24 +cbs], 2);
			gamejs.draw.line(surface,'#C2EFFF', [start[0],start[1]-26 +cbs], [end[0],end[1]-26 +cbs], 2);
			gamejs.draw.line(surface,'#E0F1FF', [start[0],start[1]-28 +cbs], [end[0],end[1]-28 +cbs], 2);
			gamejs.draw.line(surface,'#E0FFFF', [start[0],start[1]-30 +cbs], [end[0],end[1]-30 +cbs], 2);
			
		}
			
	}

};