var gamejs = require("gamejs");
var utils = require("js/utils");
var NightAction = require("js/nightAction").NightAction;
var Explosion = require("js/explosion").Explosion;

/**
 * NukeBomb : this NightAction, once executed, destroys every enemy in a large area.
 */

/**
 * Creates a Bomb object.
 * 
 * @param {Match} match The Match object where this NukeBomb will be executed.
 * 
 * @constructor
 */
var NukeBomb = exports.NukeBomb = function (match) {
	NukeBomb.superConstructor.call(this, match);
	this.image = gamejs.image.load(IMAGE_ROOT + "nightActions/bomb.png");
	this.executionTime = 1000;// 1 sec
	this.reloadTime = 120000;// 120 sec = 2 min
	this.buyPrice = 350;
	this.executionPoint = null;
	
	// not modify this variable in when extending
	this.timer = this.executionTime;
};

/**
 * Bomb extends NightAction.
 */
gamejs.utils.objects.extend(NukeBomb, NightAction);

/**
 * Executes the Cannon in the last set position, setting
 * executed = true and executionPoint = position.
 */
NukeBomb.prototype.execute = function () {
	var corner = [this.position[0]-NUKE_RADIUS, this.position[1]-NUKE_RADIUS];
	if(corner[0] < WIDTH_OFFSET)
		corner[0] = WIDTH_OFFSET;
	if(corner[1] < HEIGHT_OFFSET)
		corner[1] = HEIGHT_OFFSET;
	
	var nukeRect = new gamejs.Rect(corner, [NUKE_RADIUS*2, NUKE_RADIUS*2]);
	var allEnemies = this.match.getMap().getEnemies().sprites();
	var nEnemies = allEnemies.length;
	for(var i=0; i < nEnemies; i++){
		if(nukeRect.collideRect(allEnemies[0].rect)){
			allEnemies[0].hit(allEnemies[0].health + allEnemies[0].armor); //kill the enemy
		}
	}
	
	this.executionPoint = this.selectedPosition;
	this.executed = true;
};

NukeBomb.prototype.draw = function (surface) {
	if( ! this.isOver()) {
		if( ! this.executed) {
			// the position is referred to the map, but we have to draw
			// in the main surface, mind the offset
			var drawingPos = [
				this.position[X] + WIDTH_OFFSET,
				this.position[Y] + HEIGHT_OFFSET
			];
			if(drawingPos[0] < WIDTH_OFFSET + NUKE_RADIUS)
				drawingPos[0] = WIDTH_OFFSET + NUKE_RADIUS;
			if(drawingPos[1] < HEIGHT_OFFSET + NUKE_RADIUS)
				drawingPos[1] = HEIGHT_OFFSET + NUKE_RADIUS;
			document.getElementsByTagName("body")[0].style.cursor = 'crosshair';
			gamejs.draw.circle(surface, "rgba(255,0,0,0.3)", drawingPos, NUKE_RADIUS, 0);
		}else{
			document.getElementsByTagName("body")[0].style.cursor = 'default';
		}	
	}

};