var gamejs = require("gamejs");
var utils = require("js/utils");
var NightAction = require("js/nightAction").NightAction;

/**
 * Flare : this NightAction, once executed, detects all the enemies in the map.
 */

/**
 * Creates a Flare object.
 * 
 * @param {Match} match The Match object where this Flare will be executed.
 * 
 * @constructor
 */
var Flare = exports.Flare = function (match) {
	Flare.superConstructor.call(this, match);
	this.image = gamejs.image.load(IMAGE_ROOT + "nightActions/flare.png");
	this.executionTime = 1000;// 1 sec
	this.reloadTime = 10000;// 10 sec
	this.buyPrice = 50;
	
	// not modify this variable in when extending
	this.timer = this.executionTime;
};

/**
 * Flare extends NightAction.
 */
gamejs.utils.objects.extend(Flare, NightAction);

/**
 * Executes the Flare in the last set position, setting
 * executed = true and executionPoint = position.
 * The Flare detects all the enemies in the map.
 */
Flare.prototype.execute = function () {
	this.executed = true;
	this.executionPoint = this.position;
	// detects all enemies
	var allEnemies = this.match.getMap().getEnemies().sprites();
	allEnemies.forEach(function (enemy) {
		enemy.setDetected(true);
	});
};