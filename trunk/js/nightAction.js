var gamejs = require("gamejs");
var utils = require("js/utils");

/**
 * NightAction : this object represents a buyable action(from the night store)
 * and executable during the night.
 * 
 * A NightAction contains :
 * - match : the Match object where the NightAction will be executed;
 * - executed : a boolean value that says if the action is already been executed;
 * - image : the Surface storing the image to draw before the execution(*);
 * - position : the coordinates of the action, it's used to know where to draw
 * 				the image and where to execute the action, must be a point of
 * 				the match map;
 * - executionPoint : the point where the action is been executed.
 * - executionTime : the time that have to pass before the action can be
 * 				considered over, in milliseconds(*);
 * - reloadTime : the time that have to pass before player can use another
 * 				action, in milliseconds(*);
 * - buyPrice : the price to buy the action(*);
 * (*) = you must redefine this variable in your extension.
 * 
 * This object is used like an interface, don't initialize it.
 * All the objects in the night store MUST extend this.
 */

/**
 * Creates a NightAction object.
 * 
 * @param {Match} match The Match object where this NightAction will be executed.
 * 
 * @constructor
 */
var NightAction = exports.NightAction = function (match) {
	this.match = match;
	this.executed = false;
	this.position = [0, 0];
	this.executionPoint = null;
	
	// must be modify when extending
	this.image = new gamejs.Surface([50, 50]);
	this.executionTime = 1000;
	this.reloadTime = 1000;
	this.buyPrice = 10;
	
	// not modify this variable in when extending
	this.timer = this.executionTime;
};

/**
 * Says if the action is over, mind that when this returns true the action
 * is deleted, so the update(..) and draw(..) methods is no longer called.
 * 
 * @return {boolean} True if the action is over, false otherwise.
 */
NightAction.prototype.isOver = function () {
	return (this.timer <= 0);
};

/**
 * Returns the price to buy this NightAction.
 * 
 * @return {number} The price to buy this NightAction.
 */
NightAction.prototype.getBuyPrice = function () {
	return this.buyPrice;
};

/**
 * Returns the time that have to pass form the action conclusion (isOver() = true)
 * before player can use another NightAction.
 * 
 * @return {number} The time that have to pass before player can use another
 * 					NightAction, in milliseconds.
 */
NightAction.prototype.getReloadTime = function () {
	return this.reloadTime;
};

/**
 * Sets the position, it's used for the drawing and the execution.
 * 
 * @param {[number, number]} pos A point of the map.
 */
NightAction.prototype.setPosition = function (pos) {
	if( ! this.match.getMap().isInside(pos)) {
		throw new Error("pos must be inside the map");
	}
	
	this.position = pos;
};

/**
 * Executes the NightAction in the last set position, setting
 * executed = true and executionPoint = position.
 */
NightAction.prototype.execute = function () {
	this.executed = true;
	this.executionPoint = this.position;
};

/**
 * Updates any effects of this NightAction, including the updating
 * of the timer.
 *
 * @param {number} msDuration The time past from the last call, in ms.
 */
NightAction.prototype.update = function (msDuration) {
	if( ! this.isOver()) {
		if(this.executed) {
			this.timer -= msDuration;
		}
	}
};

/**
 * Draws the image and/or any effects of this NightAction.
 *
 * @param {gamejs.Surface} surface The Surface where draw on.
 */
NightAction.prototype.draw = function (surface) {
	if( ! this.isOver()) {
		if( ! this.executed) {
			// the position is referred to the map, but we have to draw
			// in the main surface, mind the offset
			var drawingPos = [
				this.position[X] + WIDTH_OFFSET,
				this.position[Y] + HEIGHT_OFFSET
			];
			utils.drawAtMiddle(this.image, surface, drawingPos);
		}
	}
};