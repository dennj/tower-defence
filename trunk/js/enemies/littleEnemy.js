var gamejs = require('gamejs');
var mainEnemy = require('js/enemy');

/**
 * LittleEnemy: an object representing a little and weak enemy.
 *
 * @extends Enemy

/**
 * Creates a LittleEnemy object.
 *
 * @param {Match} match The Match object where this object will work.
 * @param {[number, number]} position The enemy's position, in format [x, y].
 * @param {Number} level The Enemy level [default 0].
 * 
 * @constructor
 */
var LittleEnemy = exports.LittleEnemy = function(match, position, level) {
	LittleEnemy.superConstructor.call(this, match, position, level);
};

gamejs.utils.objects.extend(LittleEnemy, mainEnemy.Enemy);

/**
 * The list of available levels.
 * NOTE that this isn't a list of available upgrade, so when we have to
 * pass the upgradeList to the constructor of GameEntity we pass to it just
 * one level; this make the enemy not upgradable, as it's right.
 *
 * Each level must have the following properties (in addition to the
 * GameEntity's ones):
 *    - detectedImage: Image to use when Enemy is detected
 *    - health: HP of this Enemy
 *    - speed: movement speed in pixels/s
 *    - visibilityTime: when detected the Enemy is visible for this time [ms]
 *    - killScore: score awarded to the player for killing the enemy
 *    - killCredit: credits awarded to the player for killing the enemy
 */
LittleEnemy.LEVELS_LIST = [
	// Level 0
	{
		image: IMAGE_ROOT + "enemies/squaredEnemy.png",
		detectedImage: IMAGE_ROOT + "enemies/squaredDetectedEnemy.png",
		health: 5, speed: 30, visibilityTime: 1500, killScore: 1, armor: 2,
		alpha: 40,damage:1, killCredit: 5
	},
	// Level 1
	{
		image: IMAGE_ROOT + "enemies/squaredEnemy.png",
		detectedImage: IMAGE_ROOT + "enemies/squaredDetectedEnemy.png",
		health: 10, speed: 40, visibilityTime: 1200, killScore: 3, armor: 4,
		alpha: 40,damage:2, killCredit: 8
	}
];
