var gamejs = require('gamejs');
var mainEnemy = require('js/enemy');

/**
 * GiantEnemy: an object representing a big and strong enemy.
 *
 * @extends Enemy

/**
 * Creates a GiantEnemy object.
 *
 * @param {Match} match The Match object where this object will work.
 * @param {[number, number]} position The enemy's position, in format [x, y].
 * @param {Number} level The Enemy level [default 0].
 * 
 * @constructor
 */
var GiantEnemy = exports.GiantEnemy = function(match, position, level)  {
	GiantEnemy.superConstructor.call(this, match, position, level);
};

gamejs.utils.objects.extend(GiantEnemy, mainEnemy.Enemy);

/**
 * The list of available levels.
 * NOTE that this isn't a list of available upgrade, so when we have to
 * pass the upgradeList to the constructor of GameEntity we pass to it just
 * one level; this make the enemy not upgradable, as it's right.
 *
 * Each level must have the following properties (in addition to the
 * GameEntity's ones):
 *    - detectedImange: Image to use when Enemy is detected
 *    - health: HP of this Enemy
 *    - speed: movement speed in pixels/s
 *    - visibilityTime: when detected the Enemy is visible for this time [ms]
 *    - killScore: score awarded to the player for killing the enemy
 *    - killCredit: credits awarded to the player for killing the enemy
 */
GiantEnemy.LEVELS_LIST = [
	// Level 0
	{
		image: IMAGE_ROOT + "enemies/enemyPink.png",
		detectedImage: IMAGE_ROOT + "enemies/enemyPink.png",
		health: 50, speed: 50, visibilityTime: 1500, killScore: 30,
		armor: 20, alpha: 40,damage:20, killCredit: 50
	},
	// Level 1
	{
		image: IMAGE_ROOT + "enemies/enemyPink.png",
		detectedImage: IMAGE_ROOT + "enemies/enemyPink.png",
		health: 50, speed: 40, visibilityTime: 1200, killScore: 50,
		armor: 50, alpha: 40,damage:40, killCredit: 100
	}
];
