var gamejs = require('gamejs');
var mainEnemy = require('js/enemy');
var LittleEnemy = require("js/enemies/littleEnemy").LittleEnemy;



/* Creates a GiantEnemy object.
 *
 * @param {Match} match The Match object where this object will work.
 * @param {[number, number]} position The enemy's position, in format [x, y].
 * @param {Number} level The Enemy level [default 0].
 * 
 * @constructor
 */
var ExplodingEnemy = exports.ExplodingEnemy = function(match, position, level)  {
	 ExplodingEnemy.superConstructor.call(this, match, position, level);
};

gamejs.utils.objects.extend(ExplodingEnemy, mainEnemy.Enemy);

/**
 * The list of available levels.
 * NOTE that this isn't a list of available upgrade, so when we have to
 * pass the upgradeList to the constructor of GameEntity we pass to it just
 * one level; this make the enemy not upgradable, as it's right.
 *
 * Each level must have the following properties (in addition to the
 * GameEntity's ones):
 *    - detectedImange: Image to use when Enemy is detected
 *    - health: HP of this Enemy
 *    - speed: movement speed in pixels/s
 *    - visibilityTime: when detected the Enemy is visible for this time [ms]
 *    - killScore: score awarded to the player for killing the enemy
 *    - killCredit: credits awarded to the player for killing the enemy
 */
ExplodingEnemy.LEVELS_LIST = [
	// Level 0
	{
		image: IMAGE_ROOT + "enemies/enemyRed.png",
		detectedImage: IMAGE_ROOT + "enemies/enemyRed.png",
		health: 1, speed: 100, visibilityTime: 1500, killScore: 30,
		armor: 0, alpha: 40,damage:20, killCredit: 50
	},
	// Level 1
	{
		image: IMAGE_ROOT + "enemies/enemyRed.png",
		detectedImage: IMAGE_ROOT + "enemies/enemyRed.png",
		health: 50, speed: 40, visibilityTime: 1200, killScore: 50,
		armor: 50, alpha: 40,damage:40, killCredit: 100
	}
];

ExplodingEnemy.prototype.doKill = function() {
	//the enemy will generate from 0 to 3 little enemy at his death
	for(var i = 0; i<3; i++){
		//with p = 70% a new little enemy will be added
		var extraction = Math.floor(Math.random()*101); //random value from 0 to 100
		if(extraction > PROBABILITY_NOT_NEW_ENEMY)
			this.getMatch().getMap().addGameEntity(new LittleEnemy(this.getMatch(),this.getCenter(),0,true));
	}
	this.kill();
}

