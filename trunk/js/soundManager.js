var gamejs = require('gamejs');

/**
 * Sound manager class.
 * There are 2 kind of sound: laser sounds and explosion sounds
 * each type of sound corresponds to an array: lasersSounds[] and explosionSounds[]
 *
 * There are 2 type of lasers and 2 type of explosions
 *
 * There are 2 soundtrack: a day theme and a night theme
 */



var SoundManager = exports.SoundManager = function() {
	this.laserSounds = [];
	this.explosionSounds = [];
	this.missileSounds = [];
	this.dayTheme = new Audio(SOUND_ROOT + "DayTheme.ogg");
	this.nightTheme = new Audio(SOUND_ROOT + "NightTheme.ogg");
	
	// Push all laser sounds in laserSounds array
	this.laserSounds.push(new Audio(SOUND_ROOT + "laser0.ogg"));
	this.laserSounds.push(new Audio(SOUND_ROOT + "laser1.ogg"));
	// Push all laser explosion in explosionSounds array
	this.explosionSounds.push(new Audio(SOUND_ROOT + "explosion0.ogg"));
	this.explosionSounds.push(new Audio(SOUND_ROOT + "explosion1.ogg"));
	// Push all missile sounds in missileSounds array
	this.missileSounds.push(new Audio(SOUND_ROOT + "missile0.ogg"));
	this.missileSounds.push(new Audio(SOUND_ROOT + "missile1.ogg"));
	this.missileSounds.push(new Audio(SOUND_ROOT + "missile2.ogg"));

};
/**
 * Play the laser sound corresponding with id
 *
 * @param {int} the id of the laser to play, must be in range
 */
SoundManager.prototype.playLaser = function(id){
	if(id<0 || id >= this.laserSounds.length)
		throw new ReferenceError("Laser not in array");
	this.laserSounds[id].play();
};

/**
 * Play the missile sound corresponding with id
 *
 * @param {int} the id of the laser to play, must be in range
 */
SoundManager.prototype.playMissle = function(id){
	if(id<0 || id >= this.missileSounds.length)
		throw new ReferenceError("Missile not in array");
	this.missileSounds[id].play();
};

/**
 * Stop playing the missile sound corresponding with id
 *
 * @param {int} the id of the missile to stop playing, must be in range
 */
SoundManager.prototype.stopMissle = function(id){
	if(id<0 || id >= this.missileSounds.length)
		throw new ReferenceError("Missile not in array");
	this.missileSounds[id].pause();
};

/**
 * Play the explosion sound corresponding with id
 *
 * @param {int} the id of the explosion to play, must be in range
 */
SoundManager.prototype.playExplosion = function(id){
	if(id<0 || id >= this.explosionSounds.length)
		throw new ReferenceError("Explosion not in array");
	this.explosionSounds[id].play();
};

/**
 * Play the day theme
 *
 */
SoundManager.prototype.playDay = function(){
	this.dayTheme.addEventListener('ended', function() {
		//this.currentTime = 0;
		this.play();
	}, false);
	this.dayTheme.play();
};

/**
 * Play the night theme
 *
 */
SoundManager.prototype.playNight = function(){
	this.nightTheme.addEventListener('ended', function() {
		//this.currentTime = 0;
		this.play();
	}, false);
	this.nightTheme.play();
};

/**
 * Stop all sounds
 *
 */
SoundManager.prototype.stop = function(){
	this.nightTheme.pause();
	this.dayTheme.pause();
};