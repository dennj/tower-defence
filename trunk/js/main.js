var gamejs = require("gamejs");
var missions = require("js/missions");
var scenesDirectorModule = require("js/scenes/scenesDirector");
var startSceneModule = require("js/scenes/startScene");
var utils = require("js/utils");


gamejs.ready(main);

/**
 *The main.
 */
function main() {
	//our code here
	gamejs.display.setMode([DISPLAY_WIDTH, DISPLAY_HEIGHT]);
	gamejs.display.setCaption("Tower Defense");
	var mainSurface = gamejs.display.getSurface();
	
	var director = new scenesDirectorModule.ScenesDirector(mainSurface);
	director.start(new startSceneModule.StartScene(director));

	gamejs.time.fpsCallback(director.processScene, director, FPS);

}

// Preload all resources
utils.preload();