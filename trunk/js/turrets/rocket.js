var gamejs = require('gamejs');
var matchjs = require('js/match')
var GameEntity = require('js/gameEntity').GameEntity;
var Explosion = require('js/explosion').Explosion;


var Rocket = exports.Rocket = function(level,turret,target,match) {
	Rocket.superConstructor.call(this , match, turret.getCenter(), this.constructor.UPGRADE_LIST, true);
	
	this.originalImage = this.image;
	
	this.level = level;
	this.turret = turret;
	this.target = target; 
	
	// default relative movement
	this.relativeNext = [this.getLevelSettings().speed, 0];
	//used to limit the recalculation of relativeNext
	this.refresh = 1;
}
/**
 * The list of available upgrades
 */
Rocket.UPGRADE_LIST = [
	// Level 0
	{speed: 4, damage: 10,
		image: IMAGE_ROOT + "turrets/rocket.png"
	},
	// Level 1
	{speed: 9, damage: 20,
		image: IMAGE_ROOT + "turrets/rocket.png"
	}
];
/**
 * Turret extends GameEntity
 */
GameEntity.extend(Rocket);


/**
 * Updates the position of the Bullet
 * 
 * @param {number} ms duration
 */
Rocket.prototype.update = function(msDuration) {
	var center = this.getCenter();
	var speed = this.getLevelSettings().speed;
	var damage = this.getLevelSettings().damage;
	//check if the bullet is out of the field
	if(! this.getMatchMap().isInside(center)){
		this.kill();
		return ;
	};
	
	if(!(this.target.isDead())){
		var rotation; // the rotation angle of the image
		var endPoint = this.target.getCenter();
		if ((Math.abs(endPoint[1] - center[1]) > Math.sqrt(speed)) && (Math.abs(endPoint[0] - center[0]) > Math.sqrt(speed)) && (this.refresh -- <= 0)){
			//it's 10 to reduce by 1/10 the number of recalculations
			this.refresh = 10;
			if((endPoint[1] != center[1]) && (endPoint[0] != center[0])){
				//get x/y direction
				var xDirection = center[0] > endPoint[0] ? -1 : 1;
				var yDirection = center[1] > endPoint[1] ? -1 : 1;

				//calculate the x/y proportion
				var increment = Math.abs((endPoint[1] - center[1]) / (endPoint[0] - center[0]));

				//get the module of bullet direction vector (1, increment)
				var module = Math.sqrt( 1 + Math.pow(increment, 2));

				this.relativeNext = [Math.sqrt(speed)* xDirection / module,
				                     Math.sqrt(speed) * increment * yDirection /module ];
					
				this.relativeNext = [Math.round(this.relativeNext[0]), Math.round(this.relativeNext[1])];
				
				//calculate the movement angle
				rotation =((xDirection==1))?
					gamejs.utils.math.degrees(Math.atan( this.relativeNext[1] / this.relativeNext[0])):
					gamejs.utils.math.degrees(Math.atan( this.relativeNext[1] / this.relativeNext[0])) + 180;
			}
			else{
					var direction;
					if (endPoint[0] == center[0]) {
						// bullet must move up/down
						direction = endPoint[1] > center[1] ? 1 : -1;
						this.relativeNext = [0, Math.round(Math.sqrt(speed))*direction];
						
						rotation = (direction == 1) ? -90 : 90;
					}
					else {
						// bullet must move left/right
						direction = endPoint[0] > center[0] ? 1 : -1;
						this.relativeNext = [Math.round(Math.sqrt(speed))*direction, 0];
							
						rotation = (direction == 1) ? 0: 180;
					};
				}
				//rotate its image
				this.image = gamejs.transform.rotate(this.originalImage, rotation);

		};
	};
	
	var next = gamejs.utils.vectors.add(center, this.relativeNext);
	this.rect = new gamejs.Rect(gamejs.utils.vectors.subtract(next, gamejs.utils.vectors.divide(this.image.getSize(), 2)), this.image.getSize());;


	//check if any obstacle is colliding
	var collision =  this.getMatchMap().getObstacles().collidePoint(this.getCenter());
	if(collision.length >0){
		this.kill();
		var expl = new Explosion(this.getMatch(), this.rect, 500);
		this.getMatchMap().addExplosion(expl);
		return;
	};


	//check if any enemy is colliding
	collision =  this.getMatchMap().getEnemies().collidePoint(this.getCenter());
	if(collision.length >0){
		collision.forEach(function(enemy) {
			enemy.hit(damage);
		});
		this.kill();
		var expl = new Explosion(this.getMatch(), this.rect, 500);
		this.getMatchMap().addExplosion(expl);
	}
};