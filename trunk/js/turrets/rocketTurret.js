var gamejs = require('gamejs');
var mainTurret = require('js/turret');
var Rocket = require('js/turrets/rocket').Rocket;
/**
 * Create a Turret object
 *
 * @param {Match} match The match where this object will work
 * @param {[number, number]} coords The radar's position, in format [x, y]
 */
var RocketTurret = exports.RocketTurret = function(match, coordinates, tf) {
	RocketTurret.superConstructor.call(this , match, coordinates, tf);
	this.target = null;
	this.bullets = new gamejs.sprite.Group();
	this.timeFromLastShoot = this.getLevelSettings().reloadingTime;
};


/**
 * The list of available upgrades
 */
RocketTurret.UPGRADE_LIST = [
	// Level 0
	{price: 200, range: 200, resaleValue: 100, rocketLevel: 1,reloadingTime : 1000 ,
		image: IMAGE_ROOT + "turrets/rocketturret.png"
	},
	// Level 1
	{price: 150, range: 300, resaleValue: 125, rocketLevel: 2,reloadingTime : 500 ,
		image: IMAGE_ROOT + "turrets/rocketturret.png"
	}
];

gamejs.utils.objects.extend(RocketTurret, mainTurret.Turret);

/**
 * Updates the position of the detector ray of the turret.
 *
 * @param {number} msDuration The time past from the last call, in ms.
 */
RocketTurret.prototype.update = function(msDuration) {
	this.bullets.update(msDuration);
	this.timeFromLastShoot += msDuration;
	//checks if the turret is ready to shoot again
	if(this.timeFromLastShoot > this.getLevelSettings().reloadingTime) {
		var enemy_seen = this._targetEnemy();
		if(enemy_seen){
			this.bullets.add(new Rocket(this.getLevelSettings().rocketLevel,this,enemy_seen,this.getMatch()),null);
			//this.getMatch().getSoundManager().playMissle(0);
			this.timeFromLastShoot = 0;
		}else{
			this.target = null;
		}
		
	}
};

/**
 * Draws the turret in the given Surface.
 *
 * @param {gamejs.Surface} surface The Surface where draw on.
 */
RocketTurret.prototype.draw = function(surface) {
	//draw turret tower
	surface.blit(this.image, this.rect.topleft);
	this.bullets.draw(surface);
};