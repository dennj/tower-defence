var gamejs = require('gamejs');
var matchjs = require('js/match')
var GameEntity = require('js/gameEntity').GameEntity;
var mainTurret = require('js/turret');
var Explosion = require ('js/explosion').Explosion;


var Mine = exports.Mine = function(match, coordinates, tf) {
	Mine.superConstructor.call(this, match, coordinates, tf);
	
	this.originalImage = this.image;
	this.coordinates = coordinates;
}

gamejs.utils.objects.extend(Mine, mainTurret.Turret);

/**
 * The list of settings
 */
Mine.UPGRADE_LIST = [
	// Level 0
	{ price: 300, range: 100, resaleValue: 150, damage: 1000, image: IMAGE_ROOT + "turrets/mine.png" }
];

/**
 * Check collisions
 * 
 * @param {number} ms duration
 */
Mine.prototype.update = function(msDuration) {
	//check if any enemy is colliding
	collision =  gamejs.sprite.spriteCollide(this, this.getMatchMap().getEnemies());
	if(collision.length >0){
		collision.forEach(function(enemy) {
			enemy.kill();
			//enemy.hit(Mine.UPGRADE_LIST.damage);
		});
		this.kill();
		var expl = new Explosion(this.getMatch(), this.rect, 1000);
		this.getMatchMap().addExplosion(expl);
	}
};