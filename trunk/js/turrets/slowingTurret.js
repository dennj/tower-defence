var gamejs = require('gamejs');
var mainTurret = require('js/turret');

/**
 * Create a Turret object
 *
 * @param {Match} match The match where this object will work
 * @param {[number, number]} coords The radar's position, in format [x, y]
 */
var SlowingTurret = exports.SlowingTurret = function(match, coordinates, tf) {
	SlowingTurret.superConstructor.call(this,match, coordinates, tf);
	this.timeFromLastShoot = this.getLevelSettings().reloadingTime;
};

/**
 * The list of available upgrades
 */
SlowingTurret.UPGRADE_LIST = [
	// Level 0
	{price: 250, range: 100, resaleValue: 125, damage: 0.5,reloadingTime : 1000 ,
		image: IMAGE_ROOT + "turrets/slowingturret.png"
	},
	// Level 1
	{price: 100, range: 200, resaleValue: 175, damage: 0.1,reloadingTime : 1000 ,
		image: IMAGE_ROOT + "turrets/slowingturret.png"
	}
];

gamejs.utils.objects.extend(SlowingTurret, mainTurret.Turret);

/**
 * Updates the position of the detector ray of the turret.
 *
 * @param {number} msDuration The time past from the last call, in ms.
 */
SlowingTurret.prototype.update = function(msDuration) {
	this.timeFromLastShoot += msDuration;
	if(this.timeFromLastShoot > this.getLevelSettings().reloadingTime) {
		var enemy_seen;
		var cur_center =  this.getCenter();
		var cur_radius = this.getLevelSettings().range;
		var cur_slow =this.getLevelSettings().damage;
		this.getMatchMap().getEnemies().sprites().forEach(function(enemy) {
			if(Math.sqrt(
				(enemy.getCenter()[0] - cur_center[0])*(enemy.getCenter()[0] - cur_center[0]) +
				(enemy.getCenter()[1] - cur_center[1])*(enemy.getCenter()[1] - cur_center[1])
			) < cur_radius){
				enemy.slow(cur_slow,2);
			}
		});
		this.timeFromLastShoot = 0;
	}
};

/**
 * Draws the turret in the given Surface.
 *
 * @param {gamejs.Surface} surface The Surface where draw on.
 */
SlowingTurret.prototype.draw = function(surface) {
	surface.blit(this.image, this.rect.topleft);
	gamejs.draw.circle(surface, '#FFFFFF', this.getCenter(), this.getLevelSettings().range*((this.timeFromLastShoot+1)/ this.getLevelSettings().reloadingTime), 2);

};