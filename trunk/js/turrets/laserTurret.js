var gamejs = require('gamejs');
var mainTurret = require('js/turret');

/**
 * Create a Turret object
 *
 * @param {Match} match The match where this object will work
 * @param {[number, number]} coords The radar's position, in format [x, y]
 */
var LaserTurret = exports.LaserTurret =  function(match, coordinates, tf) {
	LaserTurret.superConstructor.call(this,match, coordinates, tf);
	this._target = null;

};

gamejs.utils.objects.extend(LaserTurret, mainTurret.Turret);

/**
 * The list of available upgrades
 */
LaserTurret.UPGRADE_LIST = [
	// Level 0
	{price: 120, range: 150, resaleValue: 60, dps: 0.05,
		image: IMAGE_ROOT + "turrets/laserturret.png"
	},
	// Level 1
	{price: 100, range: 200, resaleValue: 110, dps: 0.5,
		image: IMAGE_ROOT + "turrets/laserturret.png"
	}
];

/**
 * Updates the position of the detector ray of the turret.
 *
 * @param {number} msDuration The time past from the last call, in ms.
 */
LaserTurret.prototype.update = function(msDuration) {
	var enemy = this._targetEnemy();
		if(enemy){
			this._target = enemy.getCenter();
			enemy.hit(this.getLevelSettings().dps);
			this.getMatch().getSoundManager().playLaser(0);
		}else{
			this._target = null;
		}
	
};

/**
 * Draws the turret in the given Surface.
 *
 * @param {gamejs.Surface} surface The Surface where draw on.
 */
LaserTurret.prototype.draw = function(surface) {
	//draw turret tower
	surface.blit(this.image, this.rect.topleft);
	if(this._target){
		gamejs.draw.line(surface, '#0000FF', this.getCenter(), this._target, 3);
		gamejs.draw.line(surface, '#ff0000', this.getCenter(), this._target, 1);
		gamejs.draw.circle(surface, '#0000FF', this._target, 3);
		gamejs.draw.circle(surface, '#ff0000', this._target, 2);
	}
};