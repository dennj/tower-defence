var utils = require('js/utils');

qModule('js/utils');

test("utils.distance()", function() {
	// Test with the formula
	var pointAx = 2, pointAy = 2;
	var pointBx = 5, pointBy = 6;
	var pointA = [pointAx, pointAy];
	var pointB = [pointBx, pointBy];
	var distance = Math.sqrt(
		Math.pow(pointAx - pointBx, 2) + Math.pow(pointAy - pointBy, 2)
	);
	strictEqual(utils.distance(pointA, pointB), distance,
		"Test with arguments: (pointA = [2,2], pointB = [5,6])"
	);
	// Test with positive numbers
	pointA = [1, 1];
	pointB = [4, 5];
	strictEqual(utils.distance(pointA, pointB), 5,
		"Test with arguments: (pointA = [1,1], pointB = [4,5])"
	);
	// Test with negative numbers
	pointA = [-1, -1];
	pointB = [-4, -5];
	strictEqual(utils.distance(pointA, pointB), 5,
		"Test with arguments: (pointA = [-1,-1], pointB = [-4,-5])"
	);
	// Test with zeros
	pointA = [0, 0];
	pointB = [0, 0];
	strictEqual(utils.distance(pointA, pointB), 0,
		"Test with arguments: (pointA = [0,0], pointB = [0,0])"
	);
	// Test with zeros and posite numbers
	pointA = [0, 0];
	pointB = [4, 3];
	strictEqual(utils.distance(pointA, pointB), 5,
		"Test with arguments: (pointA = [0,0], pointB = [4,3])"
	);
	// Test with zeros and negative numbers
	pointA = [0, 0];
	pointB = [-4, -3];
	strictEqual(utils.distance(pointA, pointB), 5,
		"Test with arguments: (pointA = [0,0], pointB = [-4,-3])"
	);
	// Tests with positive and negative numbers
	pointA = [-1, -1];
	pointB = [3, 2];
	strictEqual(utils.distance(pointA, pointB), 5,
		"Test with arguments: (pointA = [-1,-1], pointB = [3,2])"
	);
	pointA = [-1, 1];
	pointB = [3, 4];
	strictEqual(utils.distance(pointA, pointB), 5,
		"Test with arguments: (pointA = [-1,1], pointB = [3,4])"
	);
});