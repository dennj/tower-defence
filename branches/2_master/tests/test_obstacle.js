var Match = require('js/match').Match;
var Obstacle = require('js/obstacle').Obstacle;
var gamejs = require('gamejs');


qModule('js/obstacle', {
	setup: function() {
		this.testMatch = new Match();
		this.center = [200, 200];
		this.testObstacle = new Obstacle(this.testMatch, this.center);
	}
});

test("Obstacle(match, center, level)", function(){
	ok(this.testObstacle instanceof Obstacle,
		"Test with arguments (Match, [200, 200], default): " +
		"object correctly created"
	);
	deepEqual(this.testObstacle.getMatch(), this.testMatch,
		"Test with arguments (Match, [200, 200], default): " +
		"match correctly setted"
	);
	deepEqual(this.testObstacle.getCenter(), this.center,
		"Test with arguments (Match, [200, 200], default): " +
		"center correctly setted"
	);
	deepEqual(this.testObstacle.getLevel(), 0,
		"Test with arguments (Match, [200, 200], default): " +
		"default level correctly setted"
	);
	// Test level param
	var lvl1_Obstacle = new Obstacle(this.testMatch, this.center, 1);
	deepEqual(lvl1_Obstacle.getLevel(), 1,
		"Test with arguments (Match, [10,50], 1): " +
		"level correctly setted"
	);
});

test("Obstacle.getCenter()", function(){
	// Passes if the function returns the given (expected)center
	deepEqual(this.testObstacle.getCenter(), this.center,
		"Test on a standard Obstacle"
	);
});

test("Obstacle.getRadius()", function(){
	// Passes if the function returns the given (expected)radius
	var radius = this.testObstacle.getRadius();
	var imageRadius = this.testObstacle.image.getSize()[1] / 2;
	deepEqual(radius, imageRadius, "Test on a standard Obstacle");
});