var Match = require('js/match').Match;
var Map = require('js/map').Map;
var gamejs = require('gamejs');

qModule('js/match', {
	setup: function() {
		this.testMatch = new Match();
	}
});


test("Match(health, credit)", function(){
	// Test with default arguments
	ok(this.testMatch instanceof Match,
		"Test with arguments: (default, default)"
	);
	equal(this.testMatch.getHealth(), 100,
		"Test with arguments: (default, default): health is correct"
	);
	equal(this.testMatch.getCredit(), 1000,
		"Test with arguments: (default, default): credit is correct"
	);
	equal(this.testMatch.getScore(), 0,
		"Test with arguments: (default, default): score is correct"
	);
	// Test with user provided arguments
	var health = 1000, credit = 50;
	var userMatch = new Match(health, credit);
	ok(userMatch instanceof Match,
		"Test with arguments: (Number > 0, Number > 0)"
	);
	equal(userMatch.getHealth(), health,
		"Test with arguments: (Number > 0, Number > 0): health is correct"
	);
	equal(userMatch.getCredit(), credit,
		"Test with arguments: (Number > 0, Number > 0): credit is correct"
	);
	equal(userMatch.getScore(), 0,
		"Test with arguments: (Number > 0, Number > 0): score is correct"
	);
	// Check that the Match is correctly created with a credit = 0
	var creditZeroMatch = new Match(100, 0);
	ok(creditZeroMatch instanceof Match,
		"Test with arguments: (Number > 0, Number > 0)"
	);
	equal(creditZeroMatch.getCredit(), 0,
		"Test with arguments: (Number > 0, 0): credit is correct"
	);
	// Test that exception are raised if created with wrong arguments
	throws(
		function() { var errorMatch = new Match(0, 100); },
		TypeError,
		"Test with argument: (0, Number > 0): exception correctly throwed"
	);
	throws(
		function() { var errorMatch = new Match(-1, 100); },
		TypeError,
		"Test with argument: (Number < 0, Number > 0): " +
		"exception correctly throwed"
	);
	throws(
		function() { var errorMatch = new Match(100, -1); },
		TypeError,
		"Test with argument: (Number > 0, Number < 0): " +
		"exception correctly throwed"
	);
	throws(
		function() { var errorMatch = new Match(-1, -1); },
		TypeError,
		"Test with argument: (Number < 0, Number < 0): " +
		"exception correctly throwed"
	);
	throws(
		function() { var errorMatch = new Match(NaN, NaN); },
		TypeError,
		"Test with argument: (NaN, NaN): exception correctly throwed"
	);
});

test("Match.getMap()", function(){
	// Match without a Map
	this.testMatch.setMap(undefined);
	strictEqual(this.testMatch.getMap(), undefined,
		"Test on a Match with an undefined Map"
	);
	// Normal map
	testMap = new Map(this.testMatch, 10, 10);
	this.testMatch.setMap(testMap);
	strictEqual(this.testMatch.getMap(), testMap,
		"Test on a Match having a Map"
	);
});

test("Match.setMap(map)", function(){
	var testMap = new Map(this.testMatch, 10, 10);
	this.testMatch.setMap(testMap);
	strictEqual(this.testMatch.getMap(), testMap, "Test with argument: (Map)");
});

test("Match.getHealth()", function(){
	var health = 300;
	var healthMatch = new Match(health);
	equal(healthMatch.getHealth(), health, "Test with a regular Match object");
});

test("Match.decreaseHealth(hitpoint)", function() {
	var health = this.testMatch.getHealth();
	// Test with undefined argument
	var returnedHealth = this.testMatch.decreaseHealth();
	health = health - 1;
	equal(this.testMatch.getHealth(), health,
		"Test with argument: (undefined)"
	);
	equal(returnedHealth, health,
		"Test with argument: (undefined): returned health is correct"
	);
	// Test with a Number > 0
	var hit = 10;
	health = health - hit;
	returnedHealth = this.testMatch.decreaseHealth(hit);
	equal(this.testMatch.getHealth(), health,
		"Test with argument: (Number > 0)"
	);
	equal(returnedHealth, health,
		"Test with argument: (Number > 0): returned health is correct"
	);
	// Test with 0
	health = this.testMatch.getHealth();
	returnedHealth = this.testMatch.decreaseHealth(0);
	equal(this.testMatch.getHealth(), health,
		"Test with argument: (0)"
	);
	equal(returnedHealth, health,
		"Test with argument: (0): returned health is correct"
	);
	// Test with Number < 0
	hit = -10;
	health = health - hit;
	returnedHealth = this.testMatch.decreaseHealth(hit);
	equal(this.testMatch.getHealth(), health,
		"Test with argument: (Number < 0)"
	);
	equal(returnedHealth, health,
		"Test with argument: (Number < 0): returned health is correct"
	);
	// Test with a Numeric argument greater than current health
	hit = this.testMatch.getHealth() + 10;
	returnedHealth = this.testMatch.decreaseHealth(hit);
	equal(this.testMatch.getHealth(), 0,
		"Test with argument: (Number > Match.health)"
	);
	equal(returnedHealth, 0,
		"Test with argument: (Number > Match.health): " +
		"returned health is correct"
	);
});

test("Match.getCredit()", function(){
	var credit = 300;
	var creditMatch = new Match(100, credit);
	equal(creditMatch.getCredit(), credit, "Test with a regular Match object");
});

test("Match.changeCredit(amount)", function() {
	var credit = this.testMatch.getCredit();
	// Test with undefined argument
	var returnedCredit = this.testMatch.changeCredit();
	credit = credit + 1;
	equal(this.testMatch.getCredit(), credit,
		"Test with argument: (undefined)"
	);
	equal(returnedCredit, credit,
		"Test with argument: (undefined): returned credit is correct"
	);
	// Test with a Number > 0
	var increment = 10;
	credit = credit + increment;
	returnedCredit = this.testMatch.changeCredit(increment);
	equal(this.testMatch.getCredit(), credit,
		"Test with argument: (Number > 0)"
	);
	equal(returnedCredit, credit,
		"Test with argument: (Number > 0): returned credit is correct"
	);
	// Test with 0
	credit = this.testMatch.getCredit();
	returnedCredit = this.testMatch.changeCredit(0);
	equal(this.testMatch.getCredit(), credit,
		"Test with argument: (0)"
	);
	equal(returnedCredit, credit,
		"Test with argument: (0): returned credit is correct"
	);
	// Test with Number < 0
	var decrement = -10;
	credit = credit + decrement;
	returnedCredit = this.testMatch.changeCredit(decrement);
	equal(this.testMatch.getCredit(), credit,
		"Test with argument: (Number < 0)"
	);
	equal(returnedCredit, credit,
		"Test with argument: (Number < 0): returned credit is correct"
	);
	// Test a decrement bigger than available credits
	var available = this.testMatch.getCredit();
	decrement = - (available + 10);
	throws(
		function() { this.testMatch.changeCredit(decrement); },
		RangeError,
		"Test with argument: (Number < 0 and |Number| > Match.credit)"
	);
});

test("Match.getScore()", function() {
	// A new match has score = 0
	equal(this.testMatch.getScore(), 0, "Test with a regular Match object");
});

test("Match.changeScore(amount)", function() {
	var score = this.testMatch.getScore();
	// Test with undefined argument
	var returnedScore = this.testMatch.changeScore();
	score = score + 1;
	equal(this.testMatch.getScore(), score,
		"Test with argument: (undefined)"
	);
	equal(returnedScore, score,
		"Test with argument: (undefined): returned score is correct"
	);
	// Test with a Number > 0
	var increment = 10;
	score = score + increment;
	returnedScore = this.testMatch.changeScore(increment);
	equal(this.testMatch.getScore(), score,
		"Test with argument: (Number > 0)"
	);
	equal(returnedScore, score,
		"Test with argument: (Number > 0): returned score is correct"
	);
	// Test with 0
	score = this.testMatch.getScore();
	returnedScore = this.testMatch.changeScore(0);
	equal(this.testMatch.getScore(), score,
		"Test with argument: (0)"
	);
	equal(returnedScore, score,
		"Test with argument: (0): returned score is correct"
	);
	// Test with Number < 0
	var decrement = -10;
	score = score + decrement;
	returnedScore = this.testMatch.changeScore(decrement);
	equal(this.testMatch.getScore(), score,
		"Test with argument: (Number < 0)"
	);
	equal(returnedScore, score,
		"Test with argument: (Number < 0): returned score is correct"
	);
	// Test a decrement bigger than available score
	var available = this.testMatch.getScore();
	decrement = - (available + 10);
	throws(
		function() { this.testMatch.changeScore(decrement); },
		RangeError,
		"Test with argument: (Number < 0 and |Number| > Match.score)"
	);
});