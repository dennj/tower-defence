var gamejs = require('gamejs');
var preload = require('js/preload');

// Preload of resources
preload.preload();

gamejs.ready(function(){
	require('tests/test_enemy');
	require('tests/test_map');
	require('tests/test_obstacle');
	require('tests/test_radar');
	require('tests/test_match');
	require('tests/test_projectile');
	require('tests/test_turret');
	require('tests/test_utils');
	require('tests/test_gameEntity');
});

