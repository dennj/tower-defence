var gamejs = require('gamejs');

/**
 * Calculate the distance between two points
 *
 * @param {[x,y]} point1 the first point
 * @param {[x,y]} point2 the second point
 *
 * @return {Number} the distance between point1 and point2
 */
var distance = exports.distance = function (point1, point2) {
	var squareX = Math.pow(point1[0]-point2[0], 2);
	var squareY = Math.pow(point1[1]-point2[1], 2);
	return Math.sqrt(squareX + squareY);
};