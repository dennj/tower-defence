var GameEntity = require('js/gameEntity').GameEntity;
var gamejs = require('gamejs');

/*
 * OBSTACLE: an object representing an obstacle
 */

/**
 * Creates a new obstacle
 *
 * @param {array} center The coordinates as vector [x, y]
 * @param {number} radius The radius of the obstacle
 * @param {Number} level The level of the obstacle [default 0]
 *
 * @extends Sprite
 * @constructor
 */
var Obstacle = exports.Obstacle = function(match, center, level) {
	Obstacle.superConstructor.call(this, match, center, Obstacle.UPGRADE_LIST,
		true, level
	);
	this.radius = this.image.getSize()[0] / 2;
};

/**
 * Extending GameEntity with Obstacle
 */
gamejs.utils.objects.extend(Obstacle, GameEntity);

/**
 * The list of available upgrades
 */
Obstacle.UPGRADE_LIST = [
	// Level 0
	{ image: IMAGE_ROOT + "obstacle0.png", imageShape: 'circular'},
	// Level 1
	{ image: IMAGE_ROOT + "obstacle1.png", imageShape: 'circular'},
	// Level 2
	{ image: IMAGE_ROOT + "obstacle2.png", imageShape: 'circular'}
];

/**
 * Returns the radius of the Obstacle
 *
 * @returns {number} The radius of the Obstacle
 */
Obstacle.prototype.getRadius = function() {
	return this.radius;
};