var gamejs = require('gamejs');
var utils = require('js/utils');

var SHAPE_CIRCULAR = 'circular';
var SHAPE_RECTANGULAR = 'rectangular';
/**
 * GameEntity is an extension of gamejs.Sprite that integrates features
 * previously duplicated across Turret, Enemy, Radar ...
 *
 * These features are:
 *     - Match management
 *     - Levels and upgrade managment
 *     - Center, rect, image and coordinates managment
 *
 * @param {Match} match, the Match were this GameEntity lives
 * @param {Array} center, the GameEntity location on the Match Map, in
 *                [x,y] format
 * @param {Array} upgradeList, the upgradeList used for this GameEntity
 *                (see GameEntity.UPGRADE_LIST for more information)
 * @param {Boolean} fillsMap, True if the GameEntity fills the space on the Map,
 *                  blocking other entities. The clearing/filling of points
 *                  it's done directly by the Map objects checking this field
 *                  with GameEntity.fillsMap() method.
 *                  [default false]
 * @param {Number} startingLvl, the level used to spawn the GameEntity
 *                [default 0]
 * @param {String} Shape, a string choosed between "rectangular" and
 *                 "circular". It's used for the collision between other
 *                 GameEntities [default "circular"]
 *
 * @throws {RangeError} When startingLvl is < 0 or bigger than the maximum
 *                      reachable level specified in the upgradeList
 * @throws {Error} When used with an invalid Shape
 */
var GameEntity = function(match, center, upgradeList, fillsMap, startingLvl, shape) {
	// Call gamejs.Sprite constructor
	GameEntity.superConstructor.apply(this);
	// Set all the private internal variables
	this._match = match;
	this._fillsMap = fillsMap || false;
	this._upgradeList = upgradeList;
	this._level = startingLvl || 0;
	// Check if the level is acceptable
	if (this._level < 0 || this._level > this._upgradeList.length - 1) {
		throw new RangeError('GameEntity: invalid starting level!');
	}
	// Set and check Shape
	this._shape = shape || SHAPE_CIRCULAR;
	if ( ! (this._shape === SHAPE_CIRCULAR ||
			this._shape === SHAPE_RECTANGULAR) ) {
		throw new Error("GameEntity: invalid Shape");
	}
	// Set this.image and this.rect needed by gamejs.Sprite
	this.updateImage();
	this.createRect(center);
	// Set the radius for circular gameEntities
	if (this._shape === SHAPE_CIRCULAR) {
		this.radius = this.image.getSize[0] / 2;
	}

};

// GameEntity extends gamejs.Sprite
gamejs.utils.objects.extend(GameEntity, gamejs.sprite.Sprite);

// Export GameEntity
exports.GameEntity = GameEntity;

// ========= MATCH MANAGMENT =========
/**
 * Returns the match where the GameEntity lives
 *
 * @param {Match}
 */
GameEntity.prototype.getMatch = function() {
	return this._match;
};

/**
 * Returns the Map used by the Match where the GameEntity lives
 *
 * @param {Map}
 */
GameEntity.prototype.getMatchMap = function() {
	return this._match.getMap();
};

// ========= COORDINATES MANAGMENT =========
/**
 * Returns the gameEntity shape
 *
 * @returns {String} "rectangular" or "circular"
 */
GameEntity.prototype.getShape = function() {
	return this._shape;
};

/**
 * Returns the center coordinates
 *
 * @returns {Array} center's coordinates using [x,y] format
 */
GameEntity.prototype.getCenter = function() {
	return this.rect.center;
};

/**
 * Returns the rect of this GameEnemy
 *
 * @returns {gamejs.Rect}
 */
GameEntity.prototype.getRect = function() {
	return this.rect;
};

/**
 * Checks if the GameEntity fills Map space
 *
 * @returns {Boolean} true if GameEntity fills Map space, false otherwise
 */
GameEntity.prototype.fillsMap = function() {
	return this._fillsMap;
};

/**
 * Returns all the points occupied by the GameEntity on the Match's Map.
 * This method uses the Shape information stored in the upgradeList.
 * If the imageShape property is invalid or undefined the default rectangular
 * one will be used.
 *
 * @returns {Array} list of coordinates in [x,y] format.
 */
GameEntity.prototype.pointsOnMap = function() {
	var shape = this.getLevelSettings().imageShape;
	if (shape === 'circular') {
		return this._circlePoints();
	}
	else {
		return this._rectPoints();
	}
};

// ========= LEVEL MANAGMENT =========
/**
 * An upgradeList must be an Array of objects, each object containing the
 * settings for a level, ordered from the first to the last:
 *    - upgradeList[0] it's the object with the setting for the first level
 *    - upgradeList[length-1] it's the object with the settings for the last
 *      level (the maximum reachable level by the GameEntity)
 *
 * Each object MUST have the 'image' property with the relative (to the
 * html page running the game) url to the image used for the GameEntity.
 * Relative urls must be used for a bug (or maybe stupid inteded feature)
 * of gamejs.image.load() method; absolute urls aren't loaded correctly.
 *
 * NOTE ON USING DIFFERENT IMAGES ON DIFFERENT LEVELS:
 * If you are using a GameEntity with fillsMap = false there is no problem.
 * If you are using a GameEntity with fillsMap = true things are more
 * complicated:
 *    - if you want to use the the upgrade method make sure that images
 *      across levels have the same size and shape. Wierd things can happen
 *      when upgrading a GameEntity to a bigger image.
 *  or
 *    - if you want levels with images of different size don't use the
 *      upgrade method. Instantiate directly the GameEntity to the desired
 *      level.
 *
 * Another two optional properties are:
 *    - 'price': used to store the number of credit to buy the upgrade
 *       [default 0]
 *    - 'resaleValue': used to store the number of credit that the player
 *       can make by selling the GameEntiy [default 0]
 *
 * When extending the GameEntity you can obviously add all the properties
 * that you need.
 *
 * To query a particular property you can use the getLevelSettings() to get
 * the current level object and then use the stantard js property access
 * syntax on the returned object. For example:
 *     exampleEntity.getLevelSettings().propertyName
 * or equivalently:
 *     exampleEntity.getLevelSettings()['propertyName']
 */
GameEntity.UPGRADE_LIST = [
	// Level 0
	{image: '/url/to/GameEntity0.png', price: 100},
	// Level 1
	{image: '/url/to/GameEntity1.png', price: 200},
	// Level 2, here imageShape uses the default value 'rectangular'
	{image: '/url/to/GameEntity2.png', userDefinedProperty: 'whatever'}
];

/**
 * Return the current level
 *
 * @return {Number}
 */
GameEntity.prototype.getLevel= function() {
	return this._level;
};

/**
 * Returns true if the GameEntity is at the maximum allowed level, false
 * otherwise
 *
 * @returns {Boolean}
 */
GameEntity.prototype.isAtMaximumLevel = function() {
	if (this.getLevel() === this._upgradeList.length - 1) {
		return true;
	}
	else {
		return false;
	}
};

/**
 * Returns true if the GameEntity can be upgraded (there's another available
 * level and enough credits), false otherwise
 *
 * @returns {Boolean}
 */
GameEntity.prototype.canUpgrade = function() {
	var credits = this.getMatch().getCredit();
	if ( ! this.isAtMaximumLevel() && this.getUpgradePrice() <= credits) {
		return true;
	}
	else {
		return false;
	}
};

/**
 * Return the upgradeList
 *
 * @return {Array}
 */
GameEntity.prototype.getUpgradeList= function() {
	return this._upgradeList;
};

/**
 * Returns the Object in the upgradeList containing the settings for
 * a level.
 *
 * Note: levels are zero indexed (0 is the first level, 1 is second one, ...)
 *
 * @param {Number} level [default current level]
 *
 * @returns {Object}
 *
 * @throws {RangeError} If level it's not a valid level
 */
GameEntity.prototype.getLevelSettings = function(level) {
	level = level || this._level;
	// Check if the argument is valid
	var maximumLevel = this._upgradeList.length - 1;
	if (typeof level !== 'number' || level < 0 || level > maximumLevel){
		throw new RangeError("Invalid level");
	}
	return this._upgradeList[level];
};

/**
 * Return the number of credit needed to upgrade the GameEntity.
 *
 * This uses the property 'price' inside the upgradeList.
 *
 * @return {Number}
 *
 * @throws {RangeError} If the GameEntity is already at the maximum level
 */
GameEntity.prototype.getUpgradePrice= function() {
	if ( this.isAtMaximumLevel() ) {
		throw new Error('GameEntity: Already at the maximum level');
	}
	var nextLevel = this._level + 1;
	// if no price is specified the upgrade is free
	var price = this.getLevelSettings(nextLevel).price || 0;
	return price;
};

/**
 * Upgrades the GameEntity by one level
 *
 * @throws {Error} If the GameEntity is already at the maximum level.
 * @throws {Error} If there isn't enough credits for the upgrade
 * @throws {Error} If the GameEntity cannot be upgrade for space problems:
 *                 this can happen when the upgraded version is bigger
 *                 than the "to be upgraded" one and there isn't enough space
 *                 around it to increase it's size.
 */
GameEntity.prototype.upgrade = function() {
	// Check if the maximum level is already reached
	if ( this.isAtMaximumLevel() ) {
		throw new Error('GameEntity: Already at the maximum level');
	}
	// Check that the user has enough money
	if ( this.getUpgradePrice() > this.getMatch().getCredit()) {
		throw new Error('GameEntity: Not enough credits for the upgrade');
	}
	// Get the new and old image
	var oldImage = this.getLevelSettings().image;
	var newImage = this.getLevelSettings(this._level + 1 ).image;
	// Update the level
	this._level ++;
	// If the new image is different from the old one we must update
	// this.image
	if (oldImage !== newImage) {
		this.updateImage();
		this.createRect(this.rect.center);
	}
	return;
};

/**
 * Return the resaleValue of the GameEntity (0 if resaleValue is undefined)
 *
 * This uses the property 'resaleValue' inside the upgradeList.
 *
 * @return {Number}
 *
 */
GameEntity.prototype.getResaleValue= function() {
	return this.getLevelSettings().resaleValue || 0;
};

/**
 * Set this.image by loading the correct one from the upgradeList
 */
GameEntity.prototype.updateImage = function() {
	this.image = gamejs.image.load(this.getLevelSettings().image);
};

/**
 * Set this.rect using the center coordinates and the size of this.image.
 * Must be called after defining this.image
 *
 * @param {Array} center, the GameEntity location on the Match Map, in
 *                [x,y] format
 */
GameEntity.prototype.createRect = function(center) {
	var dims = this.image.getSize();
	var topLeftCornerX = Math.ceil(center[0] - (dims[0] / 2));
	var topLeftCornerY = Math.ceil(center[1] - (dims[1] / 2));
	var topLeftCorner = [topLeftCornerX, topLeftCornerY];
	this.rect = new gamejs.Rect(topLeftCorner, dims);
};

/**
 * Finds all the points occupied on the Map by a GameEntity having a
 * circular imageShape
 *
 * @return {Array} list of coordinates in [x,y] format.
 */
GameEntity.prototype._circlePoints = function() {
	// Get the map
	var map = this.getMatchMap();
	// Get the GameEntity radius and center
	var radius = this.image.getSize()[0] / 2;
	var center = this.getCenter();
	// Get center's coordinates
	var centerX = this.getCenter()[0];
	var centerY = this.getCenter()[1];

	// Calculate the coordinates of the square circumscribing the circle
	var squareFromX = centerX - radius;
	var squareToX = centerX + radius;
	var squareFromY = centerY - radius;
	var squareToY = centerY + radius;

	// Define some variables used inside the for loops
	var resultsArray = [];
	var x, y, point;

	// Iterate over all the pixels of the square enclosing the obstacle
	for (x = squareFromX; x < squareToX; x++) {
		for (y = squareFromY; y < squareToY; y++) {
			point = [x,y];
			// Skip the point if it's outside the map
			if ( ! map.isInRange(point)) {
				continue;
			}
			// Skip the point if it's in one of the four parts of the square
			// that are not part of the circle
			else if (utils.distance(center, point) > radius) {
				continue;
			}
			// Finally the "good" points
			else {
				resultsArray.push(point);
			}
		}
	}
	return resultsArray;
};

/**
 * Finds all the points occupied on the Map by a GameEntity having a
 * rectangular imageShape
 *
 * @return {Array} list of coordinates in [x,y] format.
 */
GameEntity.prototype._rectPoints = function() {
	// Get the map
	var map = this.getMatchMap();
	// Get the borders' coordinates
	var leftBorder = this.rect.topleft[0];
	var rightBorder = this.rect.bottomright[0];
	var topBorder = this.rect.topleft[1];
	var bottomBorder = this.rect.bottomright[1];

	// Define some variables used inside the for loops
	var resultsArray = [];
	var x, y, point;

	// Iterate over all the points
	for (x = leftBorder; x <= rightBorder; x++) {
		for (y = topBorder; y <= bottomBorder; y++) {
			point = [x,y];
			if ( ! map.isInRange(point)) {
				// Skip the point if it's outside the map
				continue;
			}
			// Finally the "good" points
			else {
				resultsArray.push(point);
			}
		}
	}
	return resultsArray;
};