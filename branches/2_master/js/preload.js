var gamejs = require('gamejs');

/**
 * Preload all resources needed by the game
 */
var preload = exports.preload = function() {
	gamejs.preload([
		IMAGE_ROOT + 'background.png',
		IMAGE_ROOT + 'enemy.png',
		IMAGE_ROOT + 'enemy_detected.png',
		IMAGE_ROOT + 'obstacle0.png',
		IMAGE_ROOT + 'obstacle1.png',
		IMAGE_ROOT + 'obstacle2.png',
		IMAGE_ROOT + 'radar.png',
		IMAGE_ROOT + 'turret.png',
		IMAGE_ROOT + 'menu.png',
		IMAGE_ROOT + 'instructionsEx.png',
		IMAGE_ROOT + 'turret1.png',
		IMAGE_ROOT + 'add.png',
		IMAGE_ROOT + 'action.png'
	]);
};