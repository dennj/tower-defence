var gamejs = require("gamejs");
var matchModule = require("js/match");

/**
 * Obstacle : an object representing an obstacle.
 * It blocks enemies, the radars ray and some bullet types.
 * 
 * @extends Sprite
 */

/**
 * Creates a new obstacle with the given center and radius.
 *
 * @param {Match} match The match where this object will work.
 * @param {[number, number]} center The coordinates as vector [x, y].
 * @param {number} radius The radius of the obstacle.
 * @throws {TypeError} If match isn't a Match object or center isn't in
 * the correct format.
 * @throws {RangeError} If the given coordinates aren't inside the map recovered
 * from the given Match, or the given radius isn't greater than zero.
 * 
 * @constructor
 */
var Obstacle = exports.Obstacle = function(match, center, radius) {
	if( ! (match instanceof matchModule.Match)) {
		throw new TypeError("match isn't a valid Match object");
	}
	
	if( ! match.getMap().isInside(center)) {
		throw new RangeError("Invalid coordinates");
	}
	
	if(radius <= 0) {
		throw new RangeError("Radius must be greater than 0");
	}
	
	Obstacle.superConstructor.apply(this, arguments);
	this.currentMatch = match;
	this.center = center;
	this.saleable = false;
	this.price = 0;
	this.type = "obstacle";

	this.originalImage = gamejs.image.load(imgPath + "obstacle.png");
	this.image = gamejs.transform.scale(this.originalImage, [radius * 2, radius * 2]);
	
	var dims = this.image.getSize();
	var topLeftCorner = [this.center[X] - (dims[WIDTH] / 2), this.center[Y] - (dims[HEIGHT] / 2)];
	this.rect = new gamejs.Rect(topLeftCorner, dims);
	
	//radius for collideCircle
	this.radius = radius;
};

/**
 * Obstacle extends Sprite.
 */
gamejs.utils.objects.extend(Obstacle, gamejs.sprite.Sprite);

/**
 * Large-sized obstacle
 * @constant
 */
Obstacle.prototype.SIZE_BIG = 80;

/**
 * Mid-sized obstacle
 * @constant
 */
Obstacle.prototype.SIZE_MEDIUM = 60;

/**
 * Small-sized obstacle
 * @constant
 */
Obstacle.prototype.SIZE_SMALL = 40;

/**
 * Returns the coordinates of the obstacle's center.
 *
 * @returns {[number, number]} The obstacle's center as vector [x, y].
 */
Obstacle.prototype.getCenter = function() {
	return this.center;
};

/**
 * Return the Match object where this obstacle works.
 * 
 * @returns {Match} The Match object where this obstacle works.
 */
Obstacle.prototype.getMatch = function() {
	return this.currentMatch;
};

/**
 * Returns the radius of the obstacle.
 *
 * @returns {number} The radius of the obstacle.
 */
Obstacle.prototype.getRadius = function() { 
	return this.radius;
};

/**
 * Returns the resale value of the obstacle.
 *
 * @return {number} The amount of credits refunded when this obstacle is sold.
 */
Obstacle.prototype.getResaleValue = function() {
	return 0;
};

/**
 * Makes the obstacle saleable or not.
 *
 * @param {boolean} True to make saleable the obstacle, false to make
 * it not saleable.
 */
Obstacle.prototype.setSaleable = function(value) {
	if(value) {
		this.saleable = true;
	}
	else {
		this.saleable = false;
	}
};

/**
 * Tells if the obstacle is saleable.
 *
 * @return {boolean} True if the obstacle is saleable, false otherwise.
 */
Obstacle.prototype.getSaleable = function() {
	return this.saleable;
};

/**
 * Sells the obstacle, removing it from the map.
 * 
 * @throws {Error(InvalidOperationError)} If the obstacle isn't present in the map or
 * if it's present but isn't saleable.
 */
Obstacle.prototype.sell = function() {
	this.currentMatch.getMap().removeObstacle(this);
};