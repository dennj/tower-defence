var gamejs = require("gamejs");
var Sprite = require("gamejs/sprite").Sprite;
var Font = require("gamejs/font").Font;
var rectModule = require("gamejs/draw");
var radarModule = require("js/radar");
var obstacleModule = require("js/obstacle");
var turretModule = require("js/turret");

/**
*Store Scene where player can buy obstacles, turrets and radars
*/

/**
 * Creates a StoreScene object, here the player can buy obstacles, turrets and radars.
 * @param {ScenesDirector} director The ScenesDirector object that directs the game.
 * @param {Match} match The Match object representing the game.
 * 
 * @constructor
 */
var StoreScene = exports.StoreScene = function(director, match){
	this.director = director;
	this.match = match;
	this.credit = match.getCredit();
	
	//background image
	this.background = gamejs.image.load(imgPath + "startBackground.png");
	
	//arrow image
	this.arrow = new Sprite();
	var arrow = gamejs.image.load(imgPath + "backArrow.png");
	this.arrow.image = gamejs.transform.scale(arrow, [this.IMAGESIZE[0] - 20, this.IMAGESIZE[1] - 20]);	
	this.arrow.rect = new gamejs.Rect([0,0], this.arrow.image.getSize());
	
    //info image
	this.info = new Sprite();
	var info = gamejs.image.load(imgPath + "moreInfo.png");
	this.info.image = gamejs.transform.scale(info, [30, 30]);
	
	//money image
	this.money = new Sprite();
	var money = gamejs.image.load(imgPath + "money.png");
	this.money.image = gamejs.transform.scale(money, [30, 30]);
	
	//starting top left position where will appear images
	var startX = 5;
	var startY = 120;
	
	//radars object
	this.radar1 = new Sprite();
	var radar1Img = gamejs.image.load(imgPath + "radarStore1.png");
	this.radar1.image = gamejs.transform.scale(radar1Img, this.IMAGESIZE);
	this.radar1.rect = new gamejs.Rect([startX, startY], this.IMAGESIZE);
	
	this.radar2 = new Sprite();
	radar2Img = gamejs.image.load(imgPath + "radarStore2.png");
	this.radar2.image = gamejs.transform.scale(radar2Img, this.IMAGESIZE);
	this.radar2.rect = new gamejs.Rect([this.MAXWIDTH/5, startY], this.IMAGESIZE);
	
	this.radarObject = new radarModule.Radar(this.match, [50, 50]);    //miss coords
	
	//obstacles object
	this.obstacle1 = new Sprite();
	obstacle1Img = gamejs.image.load(imgPath + "obstacleStore1.png");
	this.obstacle1.image = gamejs.transform.scale(obstacle1Img, this.IMAGESIZE);
	this.obstacle1.rect = new gamejs.Rect([this.MAXWIDTH/5 * 2, startY], this.IMAGESIZE);
	
	this.obstacle2 = new Sprite();
	obstacle2Img = gamejs.image.load(imgPath + "obstacleStore2.png");
	this.obstacle2.image = gamejs.transform.scale(obstacle2Img, this.IMAGESIZE);
	this.obstacle2.rect = new gamejs.Rect([this.MAXWIDTH/5 * 3, startY], this.IMAGESIZE);
	
	this.obstacle3 = new Sprite();
	obstacle3Img = gamejs.image.load(imgPath + "obstacleStore3.png");
	this.obstacle3.image = gamejs.transform.scale(obstacle3Img, this.IMAGESIZE);
	this.obstacle3.rect = new gamejs.Rect([this.MAXWIDTH/5 * 4, startY], this.IMAGESIZE);
	
	this.obstacleObject = new obstacleModule.Obstacle(this.match, [30,30], 5);//miss center and radius
	
	//new line
	startY += (this.MAXHEIGHT-startY) / 2;
	
	//turret object
	this.turret1 = new Sprite();
	turret1Img = gamejs.image.load(imgPath + "turretStore1.png");
	this.turret1.image = gamejs.transform.scale(turret1Img, this.IMAGESIZE);
	this.turret1.rect = new gamejs.Rect([startX, startY], this.IMAGESIZE);
	
	this.turret2 = new Sprite();
	turret2Img = gamejs.image.load(imgPath + "turretStore2.png");
	this.turret2.image = gamejs.transform.scale(turret2Img, this.IMAGESIZE);
	this.turret2.rect = new gamejs.Rect([this.MAXWIDTH/5, startY], this.IMAGESIZE);
	
	this.turret3 = new Sprite();
	turret3Img = gamejs.image.load(imgPath + "turretStore3.png");
	this.turret3.image = gamejs.transform.scale(turret3Img, this.IMAGESIZE);
	this.turret3.rect = new gamejs.Rect([this.MAXWIDTH/5 * 2, startY], this.IMAGESIZE);
	
	this.turretObject = new turretModule.Turret(this.match, [400,400]);//miss coords
}

/**
 * The max x dimension of the game screen.
 * @constant
 */
StoreScene.prototype.MAXWIDTH = 800;

/**
 * The max y dimension of the game screen.
 * @constant
 */
StoreScene.prototype.MAXHEIGHT = 480;

/**
 * The size of the marketable images.
 * @constant
 */
StoreScene.prototype.IMAGESIZE = [100, 100];

/**
 *The width of the boxes that contain the marketable images.
 *@constant 
 */
StoreScene.prototype.BOXWIDTH = 1;

/**
 *Updates the scene and its contents, managing the various events.
 */
StoreScene.prototype.update = function() {
        var events = new gamejs.event.get();
        for(i = 0; i < events.length; i++) {
          var currentEvent = events[i];
          if(currentEvent.type == gamejs.event.MOUSE_UP && this.radar1.rect.collidePoint(currentEvent.pos)) {
            this.director.previousScene().setToPlaceItem(new radarModule.Radar(this.match, currentEvent.pos));
            this.director.popScene();
            break;
          }
          if(currentEvent.type == gamejs.event.MOUSE_UP && this.turret1.rect.collidePoint(currentEvent.pos)) {
            this.director.previousScene().setToPlaceItem(new turretModule.Turret(this.match, currentEvent.pos));
            this.director.popScene();
            break;
          }
          if(currentEvent.type == gamejs.event.MOUSE_UP && this.obstacle1.rect.collidePoint(currentEvent.pos)) {
            this.director.previousScene().setToPlaceItem(new obstacleModule.Obstacle(this.match, currentEvent.pos, 80));
            this.director.popScene();
            break;
          }
          if(currentEvent.type == gamejs.event.MOUSE_UP && this.arrow.rect.collidePoint(currentEvent.pos)) {
            this.director.popScene();
            break;
          }
        }
	
}

/**
 * Draws the scenes  into the director surface
 */
StoreScene.prototype.draw = function() {
	var surface = this.director.getSurface();
	surface.blit(this.background);
	
	//credits
	var myFont = new Font("20px Verdana", "#000000");
	var money = myFont.render("CREDITS: " + this.credit, "#0033FF");
	surface.blit(money, [300, 10]);
	
	//starting top left position where will appear images
	var startX = 5;
	var startY = 120;

	//utilities
	var price = "PRICE: ";
	var tmpRender;
	var dy = 5;			//eliminates the "magic number", it is an added distance in y axis between the box and the text
	var dyIcon = 30;	//eliminates the "magic number", it is an added distance in y axis between the box and the info or money icon
	var dxMoney = 40;	//eliminates the "magic number", it is an added distance in x axis between the info and money icon
	
	//draw the various object to buy

	this.arrow.draw(surface);
	
	gamejs.draw.rect(surface, "#4B4B4B", new gamejs.Rect([startX, startY], this.IMAGESIZE), this.BOXWIDTH);	
	this.radar1.draw(surface);
	tmpRender = myFont.render(price + this.radarObject.priceToBuild(), "#0033FF");
	surface.blit(tmpRender, [startX, startY + this.IMAGESIZE[1] + dy]);
	this.info.rect = new gamejs.Rect([startX, startY + this.IMAGESIZE[1] + dyIcon]);
	this.info.draw(surface);
	this.money.rect = new gamejs.Rect([startX + dxMoney, startY + this.IMAGESIZE[1] + dyIcon]);
	this.money.draw(surface);
	
	gamejs.draw.rect(surface, "#4B4B4B", new gamejs.Rect([this.MAXWIDTH/5, startY], this.IMAGESIZE), this.BOXWIDTH);
	this.radar2.draw(surface);
	tmpRender = myFont.render(price + this.radarObject.priceToBuild(), "#0033FF");
	surface.blit(tmpRender, [this.MAXWIDTH/5, startY + this.IMAGESIZE[1] + dy]);
	this.info.rect = new gamejs.Rect([this.MAXWIDTH/5, startY + this.IMAGESIZE[1] + dyIcon]);
	this.info.draw(surface);
	this.money.rect = new gamejs.Rect([this.MAXWIDTH/5 + dxMoney, startY + this.IMAGESIZE[1] + dyIcon]);
	this.money.draw(surface);
	
	gamejs.draw.rect(surface, "#4B4B4B", new gamejs.Rect([this.MAXWIDTH/5 * 2, startY], this.IMAGESIZE), this.BOXWIDTH);
	this.obstacle1.draw(surface);
	tmpRender = myFont.render(price + this.obstacleObject.price, "#0033FF");
	surface.blit(tmpRender, [this.MAXWIDTH/5 * 2, startY + this.IMAGESIZE[1] + dy]);
	this.info.rect = new gamejs.Rect([this.MAXWIDTH/5 * 2, startY + this.IMAGESIZE[1] + dyIcon]);
	this.info.draw(surface);
	this.money.rect = new gamejs.Rect([(this.MAXWIDTH/5 * 2) + dxMoney, startY + this.IMAGESIZE[1] + dyIcon]);
	this.money.draw(surface);
	
	gamejs.draw.rect(surface, "#4B4B4B", new gamejs.Rect([this.MAXWIDTH/5 * 3, startY], this.IMAGESIZE), this.BOXWIDTH);
	this.obstacle2.draw(surface);
	tmpRender = myFont.render(price + this.obstacleObject.price, "#0033FF");
	surface.blit(tmpRender, [this.MAXWIDTH/5 * 3, startY + this.IMAGESIZE[1] + dy]);
	this.info.rect = new gamejs.Rect([this.MAXWIDTH/5 * 3, startY + this.IMAGESIZE[1] + dyIcon]);
	this.info.draw(surface);
	this.money.rect = new gamejs.Rect([(this.MAXWIDTH/5 * 3) + dxMoney, startY + this.IMAGESIZE[1] + dyIcon]);
	this.money.draw(surface);
	
	gamejs.draw.rect(surface, "#4B4B4B", new gamejs.Rect([this.MAXWIDTH/5 * 4, startY], this.IMAGESIZE), this.BOXWIDTH);
	this.obstacle3.draw(surface);
	tmpRender = myFont.render(price + this.obstacleObject.price, "#0033FF");
	surface.blit(tmpRender, [this.MAXWIDTH/5 * 4, startY + this.IMAGESIZE[1] + dy]);
	this.info.rect = new gamejs.Rect([this.MAXWIDTH/5 * 4, startY + this.IMAGESIZE[1] + dyIcon]);
	this.info.draw(surface);
	this.money.rect = new gamejs.Rect([(this.MAXWIDTH/5 * 4) + dxMoney, startY + this.IMAGESIZE[1] + dyIcon]);
	this.money.draw(surface);
	
	//new line
	startY += (this.MAXHEIGHT-startY) / 2;    	
	
	gamejs.draw.rect(surface, "#4B4B4B", new gamejs.Rect([startX, startY], this.IMAGESIZE), this.BOXWIDTH);
	this.turret1.draw(surface);
	tmpRender = myFont.render(price + this.turretObject.priceToBuild(), "#0033FF");
	surface.blit(tmpRender, [startX, startY + this.IMAGESIZE[1] + dy]);
	this.info.rect = new gamejs.Rect([startX, startY + this.IMAGESIZE[1] + dyIcon]);
	this.info.draw(surface);
	this.money.rect = new gamejs.Rect([startX + dxMoney, startY + this.IMAGESIZE[1] + dyIcon]);
	this.money.draw(surface);

	gamejs.draw.rect(surface, "#4B4B4B", new gamejs.Rect([this.MAXWIDTH/5, startY], this.IMAGESIZE), this.BOXWIDTH);
	this.turret2.draw(surface);
	tmpRender = myFont.render(price + this.turretObject.priceToBuild(), "#0033FF");
	surface.blit(tmpRender, [this.MAXWIDTH/5, startY + this.IMAGESIZE[1] + dy]);
	this.info.rect = new gamejs.Rect([this.MAXWIDTH/5, startY + this.IMAGESIZE[1] + dyIcon]);
	this.info.draw(surface);
	this.money.rect = new gamejs.Rect([this.MAXWIDTH/5 + dxMoney, startY + this.IMAGESIZE[1] + dyIcon]);
	this.money.draw(surface);
	
	gamejs.draw.rect(surface, "#4B4B4B", new gamejs.Rect([this.MAXWIDTH/5 * 2, startY], this.IMAGESIZE), this.BOXWIDTH);
	this.turret3.draw(surface);
	tmpRender = myFont.render(price + this.turretObject.priceToBuild(), "#0033FF");
	surface.blit(tmpRender, [this.MAXWIDTH/5 * 2, startY + this.IMAGESIZE[1] + dy]);
	this.info.rect = new gamejs.Rect([this.MAXWIDTH/5 * 2, startY + this.IMAGESIZE[1] + dyIcon]);
	this.info.draw(surface);
	this.money.rect = new gamejs.Rect([(this.MAXWIDTH/5 * 2) + dxMoney, startY + this.IMAGESIZE[1] + dyIcon]);
	this.money.draw(surface);

	gamejs.draw.rect(surface, "#4B4B4B", new gamejs.Rect([this.MAXWIDTH/5 * 3, startY], this.IMAGESIZE), this.BOXWIDTH);
	
	gamejs.draw.rect(surface, "#4B4B4B", new gamejs.Rect([this.MAXWIDTH/5 * 4, startY], this.IMAGESIZE), this.BOXWIDTH);
}
