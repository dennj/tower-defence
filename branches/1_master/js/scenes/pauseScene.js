var gamejs = require("gamejs");
var Sprite = require("gamejs/sprite").Sprite;
var Font = require("gamejs/font").Font;
var rectModule = require("gamejs/draw");

/**
 * PauseScene, is the scene where you can stop playing
 */
 
/**
 * Creates a PauseScene object
 *
 * @param {ScenesDirector} director The SceneDirector that directs the game
 * @param {Match} match The match object that representing the game
 *
 * @constructor
 */
var PauseScene = exports.PauseScene = function(director, match) {
        this.director = director;
        this.match = match;

         //arrow image
	this.arrow = new Sprite();
	var arrow = gamejs.image.load(imgPath + "backArrow.png");
	this.arrow.image = gamejs.transform.scale(arrow, [this.IMAGESIZE[0] - 20, this.IMAGESIZE[1] - 20]);
	this.arrow.rect = new gamejs.Rect([0,0], this.arrow.image.getSize());

	//background image
	this.background = gamejs.image.load(imgPath + "startBackground.png");

	this.font = new gamejs.font.Font("72px Verdana");


};

/**
 * The size of the marketable images.
 * @constant
 */
PauseScene.prototype.IMAGESIZE = [100, 100];

/**
 * The center of the screen
 * @constant
 */
PauseScene.prototype.CENTER = [280, 190];

/**
 * Updates the scene menaging an event
 *
 * @param {number] msDuration The time past from the last call, in ms
 */
PauseScene.prototype.update = function(msDuration) {
        var events = new gamejs.event.get();
        for(i = 0; i < events.length; i++) {
          var currentEvent = events[i];
          if(currentEvent.type == gamejs.event.MOUSE_UP && this.arrow.rect.collidePoint(currentEvent.pos)) {
            this.director.popScene();
            break;
          }
        }
};

/**
 * Draws the scene and its contents in the director Surface.
 */
PauseScene.prototype.draw = function() {
        var surface = this.director.getSurface();
        surface.blit(this.background);
        surface.blit(this.font.render("PAUSE", "#FF0000"), this.CENTER);
        this.arrow.draw(surface);
};