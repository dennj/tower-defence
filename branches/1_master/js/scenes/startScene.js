var gamejs = require("gamejs");
var Sprite = require("gamejs/sprite").Sprite;
var daySceneModule = require("js/scenes/dayScene");
var Match = require("js/match").Match;
var Map = require("js/map").Map;

/**
 * StartScene, the scene to show at start. It contains a simple menu.
 */

/**
 * Creates a StartScene object, it will draw and manage a start menu.
 * @param {ScenesDirector} director The ScenesDirector object that directs the game.
 * 
 * @constructor
 */
var StartScene = exports.StartScene = function(director) {
	this.director = director;
	
	//background image
	this.background = gamejs.image.load(imgPath + "startBackground.png");
	
	//new game image
	this.newGame = new Sprite();
	this.newGame.image = gamejs.image.load(imgPath + "newGame.png");
	this.newGame.rect = new gamejs.Rect([270, 50], this.newGame.image.getSize());
	
	//how to play image
	this.howToPlay = new Sprite();
	this.howToPlay.image = gamejs.image.load(imgPath + "howToPlay.png");
	this.howToPlay.rect = new gamejs.Rect([270, 150], this.howToPlay.image.getSize());
	
	//credits image
	this.credits = new Sprite();
	this.credits.image = gamejs.image.load(imgPath + "credits.png");
	this.credits.rect = new gamejs.Rect([270, 250], this.credits.image.getSize());
};

/**
 * Updates the scene changing the active scene according to the mouse click.
 * 
 * @param {number} msDuration The time past from the last call, in ms.
 */
StartScene.prototype.update = function(msDuration) {
	var events = gamejs.event.get();

	for ( i = 0; i < events.length; i++) {
		var currentEvent = events[i];
		//process only the mouse click
		if (currentEvent.type == gamejs.event.MOUSE_UP) {
			var point = currentEvent.pos;
			if (this.newGame.rect.collidePoint(point)) {
				//click on new game
				var match = new Match();
				match.setMap(new Map(match, MAP_WIDTH, MAP_HEIGHT));
				this.director.changeScene(new daySceneModule.DayScene(this.director, match));
				break;
			}

			if (this.howToPlay.rect.collidePoint(point)) {
				//click on how to play
				//this.director.pushScene(new HowToScene(this.director));
				break;
			}

			if (this.credits.rect.collidePoint(point)) {
				//click on credits
				//this.director.pushScene(new CreditsScene(this.director));
				break;
			}
		}
	}
};

/**
 * Draws the scene in the director Surface:
 * a background and a simple menu.
 */
StartScene.prototype.draw = function() {
	var surface = this.director.getSurface();
	surface.blit(this.background);
	this.newGame.draw(surface);
	this.howToPlay.draw(surface);
	this.credits.draw(surface);
};