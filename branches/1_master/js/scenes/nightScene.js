var gamejs = require("gamejs");
var Sprite = require("gamejs/sprite").Sprite;
var radarModule = require("js/radar");
var enemyModule = require("js/enemy");
var obstacleModule = require("js/obstacle");
var turretModule = require("js/turret");
var pauseSceneModule = require("js/scenes/pauseScene");

/**
 * NightScene, the scene where our planet will be attacked by a wave of enemies.
 */

/**
 * Creates a NightScene object, in this scene enemies will attack the planet.
 * 
 * @param {ScenesDirector} director The ScenesDirector object that directs the game.
 * @param {Match} match The Match object representing the game.
 * @param {Array} wave An Array of object in which each object has an enemy property that
 * stores an Enemy object and a time property that stores the time when show the enemy.
 * The time property of successive elements must be in non decreasing order.
 * 
 * @constructor
 */
var NightScene = exports.NightScene = function(director, match, wave) {
	this.director = director;
	this.match = match;
	this.selectedItems = null;
	this.toPlaceItem = null;
	this.timer = 0;//to create the wave
	this.wave = wave;
	this.font = new gamejs.font.Font("15px Verdana");
	
	//HUD background image
	this.hudBackground = gamejs.image.load(imgPath + "hudBackground.png");
	
	//pause image
	this.pause = new Sprite();
	this.pause.image = gamejs.image.load(imgPath + "pause.png");
	this.pause.rect = new gamejs.Rect([5, 5], this.pause.image.getSize());
	
	//build image
	this.disabledBuild = gamejs.image.load(imgPath + "disabledBuild.png");
	this.enabledBuild = gamejs.image.load(imgPath + "build.png");
	this.canBuild = false;
	this.build = new Sprite();
	this.build.image = this.canBuild ? this.enabledBuild : this.disabledBuild;
	this.build.rect = new gamejs.Rect([10, 160], this.build.image.getSize());
	
	//sell image
	this.disabledSell = gamejs.image.load(imgPath + "disabledSell.png");
	this.enabledSell = gamejs.image.load(imgPath + "sell.png");
	this.canSell = false;
	this.sell = new Sprite();
	this.sell.image = this.disabledSell;
	this.sell.rect = new gamejs.Rect([10, 240], this.sell.image.getSize());
	
	//upgrade image
	this.disabledUpgrade = gamejs.image.load(imgPath + "disabledUpgrade.png");
	this.enabledUpgrade = gamejs.image.load(imgPath + "upgrade.png");
	this.canUpgrade = false;
	this.upgrade = new Sprite();
	this.upgrade.image = this.disabledUpgrade;
	this.upgrade.rect = new gamejs.Rect([10, 320], this.upgrade.image.getSize());
	
	//info image
	this.disabledShowInfo = gamejs.image.load(imgPath + "disabledShowInfo.png");
	this.enabledShowInfo = gamejs.image.load(imgPath + "showInfo.png");
	this.canInfo = false;
	this.showInfo = new Sprite();
	this.showInfo.image = this.disabledShowInfo;
	this.showInfo.rect = new gamejs.Rect([10, 400], this.showInfo.image.getSize());
};

/**
 * Updates the scene and its contents, adding the given enemies
 * to the map and managing the various events.
 * 
 * @param {number} msDuration The time past from the last call, in ms.
 */
NightScene.prototype.update = function(msDuration) {
	//update the match and everything else in cascade
	this.match.update(msDuration);
	
	//add enemies according the given wave
	this.timer += msDuration;
	while(this.wave.length > 0 && this.wave[0].time <= this.timer) {
		this.match.getMap().addEnemy(this.wave.shift().enemy);
	}
	
	var events = gamejs.event.get();
	for ( i = 0; i < events.length; i++) {
		var currentEvent = events[i];
		if(this.toPlaceItem != null) {
			//there's an object to place in the map
			var dim = this.toPlaceItem.image.getSize();
			if(currentEvent.type == gamejs.event.MOUSE_MOTION) {
				//move the pictures
				var newPos = currentEvent.pos;
				var topleft = [newPos[X] - dim[WIDTH] / 2, newPos[Y] - dim[HEIGHT] / 2];
				
				//avoid invalid positions
				if(topleft[X] < WIDTH_OFFSET) {
					topleft[X] = WIDTH_OFFSET;
				}
				if(topleft[X] >= (WIDTH_OFFSET + MAP_WIDTH - dim[WIDTH])) {
					topleft[X] = (WIDTH_OFFSET + MAP_WIDTH - dim[WIDTH]) - 1;
				}
				if(topleft[Y] < HEIGHT_OFFSET) {
					topleft[Y] = HEIGHT_OFFSET;
				}
				if(topleft[Y] >= (HEIGHT_OFFSET + MAP_HEIGHT - dim[HEIGHT])) {
					topleft[Y] = (HEIGHT_OFFSET + MAP_HEIGHT - dim[HEIGHT]) - 1;
				}
				
				this.toPlaceItem.topLeft = topleft;
				continue;//go to the next event
			}
			
			if(currentEvent.type == gamejs.event.MOUSE_UP) {
				//add the item to the map
				var point = currentEvent.pos;
				
				//manage boundary click
				if(point[X] < (WIDTH_OFFSET + dim[WIDTH] / 2)) {
					point[X] = (WIDTH_OFFSET + dim[WIDTH] / 2);
				}
				if(point[X] >= (WIDTH_OFFSET + MAP_WIDTH - dim[WIDTH] / 2)) {
					point[X] = (WIDTH_OFFSET + MAP_WIDTH - dim[WIDTH] / 2) - 1;
				}
				if(point[Y] < (HEIGHT_OFFSET + dim[HEIGHT] / 2)) {
					point[Y] = (HEIGHT_OFFSET + dim[HEIGHT] / 2);
				}
				if(point[Y] >= (HEIGHT_OFFSET + MAP_HEIGHT - dim[HEIGHT] / 2)) {
					point[Y] = (HEIGHT_OFFSET + MAP_HEIGHT - dim[HEIGHT] / 2) - 1;
				}
				
				var actualPoint = [point[X] - WIDTH_OFFSET, point[Y] - HEIGHT_OFFSET];
				var itemType = this.toPlaceItem.type;
				var radius = this.toPlaceItem.radius;
				this.toPlaceItem = null;
				
				if(itemType == "radar") {
					//it's a radar
					this.match.getMap().addRadar(new radarModule.Radar(this.match, actualPoint));
					break;
				}

				if(itemType == "turret") {
					//it's a turret
					this.match.getMap().addTurret(new turretModule.Turret(this.match, actualPoint));
					break;
				}
				
				if(itemType == "obstacle") {
					//it's an obstacle
					this.match.getMap().addObstacle(new obstacleModule.Obstacle(this.match, actualPoint, radius));
					break;
				}
				
				throw new Error("Can't trigger this type");
			}
		}

		//process only one valid mouse click(this is the cause of the break statements)
		if (currentEvent.type == gamejs.event.MOUSE_UP) {
			var point = currentEvent.pos;
			if (this.pause.rect.collidePoint(point)) {
				//click on pause
				this.selectedItem = null;
				this.director.pushScene(new pauseSceneModule.PauseScene(this.director, this.match));
				//no break statement because selectedItem is changed,
				//need to update
			}
			
			if (this.canBuild && this.build.rect.collidePoint(point)) {
				//click on build
				this.selectedItem = null;
				this.director.pushScene(new storeSceneModule.StoreScene(this.director, this.match));
				//no break statement because selectedItem is changed,
				//need to update
			}
			
			//there's an object selected
			if(this.selectedItem != null) {
				if (this.canSell && this.sell.rect.collidePoint(point)) {
					//click on sell
					this.selectedItem.sell();
					this.selectedItem = null;//item sold
					//no break statement because selectedItem is changed,
					//need to update
				}
				
				if (this.canUpgrade && this.upgrade.rect.collidePoint(point)) {
					//click on upgrade
					this.selectedItem.upgrade();
					//no break statement because selectedItem is changed,
					//need to update
				}
				
				if (this.canShowInfo && this.showInfo.rect.collidePoint(point)) {
					//click on show info
					this.director.pushScene(new InfoScene(this.director, this.selectedItem));
					break;
				}
			}

			//checks if the click is on an item of the map
			var mapPoint = [point[X] - WIDTH_OFFSET, point[Y] - HEIGHT_OFFSET];
			var selRadars = this.match.getMap().getRadars().collidePoint(mapPoint);
			var selTurrets = this.match.getMap().getTurrets().collidePoint(mapPoint);
			var selObstacles = this.match.getMap().getObstacles().collidePoint(mapPoint);
			var numSelected = selRadars.length + selTurrets.length + selObstacles.length;
			if(numSelected == 1) {
				//there is just one item selected
				if(selRadars[0]) {
					this.selectedItem = selRadars[0];
				}
				
				if(selTurrets[0]) {
					this.selectedItem = selTurrets[0];
				}
				
				if(selObstacles[0]) {
					this.selectedItem = selObstacles[0];
				}
			}
			else {
				//there are zero or more than one items selected
				this.selectedItem = null;//selection not valid
			}
			
			//if an item is selected, checks if it can be upgraded, sold
			//or has some info to show
			if(this.selectedItem != null) {
				if(this.selectedItem.upgrade && this.selectedItem.canUpgrade()) {
					this.canUpgrade = true;
				}
				else {
					this.canUpgrade = false;
				}
				
				if(this.selectedItem.sell && this.selectedItem.getSaleable()) {
					this.canSell = true;
				}
				else {
					this.canSell = false;
				}
				
				if(this.selectedItem.getInfo) {
					this.canShowInfo = true;
				}
				else {
					this.canShowInfo = false;
				}
			}
			else {
				//nothing selected
				this.canUpgrade = false;
				this.canSell = false;
				this.canShowInfo = false;
			}
		}
	}
	
	//sets the correct image(enabled/disabled)
	this.sell.image = this.canSell ? this.enabledSell : this.disabledSell;
	this.upgrade.image = this.canUpgrade ? this.enabledUpgrade : this.disabledUpgrade;
	this.showInfo.image = this.canShowInfo ? this.enabledShowInfo : this.disabledShowInfo;
};

/**
 * Draws the scene and its contents in the director Surface.
 */
NightScene.prototype.draw = function() {
	var surface = this.director.getSurface();
	surface.blit(this.hudBackground);
	this.pause.draw(surface);
	this.build.draw(surface);
	this.sell.draw(surface);
	this.upgrade.draw(surface);
	this.showInfo.draw(surface);
	
	//draws the match(health, credits and score) and everything else in cascade
	this.match.draw(surface);
	
	//writes the past time
	surface.blit(this.font.render((this.timer / 1000).toFixed(1), "#FF0000"), [20, 70]);
	
	if(this.canSell) {
		//writes the credits received if the selected item will sold
		surface.blit(this.font.render(this.selectedItem.getResaleValue(), "#82E0FF"), [35, 290]);
	}
	
	if(this.canUpgrade) {
		//writes the credits needed to upgrade the selected item
		surface.blit(this.font.render(this.selectedItem.priceToUpgrade(), "#82E0FF"), [35, 370]);
	}
};