var DISPLAY_WIDTH = 800;
var DISPLAY_HEIGHT = 480;

var WIDTH_OFFSET = 70;//space for in-game menu
var HEIGHT_OFFSET = 50;//space for info

var MAP_WIDTH = DISPLAY_WIDTH - WIDTH_OFFSET;
var MAP_HEIGHT = DISPLAY_HEIGHT - HEIGHT_OFFSET;

//defining the planet's area, it's a vertical line
var PLANET_HEIGHT = Math.round(MAP_HEIGHT / 2);//this is the height of the planet's area
var PLANET_CENTER = Math.round(MAP_HEIGHT / 2);//the planet's area is centered at half-height


var X = 0;
var Y = 1;
var WIDTH = 0;
var HEIGHT = 1;

var FPS = 60;
var DAY_TIME = 60000;//time to build, in ms
