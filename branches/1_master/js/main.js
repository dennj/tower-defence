var gamejs = require("gamejs");
var scenesDirectorModule = require("js/scenes/scenesDirector");
var startSceneModule = require("js/scenes/startScene");


gamejs.ready(main);

/**
 *The main. 
 */
function main() {
	//our code here
	gamejs.display.setMode([DISPLAY_WIDTH, DISPLAY_HEIGHT]);
	gamejs.display.setCaption("Group 1 - Tower Defense");
	var mainSurface = gamejs.display.getSurface();
	
	var director = new scenesDirectorModule.ScenesDirector(mainSurface);
	director.start(new startSceneModule.StartScene(director));


	
	gamejs.time.fpsCallback(director.processScene, director, FPS);
}


//preload resources
gamejs.preload([
	imgPath + "startBackground.png",
	imgPath + "howToPlay.png",
	imgPath + "credits.png",
	imgPath + "newGame.png",
	imgPath + "hudBackground.png",
	imgPath + "pause.png",
	imgPath + "toNight.png",
	imgPath + "disabledToNight.png",
	imgPath + "build.png",
	imgPath + "disabledBuild.png",
	imgPath + "sell.png",
	imgPath + "disabledSell.png",
	imgPath + "upgrade.png",
	imgPath + "disabledUpgrade.png",
	imgPath + "showInfo.png",
	imgPath + "disabledShowInfo.png",
	imgPath + "loadingNightBackground.png",
	imgPath + "sat.png",
	imgPath + "background.png",
	imgPath + "squaredEnemy.png",
	imgPath + "squaredDetectedEnemy.png",
	imgPath + "squaredDamagedEnemy.png",
	imgPath + "obstacle.png",
	imgPath + "turret.png",
	imgPath + "obstacleStore1.png",
	imgPath + "obstacleStore2.png",
	imgPath + "obstacleStore3.png",
	imgPath + "radarStore1.png",
	imgPath + "radarStore2.png",
	imgPath + "turretStore1.png",
	imgPath + "turretStore2.png",
	imgPath + "turretStore3.png",
	imgPath + "backArrow.png",
	imgPath + "moreInfo.png" ,
	imgPath	+ "money.png"
]);