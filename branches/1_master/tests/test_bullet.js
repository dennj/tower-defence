var gamejs = require('gamejs');
var Enemy = require('js/enemy').Enemy;
var Match = require('js/match').Match;
var Map = require('js/map').Map;
var Bullet = require('js/bullet').Bullet;
var Turret = require('js/turret').Turret;
qModule('js/bullet');

var currentMatch = new Match();
currentMatch.setMap(new Map(currentMatch, 800, 480));

var testEnemy = new Enemy(currentMatch, [10, 10]);
var testTurret = new Turret(currentMatch, [100, 200]);
var testBullet = new Bullet(currentMatch, testTurret, testEnemy);


test("test for Bullet(match, turret, target)[constructor]", function() {
	deepEqual(testBullet.target, testEnemy, "correct target");

        	//tests findPath
	var path = testBullet.path;
	deepEqual(path[0], testTurret.getCenter(), "[findPath] ok for start from the center of the turret");
	var isOk = false;
	for(i = 0;i<path.length;i++) {
                if(((path[i][X] - testEnemy.getCenter()[X])<=1) && ((path[i][Y] - testEnemy.getCenter()[Y])<=1) )  {
                    isOk = true;
                }
        }
        ok(isOk, "[findPath] ok for passing through enemy center");
        
	throws(function() {
		testBullet2 = new Bullet(currentMatch, testTurret, null);
	}, TypeError, "ok for invalid target[raise a TypeError]");

	throws(function() {
		testBullet3 = new Bullet(currentMatch, null, testEnemy);
	}, TypeError, "ok for invalid turret[raise a TypeError]");
	
	throws(function() {
		testBullet4 = new Bullet(null, testTurret, testEnemy);
	}, TypeError, "ok for invalid match[raise a TypeError");
});


test("test for getMatch()", function() {
	strictEqual(testBullet.getMatch(), currentMatch, "ok for the correct object returned");
});

test("test for getTurret()", function() {
	strictEqual(testBullet.getTurret(), testTurret, "ok for the correct object returned");
});

test("test for update(msDuration)", function(){
	testBullet.pathIndex = 20;
	testBullet.update(100);
	var testIndex = 20 + Math.round(testBullet.upgradeList[testBullet.level].speed * (100 / 1000));
	var path = testBullet.path;
	deepEqual(path[Math.round(testBullet.pathIndex)], path[testIndex],"ok the movement for msDuration > 0");
});