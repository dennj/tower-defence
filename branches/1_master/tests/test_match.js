var Match = require("js/match").Match;
var Map = require("js/map").Map;
qModule("js/match");

var testMatch = new Match();

test("creates a match", function(){
	ok(testMatch, Match,"oggetto creato correttamente");
});

test("method match.setMap()", function(){
	var tmpMap=new Map(testMatch,800,480);
	testMatch.setMap(tmpMap);
	strictEqual(testMatch.getMap(),tmpMap,"inserimento mappa normale");
	
	tmpMap=undefined;
	throws( function(){
		testMatch.setMap(tmpMap);
		testMatch.getMap();
	},ReferenceError,"inserimento mappa undefined");
	
	tmpMap=null;
	throws( function(){
		testMatch.setMap(tmpMap);
		testMatch.getMap();
	},ReferenceError,"inserimento mappa null");
	
});


test("method match.getMap()", function(){
	testMatch.setMap(new Map(testMatch,800,480));
	strictEqual(testMatch.map,testMatch.getMap(),"Controllo metodo getMap()");
});

test("method match.decreaseHealth", function(){
	var current_health=testMatch.health;
	
	strictEqual(testMatch.decreaseHealth(0),current_health,"verifica con decrease 0");
	
	strictEqual(testMatch.decreaseHealth(undefined),--current_health,"verifica undefined");
	
	current_healt=testMatch.health;
	strictEqual(testMatch.decreaseHealth(12),current_health-12,"verifica con numero>0");
	
	current_health=testMatch.health;
	throws( function(){
		testMatch.decreaseHealth(-12);
	},RangeError,"verifica con numero<0");
	
	testMatch.health=0;
	strictEqual(testMatch.decreaseHealth(Number.MAX_VALUE+1),0,"test con MaxValue");
	
	testMatch.health=10;
	throws( function(){
		testMatch.decreaseHealth(-Number.MIN_VALUE);
	},RangeError,"verifica con numero con MinValue");
	
	testMatch.health=0;
	strictEqual(testMatch.decreaseHealth(Number.POSITIVE_INFINITY+1),0,"test con Positive Infinity");
	
	testMatch.health=0;
	throws( function(){
		testMatch.decreaseHealth(Number.NEGATIVE_INFINITY);
	},RangeError,"test con Negative Infinity");
	
	testMatch.health=0;
	current_health=testMatch.health;
	throws( function(){
		testMatch.decreaseHealth(Number.NaN);
	},TypeError,"test con NaN");
	
	testMatch.health=10;
	current_health=testMatch.health;
	throws( function(){
		testMatch.decreaseHealth('9');
	},TypeError,"test con stringa di numeri");
	
});

test("method match.changeCredit", function(){
	var currentCredit=testMatch.credit;
	
	strictEqual(testMatch.changeCredit(10),currentCredit+10,"verifica con numero>0");
	
	currentCredit=testMatch.credit;
	strictEqual(testMatch.changeCredit(-10),currentCredit-10,"verifica con numero<0");
	
	currentCredit=testMatch.credit;
	strictEqual(testMatch.changeCredit(0),currentCredit,"verifica con 0");
	
	testMatch.credit=0;
	currentCredit=-testMatch.credit-1;
	throws( function(){
		testMatch.changeCredit(currentCredit);
	},RangeError,"true se non e' possibile andare con credit<0");
	
	testMatch.credit=0;
	ok(testMatch.changeCredit(Number.MAX_VALUE+1)>0,"test con MaxValue");
	
	testMatch.credit=0;
	throws( function(){
		testMatch.changeCredit(Number.MIN_VALUE-1);
	},RangeError,"test con MinValue");
	
	testMatch.credit=0;
	ok(testMatch.changeCredit(Number.POSITIVE_INFINITY+1)>0,"test con Positive Infinity");
	
	testMatch.credit=0;
	throws( function(){
		testMatch.changeCredit(Number.NEGATIVE_INFINITY);
	},RangeError,"test con Negative Infinity");
	
	testMatch.credit=0;
	currentCredit=testMatch.credit;
	throws( function(){
		testMatch.changeCredit(Number.NaN);
	},TypeError,"test con NaN");
	
	testMatch.credit=0;
	currentCredit=testMatch.credit;
	throws( function(){
		testMatch.changeCredit("12");
	},TypeError,"test con stringa di numeri");
	
	testMatch.credit=0;
	currentCredit=testMatch.credit;
	throws( function(){
		testMatch.changeCredit(undefined);
	},TypeError,"test con undefined");
});

test("test for hasCredit(amount)", function(){
	testMatch.changeCredit(500);
	var currentCredit = testMatch.credit;
	
	throws( function(){
		testMatch.hasCredit(-5);
	}, RangeError, "ok for amount < 0 [raise a RangeError]");
	
	ok(testMatch.hasCredit(0), "ok for amount = 0");
	
	ok(testMatch.hasCredit(currentCredit - 1), "ok for amount < actual credit");
	
	ok(testMatch.hasCredit(currentCredit), "ok for amount = actual credit");
	
	ok(! testMatch.hasCredit(currentCredit + 1), "ok for amount > actual credit");
	
	ok(! testMatch.hasCredit(Number.POSITIVE_INFINITY), "ok for amount = POSITIVE_INFINITY");
	
	throws( function(){
		testMatch.hasCredit(Number.NaN);
	}, TypeError, "ok for amount = NaN [raise a TypeError]");
	
	throws( function(){
		testMatch.hasCredit("12");
	}, TypeError, "ok for amount = \"12\" [raise a TypeError]");
	
	throws( function(){
		testMatch.hasCredit(undefined);
	}, TypeError, "ok for amount = undefined [raise a TypeError]");
});

test("method match.changeScore", function(){
	var currentScore=testMatch.score;
	
	strictEqual(currentScore,testMatch.changeScore(0),"verifica con 0");
	
	strictEqual(testMatch.changeScore(10),currentScore+10,"verifica con >0");
	
	currentScore=testMatch.score;
	strictEqual(testMatch.changeScore(-10),currentScore-10,"verifica con<0");
	
	currentScore=testMatch.score;
	throws( function(){
		testMatch.changeScore(-(currentScore+1));
	},RangeError,"true se non e' possibile andare con score<0");
	
	currentScore=testMatch.score;
	ok(testMatch.changeScore(Number.MAX_VALUE+1)>0,"test con MaxValue");
	
	testMatch.score=0;
	currentScore=testMatch.score;
	throws( function(){
		testMatch.changeScore(Number.MIN_VALUE-1);
	},RangeError,"test con MinValue");
	
	testMatch.score=0;
	currentScore=testMatch.score;
	ok(testMatch.changeScore(Number.POSITIVE_INFINITY+1)>0,"test con PositiveInfinity");
	
	testMatch.score=0;
	currentScore=testMatch.score;
	throws( function(){
		testMatch.changeScore(Number.NEGATIVE_INFINITY);
	},RangeError,"test con NegativeInfinity");
	
	testMatch.score=0;
	currentScore=testMatch.score;
	throws( function(){
		testMatch.changeScore(Number.NaN);
	},TypeError,"test con NaN");
	
	testMatch.score=0;
	currentScore=testMatch.score;
	throws( function(){
		testMatch.changeScore("12");
	},TypeError,"test con stringa di numeri");
	
	testMatch.score=0;
	currentScore=testMatch.score;
	throws( function(){
		testMatch.changeScore(undefined);
	},TypeError,"test con undefined");
});