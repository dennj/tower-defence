var Obstacle = require('js/obstacle').Obstacle;
var Match = require('js/match').Match;
var Map = require('js/map').Map;
qModule('js/obstacle');

var currentMatch = new Match();
currentMatch.setMap(new Map(currentMatch, 800, 480));
var testObstacle = new Obstacle(currentMatch, [30, 50], 20);

test("test for Obstacle(match, center, radius)[constructor]", function() {
	deepEqual(testObstacle.center, [30, 50], "ok for valid coordinates [30, 50]");
	
	throws(function() {
		testObstacle = new Obstacle(currentMatch, [-5, 50], 20);
	}, RangeError, "ok for invalid coordinates [-5, 50][raise a RangeError]");
	
	throws(function() {
		testObstacle = new Obstacle(currentMatch, [30, 50], -2);
	}, RangeError, "ok for invalid radius [100, 480][raise a RangeError]");
	
	testObstacle = new Obstacle(currentMatch, [0, 50], 20);
	deepEqual(testObstacle.center, [0, 50], "ok for marginal coordinates [0, 50] (image out of the map boundaries)");
});

test("test for getMatch()", function() {
	strictEqual(testObstacle.getMatch(), currentMatch, "ok for the correct object returned");
});

testObstacle = new Obstacle(currentMatch, [100, 48], 30);
test("test for getCenter()", function() {
	deepEqual(testObstacle.getCenter(), [100, 48], "ok");
});

test("test for getRadius()", function() {
	strictEqual(testObstacle.getRadius(), 30, "ok");
});

test("test for setSaleable()", function() {
	testObstacle = new Obstacle(currentMatch, [30, 50], 20);
	testObstacle.setSaleable(false);
	ok(!(testObstacle.saleable), "ok for setSaleable in normal situation");
	
	testObstacle.setSaleable("true");
	//all non empty string evaluates to true
	ok(testObstacle.saleable, "ok for string input");
});

test("test for sell()", function(){
	testObstacle = new Obstacle(currentMatch, [30, 50], 20);
	currentMatch.changeCredit(10000);
	currentMatch.getMap().addObstacle(testObstacle);
	
	throws(function() {
		testObstacle.sell();
	}, /InvalidOperationError/, "ok for removing not saleable obstacle"+
	"[raise a InvalidOperationError]");
	
	testObstacle.setSaleable(true);
	testObstacle.sell();
	var obstacleGroup = currentMatch.getMap().getObstacles();
	ok( ! obstacleGroup.has(testObstacle), "ok for removing correctly with sell");
});