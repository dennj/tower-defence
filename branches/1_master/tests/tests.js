var gamejs = require('gamejs');

gamejs.ready(function(){
	require('tests/test_enemy');
	require('tests/test_map');
	require('tests/test_obstacle');
	require('tests/test_radar');
	require('tests/test_match');
	require('tests/test_turret');
	require('tests/test_bullet');
});

//preload resources
var imgPath="../resources/images/"
gamejs.preload([
	imgPath + "sat.png",
	imgPath + "background.png",
	imgPath + "enemy.png",
	imgPath + "squaredEnemy.png",
	imgPath + "squaredDetectedEnemy.png",
	imgPath + "squaredDamagedEnemy.png",
	imgPath + "obstacle.png",
	imgPath + "turret.png"
]);
