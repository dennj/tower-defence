var gamejs = require("gamejs");
var Turret = require("js/turret").Turret;
var Match = require("js/match").Match;
var Map = require("js/map").Map;
qModule("js/turret");

var currentMatch = new Match();
currentMatch.setMap(new Map(currentMatch, 800, 480));
var testTurret = new Turret(currentMatch, [100, 200]);

test("test for Turret(match, coords)[constructor]", function() {
	deepEqual(testTurret.center, [100, 200], "ok for valid coordinates [100, 200]");

	throws(function() {
		testTurret = new Turret(currentMatch, [-5, 100]);
	}, RangeError, "ok for invalid coordinates [-5, 100][raise a RangeError]");
	
	throws(function() {
		testTurret = new Turret(currentMatch, [50, 0]);
	}, RangeError, "ok for invalid coordinates [50, 0] (image out of the map boundaries)[raise a RangeError]");
});

test("test for getBoundingRect()", function() {
	var center = [60, 60];
	testTurret = new Turret(currentMatch, center);
	var size = testTurret.image.getSize();
	var topLeft = [center[X] - size[WIDTH] / 2, center[Y] - size[HEIGHT] / 2];
	var expectedRect = new gamejs.Rect(topLeft, size);
	deepEqual(testTurret.getBoundingRect(), expectedRect, "ok for the correct rect returned");
});

test("test for getActionRadius()", function() {
	testTurret = new Turret(currentMatch, [200, 300]);
	strictEqual(testTurret.getActionRadius(), testTurret.upgradeList[0].radius, "ok");

	testTurret.level = 1;
	strictEqual(testTurret.getActionRadius(), testTurret.upgradeList[1].radius, "ok, after an upgrade");
});

test("test for getMatch()", function() {
	strictEqual(testTurret.getMatch(), currentMatch, "ok for the correct object returned");
});

test("test for getResaleValue()", function() {
        var testTurret = new Turret(currentMatch, [100, 200]);
        strictEqual(testTurret.getResaleValue(), 70, "resale value of the first level of upgrade");
        if(currentMatch.credit >= testTurret.priceToUpgrade()) {
                    testTurret.upgrade();
                    strictEqual(testTurret.getResaleValue(), 100, "resale value of the second level of upgrade");
        }
        else {
                    throws(function() {
		    testTurret.upgrade();
	            }, Error, "ok for not enough credit to upgrade");
        }

});

test("test for canUpgrade()", function() {
        var testTurret = new Turret(currentMatch, [100, 200]);
        for(i = 0;i< testTurret.upgradeList.length;i++) {                     // added a for cycle for a future implementation
          if(i == testTurret.upgradeList.length - 1) {                        // of more level in upgradeList
              ok(!testTurret.canUpgrade(), "the turret can't be upgraded");
          }
          else {
              ok(testTurret.canUpgrade(), "the turret can be upgraded");
              if(currentMatch.credit >= testTurret.priceToUpgrade()) {
                    testTurret.upgrade();
              }
              else {
                    throws(function() {
		    testTurret.upgrade();
	            }, Error, "ok for not enough credit to upgrade");
	            break;
              }

          }
        }
});

test("test for upgrade()", function() {
	testTurret = new Turret(currentMatch, [200, 300]);
	var cost = testTurret.upgradeList[1].price;
	currentMatch.credit = cost + 100;
	testTurret.upgrade();
	strictEqual(currentMatch.credit, 100, "ok for removing the credit");
	
	strictEqual(testTurret.level, 1, "ok for incrementing the level");
	
	testTurret.level = 0;
	currentMatch.credit = cost - 50;
	throws(function() {
		testTurret.upgrade();
	}, /InvalidOperationError/, "ok for upgrade with no credits[raise a InvalidOperationError]");
});

test("test for getLevel()", function(){
	var level = testTurret.level;
	testTurret = new Turret(currentMatch, [200, 300]);
	strictEqual(testTurret.getLevel(), level, "ok");
	
	currentMatch.credit = testTurret.upgradeList[1].price;
	testTurret.upgrade();
	level = testTurret.level;
	strictEqual(testTurret.getLevel(), level, "ok after an upgrade");
});

test("test for getCenter()", function(){
	center = [200, 300];
	testTurret = new Turret(currentMatch, center);
	deepEqual(testTurret.getCenter(), center, "ok for the correct coordinates returned");
});

test("test for priceToBuild()", function() {
	testTurret = new Turret(currentMatch, [200, 300]);
	strictEqual(testTurret.priceToBuild(), testTurret.upgradeList[0].price, "ok");
});

test("test for priceToUpgrade()", function() {
	testTurret = new Turret(currentMatch, [200, 300]);
	strictEqual(testTurret.priceToUpgrade(), testTurret.upgradeList[testTurret.getLevel() + 1].price, "ok from level = 0");
	
	if(testTurret.upgradeList.length > 2) {
		currentMatch.credit = testTurret.priceToUpgrade();
		testTurret.upgrade();
		strictEqual(testTurret.priceToUpgrade(), testTurret.upgradeList[testTurret.getLevel() + 1].price, "ok from level = 1");
	}
});

test("test for getObstructiveRadius()", function() {
	testTurret = new Turret(currentMatch, [200, 300]);
	var rect = testTurret.getBoundingRect();
	var expected = Math.round((rect.width > rect.height ? rect.width : rect.height) / 2);
	strictEqual(testTurret.getObstructiveRadius(), expected, "ok for removing the credit");
});

test("test for setSaleable()", function() {
	testTurret = new Turret(currentMatch, [200,300]);
	testTurret.setSaleable(false);
	ok(!(testTurret.saleable), "ok for setSaleable in normal situation");
	
	testTurret.setSaleable("true");
	//all non empty string evaluates to true
	ok(testTurret.saleable, "ok for string input");
});

test("test for sell()", function(){
	testTurret = new Turret(currentMatch, [200,300]);
	currentMatch.changeCredit(10000);
	currentMatch.getMap().addTurret(testTurret);
	testTurret.sell();
	var turretGroup = currentMatch.getMap().getTurrets();
	ok( ! turretGroup.has(testTurret), "ok for removing correctly with sell");
});