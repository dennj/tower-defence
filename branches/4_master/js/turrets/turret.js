var gamejs = require('gamejs');

/**
 * Create a Turret object
 *
 * @param {Match} match The match where this object will work
 * @param {[number, number]} coords The radar's position, in format [x, y]
 */
var Turret = exports.Turret = function(match, coords) {
        Turret.superConstructor.apply(this, arguments);
        this.currentMatch = match;
        this.center = coords;
        this.radius;
        this.target = null;
        this.upgradeList;
        this.level = 0;    
        this.font = new gamejs.font.Font('10px ubuntu, sans-serif');
}

/**
 * Turret extend Sprite.
 */
gamejs.utils.objects.extend(Turret, gamejs.sprite.Sprite);

/**
 * Return the length of the detector ray.
 *
 * @return {number} The length of the ray, in pixel.
 */
Turret.prototype.getRadius = function() {
        return (this.upgradeList[this.level]).radius;
}

/**
 * Return the match where this turret work.
 *
 * @param {Match} The match where this turret work.
 */
Turret.prototype.getMatch = function() {
        return this.currentMatch;
}


/**
 * Triggers a turret's upgrade.
 */
Turret.prototype.upgrade = function() {
	if(this.level <this.upgradeList.length-1){		
		this.level++;
		this.currentMatch.changeCredit(-this.upgradeList[this.level].price);
		this.menuSurface = this.font.render("Price: " +this.upgradeList[this.level].price, "#fff");
	}
}


