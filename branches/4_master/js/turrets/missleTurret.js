var gamejs = require('gamejs');
var mainTurret = require('js/turrets/turret');
var bulletjs = require('js/turrets/bullet');
/**
 * Create a Turret object
 *
 * @param {Match} match The match where this object will work
 * @param {[number, number]} coords The radar's position, in format [x, y]
 */
var missleTurret = exports.missleTurret = function(match, coords) {
	missleTurret.superConstructor.apply(this, arguments);
        this.currentMatch = match;
        this.center = coords;
        this.target = null;
        this.level = 0;
        this.bullets = new gamejs.sprite.Group();
        this.upgradeList = [
                {price: 100, radius: 800, resaleValue: 70, damage: 1,reloadingTime: 1000 }, // livello 0
                {price: 100, radius: 400, resaleValue: 70, damage: 2 ,reloadingTime: 500 }, // livello 1
                {price: 100, radius: 500, resaleValue: 70, damage: 10,reloadingTime: 500  }, // livello 2
                {price: 100, radius: 600, resaleValue: 70, damage: 20 ,reloadingTime: 500 }, // livello 3
                {price: 100, radius: 700, resaleValue: 100 , damage: 30 ,reloadingTime: 100 } // livello 4
        ];
        this.timeFromLastShoot = this.upgradeList[0].reloadingTime;
        this.image = gamejs.image.load("resources/images/missleturret.png");
        this.radius = this.image.getSize()[0]/2;
        
        var dims = this.image.getSize();
        this.menuSurface = this.font.render("Price: " +this.upgradeList[this.level].price, "#fff");
}

gamejs.utils.objects.extend(missleTurret, mainTurret.Turret);

/**
 * Updates the position of the detector ray of the turret.
 *
 * @param {number} msDuration The time past from the last call, in ms.
 */
missleTurret.prototype.update = function(msDuration) {
		this.bullets.update(msDuration);
		this.timeFromLastShoot += msDuration;
		//checks if the turret is ready to shoot again
		if(this.timeFromLastShoot > this.upgradeList[this.level].reloadingTime) {
	        var enemy_seen;
	        var cur_center =  this.center;
	        var cur_radius = this.upgradeList[this.level].radius;
	        this.currentMatch.getMap().getEnemies().sprites().forEach(function(enemy) {
	                if (enemy.isDetected()){
	                	if(Math.sqrt(
								(enemy.getCoordinates()[0] - cur_center[0])*(enemy.getCoordinates()[0] - cur_center[0]) +
								(enemy.getCoordinates()[1] - cur_center[1])*(enemy.getCoordinates()[1] - cur_center[1])
						) < cur_radius){
	                		if(enemy_seen){
	                			if(
	                					Math.sqrt(
	                							(enemy.getCoordinates()[0] - cur_center[0])*(enemy.getCoordinates()[0] - cur_center[0]) +
	                							(enemy.getCoordinates()[1] - cur_center[1])*(enemy.getCoordinates()[1] - cur_center[1])
	                					) < 
	                					Math.sqrt(
	                							(enemy_seen.getCoordinates()[0] - cur_center[0])*(enemy_seen.getCoordinates()[0] - cur_center[0]) +
	                							(enemy_seen.getCoordinates()[1] - cur_center[1])*(enemy_seen.getCoordinates()[1] - cur_center[1])		
	                					)
	                			)
	                				enemy_seen = enemy;
	                		}else{
	                			enemy_seen = enemy;
	                        }
	                	}
	                }
	        });
	        if(enemy_seen){
		        this.bullets.add(new bulletjs.Bullet(this.level,this,enemy_seen,this.currentMatch));
		        //this.getMatch().getSoundManager().playMissle(0);
		        this.timeFromLastShoot = 0;
	        }else{
	        	this.target = null;
	        }
	        
		}
       
}
/**
 * Draws the turret in the given Surface.
 *
 * @param {gamejs.Surface} surface The Surface where draw on.
 */
missleTurret.prototype.draw = function(surface) {
        //draw turret tower
        var x_center = this.center[0];
        var y_center = this.center[1];
        var x_turret = x_center - this.image.getSize()[0] / 2;
        var y_turret = y_center - this.image.getSize()[1] / 2;

        surface.blit(this.image, [x_turret, y_turret]);
        this.bullets.draw(surface);
}