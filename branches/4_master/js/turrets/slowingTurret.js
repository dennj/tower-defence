var gamejs = require('gamejs');
var mainTurret = require('js/turrets/turret');

/**
 * Create a Turret object
 *
 * @param {Match} match The match where this object will work
 * @param {[number, number]} coords The radar's position, in format [x, y]
 */
var slowingTurret = exports.slowingTurret = function(match, coords) {
	slowingTurret.superConstructor.apply(this, arguments);
        this.currentMatch = match;
        this.center = coords;
        this.level = 0;
        this.upgradeList = [
                {price: 100, radius: 50, resaleValue: 70, damage: 0.3 , reloadingTime : 1000 }, // livello 0
                {price: 100, radius: 100, resaleValue: 70, damage: 0.5 , reloadingTime : 1000}, // livello 1
                {price: 100, radius: 200, resaleValue: 70, damage: 0.4 , reloadingTime : 1000}, // livello 2
                {price: 100, radius: 300, resaleValue: 70, damage: 0.3 , reloadingTime : 1000}, // livello 3
                {price: 100, radius: 400, resaleValue: 100 , damage: 0.2 , reloadingTime : 1000} // livello 4
        ];
        this.image = gamejs.image.load("resources/images/slowingturret.png");
        this.timeFromLastShoot = this.upgradeList[0].reloadingTime;
        this.radius = this.image.getSize()[0]/2;
        var dims = this.image.getSize();
        this.menuSurface = this.font.render("Price: " +this.upgradeList[this.level].price, "#fff");

}

gamejs.utils.objects.extend(slowingTurret, mainTurret.Turret);

/**
 * Updates the position of the detector ray of the turret.
 *
 * @param {number} msDuration The time past from the last call, in ms.
 */
slowingTurret.prototype.update = function(msDuration) {
		this.timeFromLastShoot += msDuration;
		if(this.timeFromLastShoot > this.upgradeList[this.level].reloadingTime) {
			var enemy_seen;
	        var cur_center =  this.center;
	        var cur_radius = this.upgradeList[this.level].radius;
	        var cur_slow =this.upgradeList[this.level].damage
	        this.currentMatch.getMap().getEnemies().sprites().forEach(function(enemy) {
	        	if(Math.sqrt(
						(enemy.getCoordinates()[0] - cur_center[0])*(enemy.getCoordinates()[0] - cur_center[0]) +
						(enemy.getCoordinates()[1] - cur_center[1])*(enemy.getCoordinates()[1] - cur_center[1])
				) < cur_radius){
	        		enemy.slow(cur_slow,2);
	        	}
	        });    
	        this.timeFromLastShoot = 0;
		}   
}
/**
 * Draws the turret in the given Surface.
 *
 * @param {gamejs.Surface} surface The Surface where draw on.
 */
slowingTurret.prototype.draw = function(surface) {
        //draw turret tower
        var x_center = this.center[0];
        var y_center = this.center[1];
        var x_turret = x_center - this.image.getSize()[0] / 2;
        var y_turret = y_center - this.image.getSize()[1] / 2;

        surface.blit(this.image, [x_turret, y_turret]);
        gamejs.draw.circle(surface, '#FFFFFF', this.center, this.upgradeList[this.level].radius*((this.timeFromLastShoot+1)/ this.upgradeList[this.level].reloadingTime), 2);

}