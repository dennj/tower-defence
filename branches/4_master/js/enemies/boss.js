var gamejs = require('gamejs');
var mainEnemy = require('js/enemies/enemy');

/**
 * Creates a super enemy!!
 *
 * @extends Sprite
 * @constructor
 */
var Boss = exports.Boss = function(match, startRect) {
	Boss.superConstructor.apply(this, arguments);
	this.path = [];
	this.pathIndex = 0;
	this.maxSpeed =  40;
	this.speed =  this.maxSpeed;
	this.health = 2000;
	this.fullHealth = 2000;
	this.armor =  2000;
	this.fullArmor = 2000;
	this.alpha = 0.6;
	this.damage = 60000;
	this.visibilityTime = 5000;//1000ms
	this.detected = 0;
	this.prize = 1;
	this.currentMatch = match;
	this.scoreForKill = 1000;
	this.originalImage = gamejs.image.load("resources/images/enemy.png");
	this.detectedImage = gamejs.image.load("resources/images/enemy_detected.png");
	
	this.originalImage = gamejs.transform.scale(
			this.originalImage,
			[this.originalImage.getSize()[0] * (2), this.originalImage.getSize()[1] *  (2)]
	);
	this.detectedImage = gamejs.transform.scale(
			this.detectedImage,
			[this.detectedImage.getSize()[0] * (2), this.detectedImage.getSize()[1] *  (2)]
	);
	this.rotation = 0;
	this.image = this.originalImage;
	this.rect = new gamejs.Rect(startRect,this.image.getSize());
	this.path = this.currentMatch.getMap().getPath(startRect,[0,0]);
}

/**
 * Extending Sprite with Enemy
 */
gamejs.utils.objects.extend(SuperEnemy, mainEnemy.Enemy);
