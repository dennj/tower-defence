var gamejs = require('gamejs');
var mainEnemy = require('js/enemies/enemy');
/*
 * NEMICI: un oggetto Animation [cicler� tra normale, colpito, esploso] caratterizzato da:
 * - Percorso, un array di punti che individuano, passo passo il percorso da seguire
 * - Velocit�, un intero che dice il numero di pixel passati in un frame
 * - Vita, un intero che rappresenta la vita rimasta
 * - Alpha, un intero [0,100] che dice se e quanto il nemico Ã¨ visibile a occhio nudo di notte
 * - Danno, un intero che dice il numero di vite tolte al pianeta, se questo nemico ci arriva
 * - visibilityTime , tempo che il nemico rimane visibile dopo il ping
 * - detected , un intero che indica se il nemico e` stato rilevato dal radar e quindi e` visibile alle armi
 * - Valore, un intero che ci dice quanto prendiamo uccidendo questo nemico.
 */

/**
 * Creates an enemy
 *
 * @extends Sprite
 * @constructor
 */
var littleEnemy = exports.littleEnemy = function(match, startRect) {
	littleEnemy.superConstructor.apply(this, arguments);
	this.maxSpeed = 50 + (40 * Math.random());
	this.speed = this.maxSpeed;
	this.health = 200;
	this.fullHealth = 200;
	this.armor = 20;
	this.fullArmor = 20;
	this.alpha = 0.6;
	this.damage = 1000;
	this.visibilityTime = 2000;//1000ms
	this.detected = 0;
	this.prize = 1;
	this.scoreForKill = 10;
	this.currentMatch = match;
	this.originalImage = gamejs.image.load("resources/images/enemy.png");
	this.detectedImage = gamejs.image.load("resources/images/enemy_detected.png");
	
	this.originalImage = gamejs.transform.scale(
			this.originalImage,
			[this.originalImage.getSize()[0] * (0.5), this.originalImage.getSize()[1] *  (0.5)]
	);
	this.detectedImage = gamejs.transform.scale(
			this.detectedImage,
			[this.detectedImage.getSize()[0] * (0.5), this.detectedImage.getSize()[1] *  (0.5)]
	);
	this.rotation = 0;
	this.image = this.originalImage;
	this.rect = new gamejs.Rect(startRect,this.image.getSize());
}


gamejs.utils.objects.extend(littleEnemy, mainEnemy.Enemy);
