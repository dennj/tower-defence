var gamejs = require('gamejs');

/**
 * Sound manager class.
 * There are 2 kind of sound: laser sounds and explosion sounds
 * each type of sound corresponds to an array: lasersSounds[] and explosionSounds[]
 * 
 * There are 2 type of lasers and 2 type of explosions
 * 
 * There are 2 soundtrack: a day theme and a night theme
 */



var SoundManager = exports.SoundManager = function() {
	this.laserSounds = [];
	var numberOfLasers=2;
	for (var i=0;i<numberOfLasers;i++) {
		this.laserSounds.push(new gamejs.mixer.Sound("resources/sounds/laser" + i + ".ogg"));
	} 
	this.missileSounds = [];
	var numberOfMissiles=3;
	for (var i=0;i<numberOfMissiles;i++) {
		this.laserSounds.push(new gamejs.mixer.Sound("resources/sounds/missile" + i + ".ogg"));
	}   
   this.explosionSounds = [];
   var numberOfExplosions=2;
   for (var i=0;i<numberOfExplosions;i++) {
      this.explosionSounds.push(new gamejs.mixer.Sound("resources/sounds/explosion" + i + ".ogg"));
   }
   this.dayTheme = new gamejs.mixer.Sound(SoundManager.DAY_THEME);
   this.nightTheme = new gamejs.mixer.Sound(SoundManager.NIGHT_THEME)
   return this; 
};

/**
 * Nigth theme
 */
SoundManager.NIGHT_THEME = "resources/sounds/NightTheme.ogg";

/**
 * Day theme
 */
SoundManager.DAY_THEME = "resources/sounds/DayTheme.ogg";

/**
 * Play the laser sound corresponding with id
 *
 * @param {int} the id of the laser to play, must be in range
 */
SoundManager.prototype.playLaser = function(id){
	if(id<0 || id >= this.laserSounds.length)
		throw new ReferenceError("Laser not in array");
	this.laserSounds[id].play();
}

/**
 * Play the missile sound corresponding with id
 *
 * @param {int} the id of the laser to play, must be in range
 */
SoundManager.prototype.playMissle = function(id){
	if(id<0 || id >= this.missileSounds.length)
		throw new ReferenceError("Missile not in array");
	this.missileSounds[id].play();
}

/**
 * Stop playing the missile sound corresponding with id
 *
 * @param {int} the id of the missile to stop playing, must be in range
 */
SoundManager.prototype.stopMissle = function(id){
	if(id<0 || id >= this.missileSounds.length)
		throw new ReferenceError("Missile not in array");
	this.missileSounds[id].stop();
}

/**
 * Play the explosion sound corresponding with id
 *
 * @param {int} the id of the explosion to play, must be in range
 */
SoundManager.prototype.playExplosion = function(id){
	if(id<0 || id >= this.explosionSounds.length)
		throw new ReferenceError("Explosion not in array");
	this.explosionSounds[id].play();
}

/**
 * Play the day theme
 *
 */
SoundManager.prototype.playDay = function(){
	//this.dayTheme.stop();
	//this.nightTheme.stop();
	this.dayTheme.play(1);
}

/**
 * Play the night theme
 *
 */
SoundManager.prototype.playNight = function(){
	//this.dayTheme.stop();
	//this.nightTheme.stop();
	this.nightTheme.play(1);
}

/**
 * Get an array containing all the audio preloads
 *
 */
SoundManager.getResourceList = function() {
   var SOUNDS = [];
   for (var i=0;i<2;i++) {
      SOUNDS.push("resources/sounds/laser" + i + ".ogg");
   }
   for (var i=0;i<3;i++) {
	      SOUNDS.push("resources/sounds/missile" + i + ".ogg");
	   }
   for (var i=0;i<2;i++) {
      SOUNDS.push("resources/sounds/explosion" + i + ".ogg");
   }
   SOUNDS.push(SoundManager.DAY_THEME);
   SOUNDS.push(SoundManager.NIGHT_THEME);
   return SOUNDS;
}
