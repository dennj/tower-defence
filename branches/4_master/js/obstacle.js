var gamejs = require('gamejs');

/*
 * OSTACOLI
 *
 * Un oggetto Sprite caratterizzato da:
 * - Centro, Dimensione [piccolo, medio, grande]
 * - Costo(quanto costa un ostacolo)
 *
 * Lo sprite rappresenter� una griglia deformata.
 */

/**
 * Creates a new obstacle
 *
 * @param {array} center The coordinates as vector [x, y]
 * @param {number} radius The radius of the obstacle
 *
 * @extends Sprite
 * @constructor
 */
var Obstacle = exports.Obstacle = function(match, cen, rad) {
	Obstacle.superConstructor.apply(this, arguments);
	this.currentMatch = match;
	this.center = cen;
	this.radius = rad;
	this.originalImage = gamejs.image.load('resources/images/obstacle.png');
	var dims = this.originalImage.getSize();
	this.image =  gamejs.transform.scale(
			this.originalImage,
			[this.radius*2, this.radius*2]
	);
	var dims = this.image.getSize();
	var topLeftCorner = [this.center[0]-(dims[0]/2), this.center[1]-(dims[1]/2)];
	this.rect = new gamejs.Rect(topLeftCorner,dims);
	
	
}

/**
 * Obstacle extend Sprite.
 */
gamejs.utils.objects.extend(Obstacle, gamejs.sprite.Sprite);

/**
 * Returns the vector of the center of the Obstacle
 *
 * @returns {array} The coordinates as vector [x, y]
 */
Obstacle.prototype.getCenter = function() {
	return this.center;
}

/**
 * Returns the radius of the Obstacle
 *
 * @returns {number} The radius of the Obstacle
 */
Obstacle.prototype.getRadius = function() {
	return this.radius;
}

/**
 * Return the match where this radar work.
 *
 * @param {Match} The match where this radar work.
 */
Obstacle.prototype.getMatch = function() {
	return this.currentMatch;
}
