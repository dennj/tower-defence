var gamejs = require('gamejs');

/**
 * RADAR: an object representing a radar. The detection area is a circular sector.
 * 
 * Properties
 * - currentMatch : the match where the radar work.
 * - center : coordinates of the radar's position;
 * - centralAngle : angle covered by the radar (central angle of the circular sector);
 * - startAngle :  angle where the detection starts;
 * - currentAngle :  current angle into the circular sector;
 * - radius : max distance of detection (the radius of the circular sector);
 * - currentRadius : actual distance of detection, useful in several drawing problem;
 * - angularVelocity : number of radians that the detector ray covers in a second;
 * - level : the upgrade level of the radar;
 * - upgradeList : a list containing all the upgrade info for each upgrade level;
 * - resaleValue : the radar's remaining price;
 * - image : a Surface object with the radar's picture;
 */
 
 /**
 * Create a Radar object.f
 * 
 * @param {Match} match The match where this object will work.
 * @param {[number, number]} coords The radar's position, in format [x, y].
 */
var Radar = exports.Radar = function(match, coords) {
	Radar.superConstructor.apply(this, arguments);
	this.currentMatch = match;
	this.level = 0;
	this.upgradeList = [
	                    { price : 100, radius : 50, angularVelocity : Math.PI /3, resaleValue : 50 }, //level 0
	                    { price : 200, radius : 100, angularVelocity : Math.PI /2, resaleValue : 100 }, //level 1
	                    { price : 300, radius : 150, angularVelocity : Math.PI , resaleValue : 150 }, //level 0
	                    { price : 400, radius : 200, angularVelocity : Math.PI*2, resaleValue : 200 } //level 1
	];
	this.center = coords;
	this.centralAngle = 2 * Math.PI;
	this.startAngle = 0;
	this.currentAngle = 0;
	this.currentRadius;
	this.originalImage = gamejs.image.load('resources/images/sat.png');
	var dims = this.originalImage.getSize();
	this.image = gamejs.transform.scale(
			this.originalImage,
			[dims[0] * (1), dims[1] *  (1)]
	);
	this.radius = this.image.getSize()[0]/2;
	this.font = new gamejs.font.Font('10px ubuntu, sans-serif');
	this.menuSurface = this.font.render("Price: " +this.upgradeList[this.level].price, "#fff");
}

/**
 * Radar extend Sprite.
 */
gamejs.utils.objects.extend(Radar, gamejs.sprite.Sprite);

/**
 * Return the match where this radar work.
 *
 * @param {Match} The match where this radar work.
 */
Radar.prototype.getMatch = function() {
	return this.currentMatch;
}

/**
 * Sets the angle where the detection starts.
 *
 * @param {number} angle The angle where the detection starts
 * @throws {TypeError} Throws this exception if angle's type isn't 'number'.
 */
Radar.prototype.setStartAngle = function(angle) {
	this.startAngle = angle % (2 * Math.PI);
	this.currentAngle = 0;
}

/**
 * Sets the angle covered by the radar.
 *
 * @param {number} angle The angle covered by the radar.
 * @throws {TypeError} Throws this exception if angle's type isn't 'number'.
 * @throws {RangeError} Throws this exception if angle's value isn't valid.
 */
Radar.prototype.setCentralAngle = function(angle) {
	this.centralAngle = angle;
}

/**
 * Triggers a radar's upgrade.
 */
Radar.prototype.upgrade = function() {
	if(this.level <this.upgradeList.length-1){
		this.level++;
		this.currentMatch.changeCredit(-this.upgradeList[this.level].price);
		this.menuSurface = this.font.render("Price: " +this.upgradeList[this.level].price, "#fff");
	}
}

/**
 * Updates the position of the detector ray of the radar.
 * 
 * @param {number} msDuration The time past from the last call, in ms.
 */
Radar.prototype.update = function(msDuration) {
	var madeAngle = this.upgradeList[this.level].angularVelocity * (msDuration / 1000);//
	this.currentAngle = this.startAngle + ((this.currentAngle + madeAngle) % this.centralAngle);
	var totalAngle = this.startAngle + this.currentAngle;
	var startPoint = this.center;
	var endPoint = this.center;
	var collision_detected = 0;
	for(var i=0; i< this.upgradeList[this.level].radius; i++){
		endPoint = [startPoint[0] + Math.sin(totalAngle) * i,startPoint[1] + Math.cos(totalAngle) * i];			
		if ( this.getMatch().getMap().matrix[Math.round(endPoint[0])][Math.round(endPoint[1])] == 0 ){
			i -= 1;
			break;
		}
	}
	this.currentRadius = i;
	this.currentMatch.getMap().getEnemies().sprites().forEach(function (collision_list) {
		if (collision_list.rect.collideLine(startPoint,endPoint)){
			collision_list.setDetected(1);
			//collision_list.hit(100);
		}
	});
}

/**
 * Draws the radar in the given Surface.
 * 
 * @param {gamejs.Surface} surface The Surface where draw on.
 */
Radar.prototype.draw = function(surface) {
	//draw the image
	//calculates the correct position
	var dims = this.image.getSize();
	var startX = this.center[0] - (dims[0] / 2);
	var startY = this.center[1] - (dims[1] / 2);
	
	
	//draw the detector ray
	var startPoint = [];
	var totalAngle = this.startAngle + this.currentAngle;
	startPoint[0] = this.center[0];
	startPoint[1] = this.center[1];
	var endPointX = startPoint[0] + Math.sin(totalAngle) * this.currentRadius;
	var endPointY = startPoint[1] + Math.cos(totalAngle) * this.currentRadius;
	var endPoint = [endPointX, endPointY];
	gamejs.draw.line(surface, '#33FF00', startPoint, endPoint, 1);
	surface.blit(this.image, [startX, startY]);
}

/**
 * Triggers a radar's upgrade.
 */
Radar.prototype.isBuyable = function() {
	return this.upgradeList[this.level].price < this.currentMatch.getCredit();
}
/**
 * Click behaviour
 */
Radar.prototype.click = function(coords) {
	if (this.currentMatch.menu == this) { // if menu is opened
		if (this.currentMatch.positionClick(coords)==1) { //controlla se � stato premuto upgrade
			if (this.isBuyable()) { //se � acquistabile
				this.upgrade();
			}
			else {} //scrivi che non puoi 
		}
		else {
			this.currentMatch.menu =null;
		}
			
	}
	else{   //if menu isn't opened
		if (Math.sqrt((coords[0] - this.center[0])
				* (coords[0] - this.center[0]) + (coords[1] - this.center[1])
				* (coords[1] - this.center[1])) < this.radius) {
			this.currentMatch.menu = this;
		}
	}
}