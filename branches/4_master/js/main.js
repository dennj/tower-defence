var gamejs = require('gamejs');
var mapjs = require('js/map');
var matchjs = require('js/match');
var lenemyjs = require('js/enemies/littleEnemy');
var genemyjs = require('js/enemies/giantEnemy');
var radarjs = require('js/radar');
var obstaclejs = require('js/obstacle');
var lturretjs = require("js/turrets/laserTurret");
var sturretjs = require("js/turrets/slowingTurret");
var mturretjs = require("js/turrets/missleTurret");
var SoundManager = require('js/soundmanager').SoundManager;


//preload resources this way
gamejs.preload([
                "resources/images/background.png",
                "resources/images/upgrade.png",
                "resources/images/enemy.png",
                "resources/images/sat.png",
                "resources/images/explosion.png",
                "resources/images/enemy_detected.png",
                "resources/images/obstacle.png",
                "resources/images/missle.png",
                "resources/images/laserturret.png",
                "resources/images/slowingturret.png",
                "resources/images/missleturret.png",
                "resources/images/gameover.png"
                ]);
gamejs.preload(SoundManager.getResourceList());
gamejs.ready(main);

/**
 *The main. 
 */
function main() {
	
	var match = new matchjs.Match();
	match.setMap(new mapjs.Map(match,800,480));
	gamejs.display.setMode([800, 480]);
	gamejs.display.setCaption("Tower Defence");
	display_surface = gamejs.display.getSurface();		
	match.getMap().addRadar(new radarjs.Radar(match,[650,50]));	
	//match.getMap().addObstacle(new obstaclejs.Obstacle(match,[650,150],50));
	match.getMap().addObstacle(new obstaclejs.Obstacle(match,[650,250],100));
	//match.getMap().addObstacle(new obstaclejs.Obstacle(match,[650,350],50));
	//match.getMap().addObstacle(new obstaclejs.Obstacle(match,[650,450],50));
	match.getMap().addTurret(new sturretjs.slowingTurret(match, [630, 100]));
	//match.getMap().addObstacle(new obstaclejs.Obstacle(match,[500,50],50));
	//match.getMap().addObstacle(new obstaclejs.Obstacle(match,[500,150],50));
	//match.getMap().addObstacle(new obstaclejs.Obstacle(match,[500,250],50));
	match.getMap().addRadar(new radarjs.Radar(match,[500,250]));
	match.getMap().addTurret(new sturretjs.slowingTurret(match, [400, 250]));
	match.getMap().addTurret(new mturretjs.missleTurret(match, [500, 450]));
	//match.getMap().addObstacle(new obstaclejs.Obstacle(match,[500,350],50));	
	match.getMap().addRadar(new radarjs.Radar(match,[350,20]));
	match.getMap().addObstacle(new obstaclejs.Obstacle(match,[350,150],100));
	//match.getMap().addObstacle(new obstaclejs.Obstacle(match,[350,250],50));
	//match.getMap().addObstacle(new obstaclejs.Obstacle(match,[350,350],50));
	match.getMap().addObstacle(new obstaclejs.Obstacle(match,[350,450],100));	
	//match.getMap().addObstacle(new obstaclejs.Obstacle(match,[200,50],50));
	//match.getMap().addObstacle(new obstaclejs.Obstacle(match,[200,150],50));
	//match.getMap().addObstacle(new obstaclejs.Obstacle(match,[200,250],50));
	//match.getMap().addObstacle(new obstaclejs.Obstacle(match,[200,350],50));
	match.getMap().addTurret(new mturretjs.missleTurret(match, [200, 450]));	
	match.getMap().addTurret(new lturretjs.laserTurret(match, [100, 100]));
	for (var j=0;j<10;j++) {
			match.getMap().addEnemy(new lenemyjs.littleEnemy(match,[match.getMap().getSize()[0]  -2, Math.random()*(match.getMap().getSize()[1]-10)]));
	}	
	var boss = function(msDuration){	
		for (var j=0;j<2;j++) {
			match.getMap().addEnemy(new lenemyjs.littleEnemy(match,[match.getMap().getSize()[0]  -2, Math.random()*(match.getMap().getSize()[1]-10)]));
	}	
			match.getMap().addEnemy(new genemyjs.SuperEnemy(match,[match.getMap().getSize()[0] -2 , Math.random()*(match.getMap().getSize()[1]-10)]));
	}
	var tick = function(msDuration) {
		match.update(msDuration);
		match.draw(display_surface);
	};
	gamejs.time.fpsCallback(tick, this, 60);
	gamejs.time.fpsCallback(boss, this, 1);
		 
}


