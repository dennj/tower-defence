var radar = require('js/radar');
var match = require('js/match');
var gamejs = require('gamejs');

qModule('js/radar');


var test_match=new match.Match();
var test_radar=new radar.Radar(test_match,[100,100]);

test("metodo getMatch", function(){
    strictEqual(test_radar.getMatch(), test_match,"controllo metodo getMatch");
});

test("metodo setStartAngle", function(){
	test_radar.setStartAngle(2);
    strictEqual(test_radar.startAngle, 2,"controllo setStartAngle con ancolo<Pi");
	test_radar.setStartAngle(8);
    strictEqual(test_radar.startAngle, 8  % (2 * Math.PI),"controllo setStartAngle con ancolo>Pi");
    strictEqual(test_radar.currentAngle, 0,"controllo current Angle");
});


test("metodo setCentralAngle", function(){
	test_radar.setCentralAngle(2);
	strictEqual(test_radar.centralAngle, 2,"controllo  setCentralAngle");
	
});