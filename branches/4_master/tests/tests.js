var gamejs = require('gamejs');
var SoundManager = require('js/soundmanager').SoundManager;
var img = "resources/images/";
gamejs.preload([
                "resources/images/background.png",
                "resources/images/enemy.png",
                "resources/images/sat.png",
                "resources/images/enemy_detected.png",
                "resources/images/obstacle.png",
                "resources/images/missle.png",
                "resources/images/laserturret.png",
                "resources/images/slowingturret.png",
                "resources/images/missleturret.png",
                "resources/images/gameover.png"
                ]);
gamejs.preload(SoundManager.getResourceList());
gamejs.ready(function(){
	require('tests/test_map');
	require('tests/test_enemy');	
	require('tests/test_obstacle');
	require('tests/test_radar');
	require('tests/test_match');
});
