var obstacle = require('js/obstacle');
var gamejs = require('gamejs');
var match = require('js/match');

qModule('js/obstacle');


var test_match = new match.Match();
var test_obstacle = new obstacle.Obstacle(match, [100,100],80);

test("metodo getCenter", function(){
    var c = test_obstacle.getCenter();
    if(c[0] == 100 && c[1] == 100)
    	equal(1,1,"Expecting [100,100]");
});

test("metodo getRadius", function(){
    equal(test_obstacle.getRadius(),80,"Expecting 80");
});