var matchjs = require('js/match');
var gamejs = require('gamejs');
qModule('js/match');

test("Default decrease health", function() {
	var match = new matchjs.Match();
	var lives = match.decreaseHealth();
	equal(lives,49999,"Expecting 49999 (50000-1)");
});

test("Custom decrease health", function() {
	var match = new matchjs.Match();
	var lives = match.decreaseHealth(4);
	equal(lives,49996,"Expecting 49996 (50000-4)");
});

test("Invalid decrease health", function() {
	var match = new matchjs.Match();
	try{
		var lives = match.decreaseHealth(-6);
		equal(lives,NaN,"Expecting an Exception");
	}catch(e){
		equal(e,"Invalid hitpoint value","Exception catched");
	}
});


test("Valid Changing of credits", function() {
	var match = new matchjs.Match();
	var cred = match.changeCredit(10);
	equal(cred,10,"Expecting 10");
});

test("Invalid Changing of credits", function() {
	var match = new matchjs.Match();
	try{
		var cred = match.changeCredit();
		equal(cred,NaN,"Expecting an Exception");
	}catch(e){
		equal(e,"Invalid credit value","Exception catched");
	}//catch
});

//test Of positionClick
test("test of positionClick",function(){
	var testMatch=new matchjs.Match();
	var testCord1= [20,60];
	var testCord2= [36,180];
	var testCord3= [90,60];
	var testCord4= [4,13];
	strictEqual(testMatch.positionClick(testCord1),1,"Cord of first line");
	strictEqual(testMatch.positionClick(testCord2),3,"Cord of 4th line");
	strictEqual(testMatch.positionClick(testCord3),-1,"You have not click on the menu");
	strictEqual(testMatch.positionClick(testCord4),0,"Cord of 0 line");
	
	
});