var map = require('js/map');
var gamejs = require('gamejs');
var match = require('js/match');
var turret = require("js/turrets/turret");
var enemy = require('js/enemies/enemy');
var obstacle = require('js/obstacle');
qModule('js/map');

var testMatch = new match.Match();
var width=800;
var height=480;
var testMap=new map.Map(testMatch,width,height);
testMatch.setMap(testMap);

//test of getMatch
test("function getMatch", function(){
	strictEqual(testMap.getMatch(), testMatch, "Function getMatch returns the correct object");
});


//test of getSize 
test("function getSize", function(){
	var testSize = [800,480];
	var mapSize = testMap.getSize();
	deepEqual(testSize, mapSize, "Function getSize returns the correct object");
});

//test of fill
test("function fill", function(){
	testMap.fill([0,0]);
    strictEqual(testMap.isFilled([0,0]), true, "Function fill works");
});

//test of clear
test("function clear", function(){
	testMap.fill([0,0]);
	testMap.clear([0,0])
    strictEqual(testMap.isFilled([0,0]), false, "Function clear works");
});

//test of isFilled
test("function isFilled", function(){
	testMap.clear([0,0])
    strictEqual(testMap.isFilled([0,0]), false, "Function isFilled works");
	strictEqual(testMap.isFilled([0,-1]),true , "It must be filled because it's out of map");
});

//test of addObstacle
test("function addObstacle", function(){
	var x=100;
	var y=100;
	var testCord = [x,y];
	var testRad=80;
    var testObstacle=new obstacle.Obstacle(testMatch, testCord ,testRad);
    testMap.addObstacle(testObstacle);
    
    strictEqual(testMap.getObstacles().has(testObstacle) , true, "function addObstacle adds Obstacle");
    strictEqual(testMap.isFilled(testCord),true,"Check if center is filled");
    strictEqual(testMap.isFilled([x+80,y]),true,"Check if random point is filled");
    strictEqual(testMap.isFilled([x+20,y-10]),true,"Check if random point is filled");
});

//test of getObstacle
test("function getObstacle", function(){
	var testCord = [100,100];
	var testRad=80;
    var testObstacle=new obstacle.Obstacle(testMatch, testCord ,testRad);
    testMap.addObstacle(testObstacle);
    strictEqual(testMap.getObstacles().has(testObstacle),true , "Function getObstacle returns the correct object");
});

//test of removeObstacle
test("function removeObstacle", function(){
	var x=100;
	var y=100;
	var testCord = [x,y];
	var testRad=80;
    var testObstacle=new obstacle.Obstacle(testMatch, testCord ,testRad);
    testMap.addObstacle(testObstacle);
    testMap.removeObstacle(testObstacle);
    
    strictEqual(testMap.getObstacles().has(testObstacle) , false, "function removeObstacle remove Obstacle");
    strictEqual(testMap.isFilled(testCord),false,"Check if center is not filled");
    strictEqual(testMap.isFilled([x+80,y]),false,"Check if random point is not filled");
    strictEqual(testMap.isFilled([x+20,y-10]),false,"Check if random point is not filled");
});

//test of addTurret
test("function addTurret", function(){
	var testCord= [100,100];
	var testTurret=new turret.Turret(testMatch,testCord);
	testMap.addTurret(testTurret);
	strictEqual(testMap.getTurrets().has(testTurret),true,"function addTurret adds turret")
	
});

//test of removeTurret
test("function removeTurret", function(){
	var testCord= [100,100];
	var testTurret=new turret.Turret(testMatch,testCord);
	testMap.addTurret(testTurret);
	testMap.removeTurret(testTurret);
	strictEqual(testMap.getTurrets().has(testTurret),false,"function removeTurrets works")
	
});

//test of addEnemy
test("function addEnemy", function(){
	var testEnemy = new enemy.Enemy(testMatch,[100,100]);
	testMap.addEnemy(testEnemy);
	strictEqual(testMap.getEnemies().has(testEnemy),true,"function addEnemy adds enemy");	
});

//test of removeEnemy
test("function removeEnemy", function(){
	var testEnemy = new enemy.Enemy(testMatch,[100,100]);
	testMap.addEnemy(testEnemy);
	testMap.removeEnemy(testEnemy);
	strictEqual(testMap.getEnemies().has(testEnemy),false,"function removeEnemy removes enemy");	
});

//test of isOver
test("function isInside", function(){
	var testCord1=[1,1];
	var testCord2=[1,-10];
	var testCord3=[-5,100];
	var testCord4=[500,700];
	strictEqual(testMap.isInside(testCord1),true,"[1,1] is inside the map");
	strictEqual(testMap.isInside(testCord2),false,"[1,-10] is outside the map");
	strictEqual(testMap.isInside(testCord3),false,"[-5,100] is outside the map");
	strictEqual(testMap.isInside(testCord4),false,"[500,700] is outside the map");
});
