var enemy = require('js/enemies/enemy');
var matchjs = require('js/match');
var mapjs = require('js/map');
var gamejs = require('gamejs');

qModule('js/enemy');


test("Enemy visibility ON", function() {
	var match = new matchjs.Match();
	match.setMap(new mapjs.Map(match,800,480));
	var enemyTest = new enemy.Enemy(match,[100,100]);
	enemyTest.setDetected(true);
	var det = enemyTest.isDetected();
	equal(det,true,"Expecting a TRUE value");
});

test("Enemy visibility OFF", function() {
	var match = new matchjs.Match();
	match.setMap(new mapjs.Map(match,800,480));
	var enemyTest = new enemy.Enemy(match,[100,100]);
	enemyTest.setDetected(false);
	var det = enemyTest.isDetected();
	equal(det,false,"Expecting a FALSE value");
});