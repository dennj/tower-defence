var map = require('js/map');
var match = require('js/match');
var turret = require('js/turret');
var enemy = require('js/enemy');
var bullet = require('js/bullet');
qModule('js/bullet');

//test getMatch
test("Checking if getMatch works correctly", function() {
	var testMatch = new match.Match();
	var testTurret = new turret.Turret(testMatch, [1,1]);
	var testEnemy = new enemy.Enemy(testMatch, [1,1]);
	var testBullet = new bullet.Bullet(testMatch, testTurret, testEnemy);
	
	strictEqual(testBullet.getMatch(), testMatch, "Function returns its match");	
});

 //test getTurret
test("Checking if getTurret works correctly", function() {
	var testMatch = new match.Match();
	var testTurret = new turret.Turret(testMatch, [1,1]);
	var testEnemy = new enemy.Enemy(testMatch, [1,1]);
	var testBullet = new bullet.Bullet(testMatch, testTurret, testEnemy);
	
	strictEqual(testBullet.getTurret(), testTurret, "Function returns its turret");	
});