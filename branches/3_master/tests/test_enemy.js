var enemy = require('js/enemy');
var match = require('js/match');
qModule('js/enemy');

//Test hit
test("Test if hit works correctly", function() {
	var testMatch = new match.Match;
	var testEnemy = new enemy.Enemy( testMatch , [100,100] );
	var startLife = testEnemy.health;
	var damage = 5;

	testEnemy.hit(damage);
	ok(testEnemy.health === startLife - damage, "Function hit subtracts the correct ammount of life with positive parameter");

	testEnemy.hit(-damage);
	ok(testEnemy.health === startLife, "Function hit subtracts the correct ammount of life with negative parameter");
});


//Test setDetected
test("Test if setDetected works correctly", function() {
	var testMatch = new match.Match;
	var testEnemy = new enemy.Enemy(testMatch, [100,100]);

	testEnemy.setDetected(true);
	ok(testEnemy.detected > 0, "Function setDetected works correctly with parameter: true");

	testEnemy.setDetected(false);
	ok(testEnemy.detected == 0, "Function setDetected works correctly with parameter: false");
});

//Test isDetected
test("Test if isDetected works correctly", function() {
	var testMatch = new match.Match;
	var testEnemy = new enemy.Enemy(testMatch, [100,100]);

	testEnemy.setDetected(true);
	ok(testEnemy.isDetected() === true, "Function isDetected works correctly with parameter :true");

	testEnemy.setDetected(false);
	ok(testEnemy.isDetected() === false, "Function isDetected works correctly with parameter :false");
});
