var map = require('js/map');
var match = require('js/match');
var turret = require('js/turret');
var enemy = require('js/enemy');
var explosion = require('js/explosion');
qModule('js/explosion');

//test getDuration
test("Checking if getDuration works correctly", function() {
	var testMatch = new match.Match();
	var testDuration = 1000;
	var testExplosion = new explosion.Explosion(testMatch, [1,1], testDuration);
	
	
	strictEqual(testExplosion.getDuration(), testDuration, "Function returns its duration");	
});

//test getMatch
test("Checking if getMatch works correctly", function() {
	var testMatch = new match.Match();
	var testDuration = 1000;
	var testExplosion = new explosion.Explosion(testMatch, [1,1], testDuration);
	
	
	strictEqual(testExplosion.getMatch(), testMatch, "Function returns its match");	
});

//test update
test("Checking if update works correctly", function() {
	var testMatch = new match.Match();
	var testDuration = 1000;
	var testExplosion = new explosion.Explosion(testMatch, [1,1], testDuration);
	
	var testUpdate=500;
	testExplosion.update(testUpdate);
	strictEqual(testExplosion.getDuration(), testDuration-testUpdate , "Function update works correctly");	
});

//test if explosion terminates correctly
test("Checking if explosion dies after its duration", function() {
	var testMatch = new match.Match();
	var testDuration = 1000;
	var testExplosion = new explosion.Explosion(testMatch, [1,1], testDuration);
	
	testExplosion.update(500);
	ok(! (testExplosion.isDead()),"Explosion is alive after 500 ms (duration = 1000ms)");
	testExplosion.update(800);
	ok(testExplosion.isDead(),"Explosion terminates correctly");	
});

