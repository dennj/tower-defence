var map = require('js/map');
var match = require('js/match');
qModule('js/match');

//Test of get/set Map
test("Checking the functions getMap, setMap", function() {
	var height = 480;
	var width = 800;
	var testMatch = new match.Match;
	var testMap = new map.Map(testMatch, height, width);

	testMatch.setMap(testMap);
	strictEqual(testMatch.getMap(), testMap, "Function get/set Map works correcty");
});

//Test of decreseHealth
test("Checking the function decreaseHealth", function() {
	var testMatch = new match.Match;
	var startHealth = testMatch.health;
	var damage=10;

	strictEqual(testMatch.decreaseHealth(damage), (startHealth - damage), "Function decreseHealth usign a positive number as parameter returs the correct value");
	
	strictEqual(testMatch.decreaseHealth(), (startHealth - damage - 1), "Function decreseHealth usign a positive number as parameter returs the correct value");
});

//Test of changeCredit
test("Checking the function changeCredit", function() {
	var testMatch = new match.Match;
	var startCredit = testMatch.credits;
	var credit = 10;

	strictEqual(testMatch.changeCredit(credit), (startCredit + credit), "Function changeCredit usign a positive number as parameter returs the correct value");

	strictEqual(testMatch.changeCredit(-credit), startCredit, "Function changeCredit usign a negative number as parameter returs the correct value");
});

//Test of changeScore
test("Checking the function changeScore", function() {
	var testMatch = new match.Match;
	var startScore = testMatch.score;
	var score = 10;

	strictEqual(testMatch.changeScore(score), (startScore + score), "Function changeScore usign a positive number as parameter returs the correct value");

	strictEqual(testMatch.changeScore(-score), startScore, "Function changeScore usign a negative number as parameter returs the correct value");
});