var map = require('js/map');
var match = require('js/match');
var obstacle = require('js/obstacle');
var enemy = require('js/enemy');
var radar = require('js/radar');
qModule('js/map');

test("Checking that every field is set to false", function() {
	var height = 480;
	var width = 800;
	var testMap = new map.Map(new match.Match, width, height);
	var testField = false;

	for(var x=0; x < width; x++){
		for(var y=0; y < height; y++){
			testField = testMap.isFilled([x,y]);
			if (testField) break;
		}
		if (testField) break;
	}
	strictEqual(testField, false, "Every cell in the field is set to false");
});

//Test getMatch
test("Test if getMatch works correctly", function() {
	var height = 480;
	var width = 800;
	var testMatch = new match.Match();
	var testMap = new map.Map(testMatch, width, height);

	strictEqual(testMap.getMatch(), testMatch, "Function testMatch returns the correct object");
});


//Test isFilled
test("Check some coordinates out of range for isFilled", function(){
	var height = 480;
	var width = 800;
	var testMap = new map.Map(new match.Match, width, height);
	var y = testMap.getSize()[1];
	var x = testMap.getSize()[0];

	strictEqual(testMap.isFilled([x+1, y]), true, "Cell [" + (x + 1) + "," + y + "] is out of range so isFilled returns true");
	strictEqual(testMap.isFilled([x, y+1]), true, "Cell [" + x + "," + (y + 1) + "] is out of range so isFilled returns true");
	strictEqual(testMap.isFilled([-1, 0]), true, "Cell [-1,0] is out of range so isFilled returns true");
	strictEqual(testMap.isFilled([0, -1]), true, "Cell [0,-1] is out of range so isFilled returns true");
});

//Test fill
test("Test if fill works correctly", function(){
	var height = 480;
	var width = 800;
	var testMap = new map.Map(new match.Match, width, height);

	testMap.fill([0,0]);
	strictEqual(testMap.isFilled([0,0]), true, "Cell [0,0] is set true by fill");
});

//Test clear
test("Test if clear works correctly", function(){
	var height = 480;
	var width = 800;
	var testMap = new map.Map(new match.Match, width, height);

	testMap.fill([0,0]);
	testMap.clear([0,0]);
	strictEqual(testMap.isFilled([0,0]), false, "Cell [0,0] is set false by clear");
});

//Test getSize
test("Test if getSize works correctly",function(){
	var height = 480;
	var width = 800;
	var testMap = new map.Map(new match.Match, width, height);

	strictEqual(testMap.getSize()[0], width, "Function testSize returns the correct values of width");
	strictEqual(testMap.getSize()[1], height, "Function testSize returns the correct values of height");
});

//Test add/get Obstacle
test("Test if add/get Obstacle",function(){
	var height = 480;
	var width = 800;
	var testMap = new map.Map(new match.Match, width, height);
	var testObs = new obstacle.Obstacle([100,100],100);
	
	testMap.addObstacle(testObs);
	ok(testMap.getObstacles().has(testObs),"Function add/get obstacle work correctly");
	
});

//Test add/get Enemy
test("Test if add/get Enemy",function(){
	var height = 480;
	var width = 800;
	var testMatch=new match.Match();
	var testMap = new map.Map(testMatch, width, height);
	var testObs = new enemy.Enemy(testMatch,[100,100]);
	
	testMap.addEnemy(testObs);
	ok(testMap.getEnemies().has(testObs),"Function add/get enemy work correctly");
	
});

//Test add/get Radar
test("Test if add/get Radar",function(){
	var height = 480;
	var width = 800;
	var testMatch=new match.Match();
	var testMap = new map.Map(testMatch, width, height);
	var testObs = new radar.Radar(testMatch,[100,100]);
	
	testMap.addRadar(testObs);
	ok(testMap.getRadars().has(testObs),"Function add/get enemy work correctly");
	
});