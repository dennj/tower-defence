var antColonyProvider = require('js/antColonyProvider');
var map = require('js/map');
var match = require('js/match');
qModule('js/antColonyProvider')

//test of setMap
test("Checking if setMap works correctly", function() {
	var height = 480;
	var width = 800;
	var testMap = new map.Map(new match.Match, width, height);
	var testProvider= antColonyProvider.AntColonyProvider.setMap(testMap);

	strictEqual(antColonyProvider.AntColonyProvider.map, testMap, "Map is correctly assigned.");
	strictEqual(antColonyProvider.AntColonyProvider, testProvider, "The function returns the provider as expected.");
});

//test of get
test("Checking if get works correctly", function() {
	var height = 480;
	var width = 800;
	var testMap = new map.Map(new match.Match, width, height);
	var testProvider= antColonyProvider.AntColonyProvider.setMap(testMap);
	var testAntColony = antColonyProvider.AntColonyProvider.get();

	strictEqual(antColonyProvider.AntColonyProvider.instance, testAntColony, "The variable holds the expected antColony.");
	strictEqual(antColonyProvider.AntColonyProvider.get(), testAntColony, "The function returns the expected antColony.");
});

test("Checking that the object is actually destroyed", function() {
	var height = 480;
	var width = 800;
	var testMap = new map.Map(new match.Match, width, height);
	var testProvider= antColonyProvider.AntColonyProvider.setMap(testMap);
	var testAntColony = antColonyProvider.AntColonyProvider.get();

	antColonyProvider.AntColonyProvider.destroy();

	ok(testAntColony !== antColonyProvider.AntColonyProvider.get(), "The object was destroyed.");
});