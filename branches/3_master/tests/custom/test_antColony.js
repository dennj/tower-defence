var antColony = require('js/antColony');
var map = require('js/map');
var match = require('js/match');
qModule('js/antColony');

test("Testing update remaining time.", function() {
	var height = 480;
	var width = 800;

	var testMap = new map.Map(new match.Match, width, height);
	var antColonyTest = new antColony.AntColony(testMap);
	strictEqual(antColonyTest.initialRefresh, antColonyTest.refresh, "Initial refresh point is correct.");
	antColonyTest.update(200);
	strictEqual(antColonyTest.refresh, antColonyTest.initialRefresh - 200, "Refresh timer after subtraction is correct.")
});