var obstacle = require('js/obstacle');
qModule('js/obstacle');

//Test getRadius
test("Cheching getRadius",function(){
	var testRadius=100;
	var testObstacle = new obstacle.Obstacle([100,100], testRadius);

	ok(testObstacle.getRadius() === testRadius, "Function getRadius returns the correct value using positive radius" );
});

//Test getCenter
test("Cheching getCenter",function(){
	var testCoords=100;
	var testObstacle = new obstacle.Obstacle( testCoords,100);
	
	ok((testObstacle.getCenter()[0] ===  testCoords[0]) && (testObstacle.getCenter()[1] ===  testCoords[1]), "Function getRadius returns the correct coordinate" );
});


