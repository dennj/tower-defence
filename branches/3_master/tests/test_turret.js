var turret = require('js/turret');
var match = require('js/match');
qModule('js/turret');

//Test getRadius
test("Cheching getRadius",function(){
	var testMatch = new match.Match;
	var testTurret = new turret.Turret(testMatch, [100,100]);
	var level = testTurret.level;
	var testRadius= testTurret.upgradeList[level].radius;
	
	ok(testTurret.getRadius() === testRadius, "Function getRadius returns the correct value" );
});

//Test of getMatch
test("Checking getMatch", function() {
	var testMatch = new match.Match;
	var testTurret = new turret.Turret(testMatch, [100,100]);

	ok(testTurret.getMatch() === testMatch, "The match is successfully fetched");
});


//Test of upgrade
test("Checking upgrade", function() {
	var testMatch = new match.Match;
	var testTurret = new turret.Turret(testMatch, [100,100]);

	strictEqual(testTurret.level, 0, "New radar is level 0")
	testTurret.upgrade();
	strictEqual(testTurret.level, 1, "Upgraded radar is level 1")
});


