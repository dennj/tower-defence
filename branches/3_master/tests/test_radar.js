var radar = require('js/radar');
var match = require('js/match');
qModule('js/radar');

//Test getRadius
test("Cheching getRadius",function(){
	var testMatch = new match.Match;
	var testRadar = new radar.Radar(testMatch, [100,100]);
	var level = testRadar.level;
	var testRadius= testRadar.upgradeList[level].radius;
	
	ok(testRadar.getRadius() === testRadius, "Function getRadius returns the correct value" );
});

//Test of getMatch
test("Checking getMatch", function() {
	var testMatch = new match.Match;
	var testRadar = new radar.Radar(testMatch, [100,100]);

	ok(testRadar.getMatch() === testMatch, "The match is successfully fetched");
});

//Test of setStartAngle
test("Checking the setStartAngle", function() {
	var testMatch = new match.Match;
	var testRadar = new radar.Radar(testMatch, [100,100]);
	var testAngle = 100;
	
	testRadar.setStartAngle(testAngle);
	strictEqual(testRadar.startAngle, testAngle, "setStartAngle successful with a positive number");
	
	testRadar.setStartAngle(-testAngle);
	strictEqual(testRadar.startAngle, -testAngle, "setStartAngle successful with a negative number");
});

//Test of setCentralAngle
test("Checking the setCentralAngle", function() {
	var testMatch = new match.Match;
	var testRadar = new radar.Radar(testMatch, [100,100]);
	var testAngle = 100;


	testRadar.setCentralAngle(testAngle);
	strictEqual(testRadar.centralAngle, testAngle, "setCentralAngle successful with a positive number")
	
	testRadar.setCentralAngle(-testAngle);
	strictEqual(testRadar.centralAngle, -testAngle, "setCentralAngle successful with a negative number")
});

//Test of upgrade
test("Checking upgrade", function() {
	var testMatch = new match.Match;
	var testRadar = new radar.Radar(testMatch, [100,100]);

	strictEqual(testRadar.level, 0, "New radar is level 0")
	testRadar.upgrade();
	strictEqual(testRadar.level, 1, "Upgraded radar is level 1")
});


