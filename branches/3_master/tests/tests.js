var gamejs = require('gamejs');

gamejs.preload([
	config.dir.images + "background.png",
	config.dir.images + "sat.png",
	config.dir.images + "enemy.png",
	config.dir.images + "enemy_detected.png",
	config.dir.images + "obstacle.png",
	config.dir.images + "turret.png",
	config.dir.images + "bullet.png",
	config.dir.images + "explosion.png"
]);

gamejs.ready(function(){
	require('tests/test_enemy');
	require('tests/test_map');
	require('tests/test_obstacle');
	require('tests/test_radar');
	require('tests/test_match');
	require('tests/test_turret');
	require('tests/test_bullet');
	require('tests/test_explosion');


	// custom, hardened tests
	require('tests/custom/test_antColonyProvider');
	require('tests/custom/test_antColony');
});

