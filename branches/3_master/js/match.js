var gamejs = require('gamejs');

/**
 * Creates a match,
 */
var Match = exports.Match = function() {
	this.score = 120;
	this.credits = 0;
	this.health = 10;
	this.startHealth = this.health;
	this.map = null;
	this.font = new gamejs.font.Font('25px ubuntu, sans-serif');
}

/**
* Return current map
*/
Match.prototype.getMap = function() {
	return this.map;
}

/**
* Set current map
* @param {Map}
*/
Match.prototype.setMap = function(map){
	this.map = map;
}

/**
 * Decrease health by one point
 *
 * @param {undefined|number} hitpoint Removes one health point if undefined, or the specified amount
 *
 * @return {number} The remaining health points
 * @throws {Exception} "Invalid hitpoint value"
 */
Match.prototype.decreaseHealth = function(hitpoint) {
	if (typeof hitpoint === "undefined"){
		this.health--;
	}
	else {
		if (typeof hitpoint === "number" && hitpoint > 0){
				this.health -= hitpoint;
		}
		else{
			throw {
				name: "TypeError",
				message: "Invalid hitpoint value"
			};
		}
	}
	if(this.health>=0)
		return this.health;
	else window.parent.location.reload(true);
}

/**
 * Change credit amount
 *
 * @param {undefined|number} amount change credit by amount
 *
 * @return {number} The remaining credit points
 * @throws {Exception} "Invalid credit value"
 */
Match.prototype.changeCredit = function(amount) {
	if (typeof amount === "number"){
		this.credits += amount;
	}
	else {
		throw {
			name: "TypeError",
			message: "Invalid credits value"
		};
	}

	return this.credits;
}

/**
 * Change score amount
 *
 * @param {undefined|number} amount change score by amount
 *
 * @return {number} The remaining score points
 * @throws {Exception} "Invalid amount value"
 */
Match.prototype.changeScore = function(amount) {
	if (typeof amount === "number"){
		this.score += amount;
	}
	else {
		throw {
			name: "TypeError",
			message: "Invalid score value"
		};
	}

	return this.score;
}

/**
 * Update the data in the game
 */
Match.prototype.update = function() { }

/**
 * Draw the overlay information
 * @param {gamejs.surface} where to draw
 */
Match.prototype.draw = function(surface) { 
	var endLine = [110,10];
	var startLine = [10,10];
	var lifeLine = Math.round(100*this.health/this.startHealth) + startLine[0];
	
	
	print_score = this.font.render(this.score, "#fff");
    surface.blit(print_score, [720, 8]);
	
	
	gamejs.draw.line(surface, '#ff0000', startLine, endLine, 4);
	if (lifeLine>startLine[0])
		gamejs.draw.line(surface, '#adff2f', startLine, [lifeLine,10] , 4);
}