var gamejs = require('gamejs');
var antColonyProvider = require('js/antColonyProvider');
var explosion = require ('js/explosion');

/*
 * NEMICI: un oggetto Animation [ciclerà tra normale, colpito, esploso] caratterizzato da:
 * - Percorso, un array di punti che individuano, passo passo il percorso da seguire
 * - Velocità , un intero che dice il numero di pixel passati in un frame
 * - Vita, un intero che rappresenta la vita rimasta
 * - Alpha, un intero [0,100] che dice se e quanto il nemico è visibile a occhio nudo di notte
 * - Danno, un intero che dice il numero di vite tolte al pianeta, se questo nemico ci arriva
 * - visibilityTime , tempo che il nemico rimane visibile dopo il ping
 * - detected , un intero che indica se il nemico e` stato rilevato dal radar e quindi e` visibile alle armi
 * - Valore, un intero che ci dice quanto prendiamo uccidendo questo nemico.
 */

/**
 * Creates an enemy
 *
 * @extends Sprite
 * @constructor
 */
var Enemy = exports.Enemy = function(match, rect,health) {
	Enemy.superConstructor.apply(this, arguments);

	this.path = new Array();
	this.path[0] = [800,Math.floor(Math.random()*481)];
	this.pathIndex = 0;
	//this.speed = 20 + (40 * Math.random());
	this.startHealth = this.health = health;
	this.alpha = 30;
	this.damage = 1;
	this.visibilityTime = 1000; //1000ms
	this.detected = 0;
	this.prize = 1;
	this.currentMatch = match;

	this.originalImage = gamejs.image.load(config.dir.images + "enemy.png");
	this.detectedImage = gamejs.image.load(config.dir.images + "enemy_detected.png");
	var dims = this.originalImage.getSize();
	this.originalImage = gamejs.transform.rotate(
		gamejs.transform.scale(
			this.originalImage,
			[dims[0] * (0.1), dims[1] * (0.1)]
		), -90
	);
	this.detectedImage = gamejs.transform.rotate(
		gamejs.transform.scale(
			this.detectedImage,
			[dims[0] * (0.1), dims[1] * (0.1)]
		), -90
	);
	this.rect = new gamejs.Rect([this.path[0][0] - (this.originalImage.getSize()[0] / 2),
	                 			this.path[0][1] - (this.originalImage.getSize()[1] / 2)],
	                 			this.originalImage.getSize());
	
	this.originalImage.setAlpha(1 - this.alpha / 100);
};

/**
 * Extending Sprite with Enemy
 */
gamejs.utils.objects.extend(Enemy, gamejs.sprite.Sprite);

/**
 * Subtracts health points
 *
 * @param {number} hitRate The amount of health to subtract
 */
Enemy.prototype.hit = function(hitRate) {
	this.health -= hitRate;

	if (this.health < 0) {
		this.kill();
		this.currentMatch.changeScore(1); // Increase score when enamy died
		var expl = new explosion.Explosion(this.currentMatch, this.path[this.pathIndex], 1000);
		this.currentMatch.getMap().addExplosion(expl);
	}
};

/**
 * Slow enamy motion
 *
 * @param {number} slowdown Percentage of slowdown
 */
Enemy.prototype.slowdown = function(slowmotion) {
	this.pathIndex-=slowmotion;

};

/**
 * Set detected flag to 1, it performs also a timer of visibility
 *
 * @param {boolean} detected True to turn detected mode on, false to turn it off
 */
Enemy.prototype.setDetected = function(detected) {
	if (detected) {
		this.detected = this.visibilityTime;
	}
	else {
		this.detected = 0;
	}
};

/**
 * Return detected flag
 *
 * @return {boolean} true if detected > 0
 */
Enemy.prototype.isDetected = function() {
	return this.detected > 0;
};

/**
 * Return enemy coordinates
 *
 * @return {int, int}
 */

Enemy.prototype.getCoordinates = function() {
	return this.path[this.pathIndex];
};

/**
 * Return the current rect
 * @return {Rect} the current rect.
 */
Enemy.prototype.getRect = function() {
	return this.rect;
}

/**
 * Updates the position of the Enemy
 */
Enemy.prototype.update = function(msDuration) {
	var dim = this.originalImage.getSize();
	var next = antColonyProvider.AntColonyProvider.get().next(this.getCoordinates(), dim);

	// put the next position to follow in the path
	this.pathIndex++;
	this.path[this.pathIndex] = next;

	// actions to take once the enemy is past the planet
	if (this.path[this.pathIndex][0] <= 0) {

		// the ant colony needs
		antColonyProvider.AntColonyProvider.get().done(this.path);
		// take health points from the planet
		this.currentMatch.decreaseHealth(this.damage);
		// bye!
		this.kill();

		return;
	}

	this.rect = new gamejs.Rect([next[0] - (this.originalImage.getSize()[0] / 2),
	                 			next[1] - (this.originalImage.getSize()[1] / 2)],
	                 		this.originalImage.getSize());

	if (this.isDetected()) {
		if (this.detected - msDuration > 0) {
			this.detected -= msDuration;
		}
		else {
			this.detected = 0;
		}
	}
};

Enemy.prototype.draw = function(surface) {
	//draw enemy
	var dim = this.originalImage.getSize();
	var upLeft = gamejs.utils.vectors.subtract(this.path[this.pathIndex], gamejs.utils.vectors.divide(dim, 2));
	surface.blit( ! this.detected ? this.originalImage : this.detectedImage, upLeft);
	
	//if enemy is detected the health-bar is shown
	if(this.isDetected()) {
		var position = this.path[this.pathIndex];
		var startLine = gamejs.utils.vectors.subtract(position, gamejs.utils.vectors.divide([dim[0], dim[1] *3/2], 2));
		var endLine = gamejs.utils.vectors.add(startLine, [dim[0] *  this.health /this.startHealth, 0]);
		gamejs.draw.line(surface,  '#adff2f', startLine, endLine, 2);
	}
};