var gamejs = require('gamejs');

/*
 * OSTACOLI
 *
 * Un oggetto Sprite caratterizzato da:
 * - Centro, Dimensione [piccolo, medio, grande]
 * - Costo(quanto costa un ostacolo)
 *
 * Lo sprite rappresenterà una griglia deformata.
 */

/**
 * Creates a new obstacle
 *
 * @param {array} center The coordinates as vector [x, y]
 * @param {number} radius The radius of the obstacle
 *
 * @extends Sprite
 * @constructor
 */
var Obstacle = exports.Obstacle = function(coords, radius) {
	Obstacle.superConstructor.apply(this, arguments);
	this.center = coords;
	this.currentRadius = radius;
	this.image = gamejs.image.load(config.dir.images + "obstacle.png");
	this.image = gamejs.transform.scale(
		this.image,
		[2 * radius, 2 * radius]
	);

	this.rect = new gamejs.Rect([
			this.center[0] - (this.image.getSize()[0] / 2),
			this.center[1] - (this.image.getSize()[1] / 2)
		],
		this.image.getSize()
	);
};

/**
 * Extending Sprite with Enemy
 */
gamejs.utils.objects.extend(Obstacle, gamejs.sprite.Sprite);


/**
 * Large-sized obstacle
 * @constant
 */
Obstacle.prototype.SIZE_BIG = 80;

/**
 * Mid-sized obstacle
 * @constant
 */
Obstacle.prototype.SIZE_MEDIUM = 60;

/**
 * Small-sized obstacle
 * @constant
 */
Obstacle.prototype.SIZE_SMALL = 40;

/**
 * Returns the vector of the center of the Obstacle
 *
 * @returns {array} The coordinates as vector [x, y]
 */
Obstacle.prototype.getCenter = function() {
	return this.center;
};

/**
 * Returns the radius of the Obstacle
 *
 * @returns {number} The radius of the Obstacle
 */
Obstacle.prototype.getRadius = function() {
	return this.currentRadius;
};

/**
 * Draws the obstacles in the given Surface.
 *
 * @param {gamejs.Surface} surface The Surface where draw on.
 */
Obstacle.prototype.draw = function(surface) {
	//draw obstacle tower
	var x_obstacle = this.center[0] - this.image.getSize()[0] / 2;
	var y_obstacle = this.center[1] - this.image.getSize()[1] / 2;
	surface.blit(this.image, [x_obstacle, y_obstacle]);
};