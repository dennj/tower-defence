var gamejs = require('gamejs');
var explosion = require ('js/explosion');

/**
 * Create a Turret object
 *
 * @param {Match} match The match where this object will work
 * @param {[number, number]} coords The radar's position, in format [x, y]
 */
var Turret = exports.Turret = function(match, coords) {
	Turret.superConstructor.apply(this, arguments);
	this.currentMatch = match;
	this.center = coords;
	this.level = 0;
	this.currentRadius;
	this.upgradeList;
	this.target = [10, 10]; // coordinates locked target
	
	this.startHealth = this.health = 1000;
};

/**
 * Turret extends Sprite
 */
gamejs.utils.objects.extend(Turret, gamejs.sprite.Sprite);

/**
 * Return the length of the detector ray
 *
 * @return {number} The length of the ray, in pixel
 */
Turret.prototype.getRadius = function() {
	return (this.upgradeList[this.level]).radius;
};

/**
 * Return the coordinate of the radar
 *
 * @return {[number,number]} the center of the radar  [x,y]
 */
Turret.prototype.getCenter = function() {
	return this.center;
};

/**
 * Return the match where this turret belongs
 *
 * @param {Match} The match where this turret work.
 */
Turret.prototype.getMatch = function() {
	return this.currentMatch;
};


/**
 * Triggers a turret's upgrade
 */
Turret.prototype.upgrade = function() {
	if(this.level < 2)
		this.level++;
	else return false;
	return true;
};

/**
 * Subtracts health points
 *
 * @param {number} hitRate The amount of health to subtract
 */
Turret.prototype.hit = function(hitRate) {
	this.health -= hitRate;

	if (this.health < 0) {
		this.kill();
		var expl = new explosion.Explosion(this.currentMatch, this.center, 1000);
		this.currentMatch.getMap().addExplosion(expl);
	}
};