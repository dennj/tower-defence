var ant = require('js/antColony');

var AntColonyProvider = exports.AntColonyProvider = {
	/**
	 * Single instance of the AntColony
	 *
	 * @var {null|AntColony}
	 */
	"instance": null,

	/**
	 * Currently set reference to Map
	 *
	 * @var {null|Map}
	 */
	"map": null,

	/**
	 * Returns an instance of AntColony and creates one if it doesn't exist yet
	 *
	 * @return {AntColony}
	 */
	"get": function() {
		if (AntColonyProvider.instance === null) {
			AntColonyProvider.instance = new ant.AntColony(AntColonyProvider.map);
		}

		return AntColonyProvider.instance;
	},

	/**
	 * Destroys the current AntColony
	 *
	 * @return {AntColonyProvider}
	 */
	"destroy": function() {
		AntColonyProvider.instance = null;

		return AntColonyProvider;
	},

	/**
	 * Set a map for a newly created AntColony
	 *
	 * @param map
	 * @return {AntColonyProvider}
	 */
	"setMap": function(map) {
		AntColonyProvider.map = map;

		return AntColonyProvider;
	}
}