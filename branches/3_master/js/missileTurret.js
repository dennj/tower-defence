var gamejs = require('gamejs');
var turretjs = require("js/turret");
var bullet = require('js/bullet');

/**
 * Create a Turret object
 *
 * @param {Match} match The match where this object will work
 * @param {[number, number]} coords The radar's position, in format [x, y]
 */
var missileTurret = exports.missileTurret = function(match, coords) {
	missileTurret.superConstructor.apply(this, arguments);
	this.currentMatch = match;
	this.center = coords;
	this.target = [10, 10]; // coordinates locked target
	this.level = 0;
	this.upgradeList = [
		{ price: 20, radius: 300, velocity: 50, resaleValue: 70 }, // livello 0
		{ price: 15, radius: 400, velocity: 25, resaleValue: 100 }, // livello 1
		{ price: 15, radius: 400, velocity: 18, resaleValue: 100 } // livello 2
	];
	this.radius = this.upgradeList[this.level].radius;
	this.currentRadius = this.radius;
	this.image = gamejs.image.load(config.dir.images + "turret.png");
	this.startHealth = this.health = 1000;
	this.reload=this.upgradeList[this.level].velocity;

	this.bullets = new gamejs.sprite.Group();
};

/**
 * missileTurret extends Turret
 */
gamejs.utils.objects.extend(missileTurret, turretjs.Turret);

/**
 * Updates the position of the detector ray of the turret.
 *
 * @param {number} msDuration The time past from the last call, in milliseconds
 */
missileTurret.prototype.update = function(msDuration) {
	var vel = this.upgradeList[this.level].velocity; //shot ratio
	this.bullets.update();
	if(this.reload > 0)
		this.reload--;

	var target;
	var detected = false;
	var distance;
	var center = this.center;
	var radius = this.radius;
	this.currentMatch.getMap().getEnemies().sprites().forEach(function(enemy) {
		if (enemy.isDetected() && ! detected) {
			//check if the enemy is in the turret radius
			if(gamejs.utils.vectors.distance(center, enemy.getCoordinates()) < radius){
				detected = true;
				target = enemy;
			};
		};
	});
	
	if((this.reload==0) && detected){
		this.bullets.add(new bullet.Bullet(this.currentMatch,this , target));
		this.reload = this.upgradeList[this.level].velocity;
	}
	else
		this.target = target;
};

/**
 * Draws the turret in the given Surface
 *
 * @param {gamejs.Surface} surface The Surface where draw on
 */
missileTurret.prototype.draw = function(surface) {
	//draw turret tower
	var dim = this.image.getSize();
	var upLeft = gamejs.utils.vectors.subtract(this.center, gamejs.utils.vectors.divide(dim, 2));
	surface.blit(this.image, upLeft);
	
	//healt bar
	var startLine = gamejs.utils.vectors.subtract(this.center, gamejs.utils.vectors.divide([dim[0], dim[1] *3/2], 2));
	var endLine = gamejs.utils.vectors.add(startLine, [dim[0] *  this.health /this.startHealth, 0]);
	gamejs.draw.line(surface, '#adff2f', startLine, endLine, 2);
	
	//draw its bullets
	this.bullets.draw(surface);
};