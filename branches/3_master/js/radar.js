var gamejs = require('gamejs');
var turretjs = require("js/turret");

/**
 * RADAR: an object representing a radar. The detection area is a circular sector.
 *
 * Properties
 * - currentMatch : the match where the radar work.
 * - center : coordinates of the radar's position;
 * - centralAngle : angle covered by the radar (central angle of the circular sector);
 * - startAngle :  angle where the detection starts;
 * - currentAngle :  current angle into the circular sector;
 * - radius : max distance of detection (the radius of the circular sector);
 * - currentRadius : actual distance of detection, useful in several drawing problem;
 * - angularVelocity : number of radians that the detector ray covers in a second;
 * - level : the upgrade level of the radar;
 * - upgradeList : a list containing all the upgrade info for each upgrade level;
 * - resaleValue : the radar's remaining price;
 * - image : a Surface object with the radar's picture;
 */

 /**
 * Create a Radar object.
 *
 * @param {Match} match The match where this object will work.
 * @param {[number, number]} coords The radar's position, in format [x, y].
 */
var Radar = exports.Radar = function(match, coords) {
	Radar.superConstructor.apply(this, arguments);
	this.currentMatch = match;
	this.center = coords;
	this.centralAngle = 2 * Math.PI;
	this.startAngle = 0;
	this.currentAngle = 0;
	this.level = 0;
	this.startHealth = this.health = 1000;
	this.upgradeList = [
		{price: 20, radius: 100, angularVelocity: Math.PI/2, resaleValue: 15 }, // livello 0
		{price: 15, radius: 150, angularVelocity: Math.PI, resaleValue: 25 }, // livello 1
		{price: 15, radius: 200, angularVelocity: Math.PI, resaleValue: 35 } // livello 2
	];
	this.radius = this.upgradeList[this.level].radius;
	this.currentRadius = this.radius;
	this.image = gamejs.image.load(config.dir.images + "sat.png");
};

/**
 * Radar extend Turret.
 */
gamejs.utils.objects.extend(Radar, turretjs.Turret);
/**
 * Return the length of the detector ray.
 *
 * @return {number} The length of the ray, in pixel.
 */
Radar.prototype.getRadius = function() {
	return (this.upgradeList[this.level]).radius;
};

/**
 * Return the match where this radar work.
 *
 * @param {Match} The match where this radar work.
 */
Radar.prototype.getMatch = function() {
	return this.currentMatch;
};

/**
 * Sets the angle where the detection starts.
 *
 * @param {number} angle The angle where the detection starts
 * @throws {TypeError} Throws this exception if angle's type isn't 'number'.
 */
Radar.prototype.setStartAngle = function(angle) {
	this.startAngle = angle;
};

/**
 * Sets the angle covered by the radar.
 *
 * @param {number} angle The angle covered by the radar.
 * @throws {TypeError} Throws this exception if angle's type isn't 'number'.
 * @throws {RangeError} Throws this exception if angle's value isn't valid.
 */
Radar.prototype.setCentralAngle = function(angle) {
	this.centralAngle = angle;
};

/**
 * Updates the position of the detector ray of the radar.
 *
 * @param {number} msDuration The time past from the last call, in ms.
 */
Radar.prototype.update = function(msDuration) {
	var angVel = this.upgradeList[this.level].angularVelocity;
	var angMade = angVel * msDuration / 1000;
	this.currentRadius = this.getRadius();
	this.currentAngle = (this.currentAngle + angMade) % this.centralAngle;

	var center=this.center;
	var x_endPoint = center[0] + Math.sin(this.currentAngle) * this.currentRadius;
	var y_endPoint = center[1] + Math.cos(this.currentAngle) * this.currentRadius;
	var endPoint = [x_endPoint, y_endPoint];

	this.currentMatch.getMap().getEnemies().sprites().forEach(function(enemy) {
		if (enemy.rect.collideLine(center, endPoint)) {
			enemy.setDetected(1);
		}
	});

	var stop = false;
	for (var i = 0; i <= this.radius && ! stop; i++) {

		var check = [
			Math.floor(this.center[0] + Math.sin(this.currentAngle) * i),
			Math.floor(this.center[1] + Math.cos(this.currentAngle) * i)
		];

		this.currentRadius = i;

		if (this.currentMatch.getMap().isFilled(check)) {
			stop = true;
		}
	};
};

/**
 * Draws the radar in the given Surface.
 *
 * @param {gamejs.Surface} surface The Surface where draw on.
 */
Radar.prototype.draw = function(surface) {
	//draw radar tower
	var dim = this.image.getSize();
	var upLeft = gamejs.utils.vectors.subtract(this.center, gamejs.utils.vectors.divide(dim, 2));
	surface.blit(this.image, upLeft);

	//draw the ray
	this.currentRadius = this.getRadius();
	var endPoint = gamejs.utils.vectors.add(this.center, gamejs.utils.vectors.multiply([Math.sin(this.currentAngle), Math.cos(this.currentAngle)], this.currentRadius));
	gamejs.draw.line(surface, '#adff2f', this.center, endPoint, 2);
	
	//healt bar
	var startLine = gamejs.utils.vectors.subtract(this.center, gamejs.utils.vectors.divide([dim[0], dim[1] *3/2], 2));
	var endLine = gamejs.utils.vectors.add(startLine, [dim[0] *  this.health /this.startHealth, 0]);
	gamejs.draw.line(surface, '#adff2f', startLine, endLine, 2);
};