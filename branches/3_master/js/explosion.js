var gamejs = require("gamejs");
var match = require("js/match");

/**
 * A explosion.
 *
 * @param {Match} match The Match object where this object will work.
 * @param {[numeric, numeric]} the position of the explosion.
 * @param {numeric} the explosion duration.
 */
var Explosion = exports.Explosion = function(match, pos, duration) {
	Explosion.superConstructor.apply(this, arguments);

	this.match = match;
	this.center = pos;
	this.duration = duration; // duration of the explosion on the screen

	this.image = gamejs.image.load(config.dir.images + "explosion.png");
	this.rect = new gamejs.Rect(this.center, this.image.getSize());
	this.dims = this.image.getSize();
};

/**
 * Explosion extend Sprite.
 */
gamejs.utils.objects.extend(Explosion, gamejs.sprite.Sprite);

/**
 * Return the match where this explosion work.
 *
 * @return {Match} The match where this explosion work.
 */
Explosion.prototype.getMatch = function() {
	return this.match;
};

/**
 * Return the current explosion duration.
 *
 * @return {numeric} The duration of the explosion.
 */
Explosion.prototype.getDuration = function() {
	return this.duration;
};

/**
 *
 *
 * We don't need to update the position of the explosion in this moment.
 *
 *
 */
Explosion.prototype.update = function(msDuration) {
	this.duration -= msDuration;
	this.image.setAlpha(1 - this.duration / 1000);
	this.center = [this.center[0]-1,this.center[1]];
	if (this.duration < 0)
		this.kill();
};

/**
 * Draws the explosion.
 *
 * @param {gamejs.Surface} surface The Surface where draw on.
 */
Explosion.prototype.draw = function(surface) {
	surface.blit( this.image, [this.center[0]- this.dims[0]/2, this.center[1] -this.dims[1]/2]);
};