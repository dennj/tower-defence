var gamejs = require('gamejs');
var antColonyProvider = require('js/antColonyProvider');

var mapjs = require("js/map");
var radarjs = require("js/radar");
var matchjs = require("js/match");
var enemyjs = require("js/enemy");
var laserenemyjs = require("js/laserEnemy");
var obstaclejs = require("js/obstacle");
var laserTurretjs = require("js/laserTurret");
var missileTurretjs = require("js/missileTurret");
var slowmotionTurretjs = require("js/slowmotionTurret");

var s=0;//Menu selection: 0 for radar, 1 for turret and 2 for upgrade

//preload resources this way
gamejs.preload([
	config.dir.images + "background.png",
	config.dir.images + "sat.png",
	config.dir.images + "enemy.png",
	config.dir.images + "enemy_detected.png",
	config.dir.images + "obstacle.png",
	config.dir.images + "turret.png",
	config.dir.images + "arrow.png",
	config.dir.images + "bullet-green.png",
	config.dir.images + "explosion.png"
]);

gamejs.ready(main);

/**
 *The main.
 */
function main() {
	//our code here
	var WIDTH = 800;
	var HEIGHT = 480;
	gamejs.display.setMode([WIDTH, HEIGHT]);
	var surface = gamejs.display.getSurface();
	var match = new matchjs.Match();
	var map = new mapjs.Map(match, WIDTH, HEIGHT);
	match.setMap(map);
	antColonyProvider.AntColonyProvider.setMap(map);


	//image for arrow
	var image_arrow = gamejs.image.load(config.dir.images + "arrow.png");

	//image  for turret
	var image_turret = gamejs.image.load(config.dir.images + "turret.png");

	//image  for obstacle
	var image_obstacle = gamejs.image.load(config.dir.images + "obstacle.png");
	image_obstacle = gamejs.transform.scale(
			image_obstacle,
		[40, 40]
	);

	//configuration parameters for object selection
	var ico_size=35;
	var ico_pos_radar = [10,440];
	var ico_pos_turret = [45,440];
	var ico_pos_missileTurret = [80,440];
	var ico_pos_slowmotionTurret = [115,440];
	var ico_pos_obstacle = [150,440];
	var ico_pos_arrow = [187,440];

	/**
	 * Function called every frame refresh
	 *
	 * @param {number} msDuration Passed by gamejs to the function in gamejs.time.fpsCallback
	 */
	var step = function(msDuration) {
		match.getMap().update(msDuration);
		match.getMap().draw(surface);

		// Draw menu ico
		surface.blit(gamejs.image.load(config.dir.images + "sat.png"), ico_pos_radar);
		surface.blit(image_turret, ico_pos_turret);
		surface.blit(image_arrow, ico_pos_arrow);
		surface.blit(image_turret, ico_pos_missileTurret);
		surface.blit(image_obstacle, ico_pos_obstacle);
		surface.blit(image_turret, ico_pos_slowmotionTurret);
		// End menu ico

		// If click => add a radar
		gamejs.event.get().forEach(function(event) {
			if (event.type == gamejs.event.MOUSE_UP) { // On mouse click
				// If radar ico is clicked => s=true => place radar every click
				// If turret ico is clicked => s=false => place turret every
				// click
				var spacefill = false; //Something fill this area
				
				// check if menu is selected
				if((event.pos[1] > ico_pos_radar[1]) && (event.pos[1] < ico_pos_radar[1] + ico_size)){
					if((event.pos[0] > ico_pos_radar[0]) && (event.pos[0] < ico_pos_radar[0] + ico_size)) {
						s = 0;
					}
					else if((event.pos[0] > ico_pos_turret[0]) && (event.pos[0] < ico_pos_turret[0] + ico_size)) {
						s = 1;
					}
					else if((event.pos[0] > ico_pos_arrow[0]) && (event.pos[0] < ico_pos_arrow[0]+ico_size)) {
						s = 2;
					}
					else if((event.pos[0] > ico_pos_missileTurret[0]) && (event.pos[0] < ico_pos_missileTurret[0]+ico_size)) {
							s = 3;
					}
					else if((event.pos[0] > ico_pos_slowmotionTurret[0]) && (event.pos[0] < ico_pos_slowmotionTurret[0]+ico_size)) {
							s = 4;
					}
					else if((event.pos[0] > ico_pos_obstacle[0]) && (event.pos[0] < ico_pos_obstacle[0]+ico_size)) {
							s = 5;
					}
				}
				
				else if(s === 2) { // upgrade
					match.getMap().getTurrets().sprites().forEach(function(turret) {
						if(gamejs.utils.vectors.distance(event.pos, turret.getCenter()) < ico_size/2){
							var price = turret.upgradeList[turret.level].price;
							if(match.changeScore(0) >= price && turret.upgrade())
								match.changeScore(-price);
						};
					});
					match.getMap().getRadars().sprites().forEach(function(radar) {
						if(gamejs.utils.vectors.distance(event.pos, radar.getCenter()) < ico_size/2){
							var price = radar.upgradeList[radar.level].price;
							if(match.changeScore(0) >= price && radar.upgrade())
								match.changeScore(-price);
						};
					});
				}
				
				else {
					// check area
					match.getMap().getTurrets().sprites().forEach(function(turret) {
						if(gamejs.utils.vectors.distance(event.pos, turret.getCenter()) < ico_size){
							spacefill = true;
							return;
						};
					});					
					if(!spacefill) // if space free from turret => check radar
					match.getMap().getRadars().sprites().forEach(function(radar) {
						if(gamejs.utils.vectors.distance(event.pos, radar.getCenter()) < ico_size){
							spacefill = true;
							return;
						};
					});
					
					if(!spacefill){ // if space is free build something					
						if(s === 0 && match.changeScore(0) >= 20) {
							match.getMap().addRadar(new radarjs.Radar(match, event.pos));
							match.changeScore(-20);
						}
						else if(s === 1 && match.changeScore(0) >= 20) {
							match.getMap().addTurret(new missileTurretjs.missileTurret(match, event.pos));
							match.changeScore(-20);
						}
		
						else if(s === 3 && match.changeScore(0) >= 20) {
							match.getMap().addTurret(new laserTurretjs.laserTurret(match, event.pos));
							match.changeScore(-20);
						}
						else if(s === 4 && match.changeScore(0) >= 20) {
							match.getMap().addTurret(new slowmotionTurretjs.slowmotionTurret(match, event.pos));
							match.changeScore(-20);
						}
						else if(s === 5 && match.changeScore(0) >= 50) {
							match.getMap().addObstacle(new obstaclejs.Obstacle(event.pos, 20));
							match.changeScore(-50);
						}
					}
				}
			}
		});
	};
	// end step function

	// spawn enemies
	var health = 0; // enemy health
	for(var time = 0; time < 20; time++){
		setTimeout(function(){
			for(var i = 0; i<4; i++) {
				match.getMap().addEnemy(new enemyjs.Enemy(match, [100, 100],health));
			}
			match.getMap().addEnemy(new laserenemyjs.LaserEnemy(match, [100, 100],health));
			health++;
		}, 4000*time);
	}
	for(var u=0; time < 200; time++){
		setTimeout(function(){
			for(var i = 0; i<5; i++) {
				match.getMap().addEnemy(new laserenemyjs.LaserEnemy(match, [100, 100],health));
			}
			health++;
		}, 4000*time);
	}

	// obstacle tester
	match.getMap().addObstacle(new obstaclejs.Obstacle([300, 300], 70));
	match.getMap().addObstacle(new obstaclejs.Obstacle([600, 100], 70));
	match.getMap().addObstacle(new obstaclejs.Obstacle([450, 200], 70));
	match.getMap().addObstacle(new obstaclejs.Obstacle([300, 699], 70));
	match.getMap().addObstacle(new obstaclejs.Obstacle([80, 200], 70));

	// boot up the update calls
	gamejs.time.fpsCallback(step, this, 40);
};
